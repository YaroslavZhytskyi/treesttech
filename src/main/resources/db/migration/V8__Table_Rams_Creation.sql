CREATE TABLE IF NOT EXISTS rams
(
    ram_id int,
    ram_name     varchar(100),
    ram_capacity int,
    equipment_id int          NOT NULL REFERENCES equipment (equipment_id) ON DELETE CASCADE
);