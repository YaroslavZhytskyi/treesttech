CREATE TABLE IF NOT EXISTS companies
(
    company_id   SERIAL PRIMARY KEY,
    company_name varchar(200) NOT NULL UNIQUE
);