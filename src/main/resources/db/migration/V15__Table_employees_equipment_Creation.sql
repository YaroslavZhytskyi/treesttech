CREATE TABLE IF NOT EXISTS employees_equipment
(
    employee_id   INT REFERENCES employees (employee_id) ON DELETE CASCADE,
    equipment_id INT REFERENCES equipment (equipment_id) ON DELETE CASCADE
);