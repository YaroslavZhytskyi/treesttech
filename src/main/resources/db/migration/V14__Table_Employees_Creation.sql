CREATE TABLE IF NOT EXISTS employees
(
    employee_id         SERIAL PRIMARY KEY,
    employee_name       varchar(50) NOT NULL,
    employee_surname    varchar(50) NOT NULL,
    employee_position   varchar(50),
    employee_department varchar(50),
    company_id          int         NOT NULL REFERENCES companies (company_id) ON DELETE CASCADE
);