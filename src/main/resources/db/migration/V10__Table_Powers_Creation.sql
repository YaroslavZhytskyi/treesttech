CREATE TABLE IF NOT EXISTS powers
(
    power_id int,
    power_name     varchar(100),
    power_capacity int,
    equipment_id   int          NOT NULL REFERENCES equipment (equipment_id) ON DELETE CASCADE
);