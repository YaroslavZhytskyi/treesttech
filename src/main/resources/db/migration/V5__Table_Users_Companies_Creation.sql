CREATE TABLE IF NOT EXISTS users_companies
(
    user_id   INT REFERENCES users (user_id) ON DELETE CASCADE,
    company_id INT REFERENCES companies (company_id) ON DELETE CASCADE
);