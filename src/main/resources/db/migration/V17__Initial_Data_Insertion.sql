INSERT INTO roles
VALUES ('ROLE_USER'),
       ('ROLE_ADMIN'),
       ('ROLE_SUPER_ADMIN');

INSERT INTO users (username, password, enabled)
VALUES ('super',
        '$2y$12$xqE0yUNw6d3WYK5hMO4mneVxqVDM.xSvNDyoLOEsWN.NxvVejdKHu',
        true)
;

INSERT INTO authorities
VALUES ('super', 'ROLE_SUPER_ADMIN')
;