CREATE TABLE IF NOT EXISTS users
(
    user_id    SERIAL PRIMARY KEY,
    username   varchar(200) NOT NULL UNIQUE,
    password   varchar(200) NOT NULL,
    user_email varchar(50),
    enabled    boolean      NOT NULL
);