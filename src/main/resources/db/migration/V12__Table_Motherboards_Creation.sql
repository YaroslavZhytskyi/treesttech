CREATE TABLE IF NOT EXISTS motherboards
(
    motherboard_name       varchar(100),
    motherboard_cpu_amount int,
    motherboard_ram_amount int,
    equipment_id           int          NOT NULL REFERENCES equipment (equipment_id) ON DELETE CASCADE
);