CREATE TABLE IF NOT EXISTS authorities
(
    username  varchar(50) NOT NULL UNIQUE,
    authority varchar(50) NOT NULL,
    FOREIGN KEY (username) REFERENCES users (username) ON DELETE CASCADE ON UPDATE CASCADE
);