CREATE TABLE IF NOT EXISTS equipment
(
    equipment_id     SERIAL PRIMARY KEY,
    equipment_name   varchar(50) NOT NULL,
    equipment_group  int         NOT NULL,
    company_id       int         NOT NULL REFERENCES companies (company_id) ON DELETE CASCADE
);