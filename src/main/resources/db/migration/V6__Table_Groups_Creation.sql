CREATE TABLE IF NOT EXISTS groups
(
    group_id   SERIAL PRIMARY KEY,
    group_name varchar(50) NOT NULL UNIQUE,
    group_type varchar(50) NOT NULL
);