CREATE TABLE IF NOT EXISTS cpus
(
    cpu_id int,
    cpu_name     varchar(100),
    cpu_capacity int,
    equipment_id int          NOT NULL REFERENCES equipment (equipment_id) ON DELETE CASCADE
);