CREATE TABLE IF NOT EXISTS roms
(
    rom_id int,
    rom_name     varchar(100),
    rom_capacity int,
    equipment_id int          NOT NULL REFERENCES equipment (equipment_id) ON DELETE CASCADE
);