CREATE TABLE IF NOT EXISTS os
(
    os_name      varchar(50),
    os_bit       varchar(10),
    equipment_id int         NOT NULL REFERENCES equipment (equipment_id) ON DELETE CASCADE
);