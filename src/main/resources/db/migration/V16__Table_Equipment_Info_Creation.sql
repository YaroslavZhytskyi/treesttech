CREATE TABLE IF NOT EXISTS equipment_info
(
    inventory_number varchar(50),
    serial_number    varchar(50),
    purchase_date    varchar(50),
    note  varchar(250),
    equipment_id     int PRIMARY KEY REFERENCES equipment (equipment_id) ON DELETE CASCADE
);