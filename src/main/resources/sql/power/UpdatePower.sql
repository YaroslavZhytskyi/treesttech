UPDATE powers
SET power_name = :name,
    power_capacity = :capacity,
    equipment_id = :newId
WHERE equipment_id = :equipmentId AND power_id = :powerId;