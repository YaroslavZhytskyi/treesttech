INSERT INTO powers
VALUES (
           :id,
           :name,
           :capacity,
           :equipmentId
       );