SELECT companies.company_id AS id, company_name AS name
FROM companies
         INNER JOIN users_companies ON companies.company_id = users_companies.company_id
WHERE user_id = :id;