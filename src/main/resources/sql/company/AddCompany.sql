INSERT INTO companies (company_name)
VALUES (
           :companyName
       );
INSERT INTO users_companies
VALUES (
           :userId,
           (SELECT MAX(company_id) FROM companies)
       );