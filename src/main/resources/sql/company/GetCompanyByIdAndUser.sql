SELECT TOP 1 companies.company_id AS id, company_name AS name
FROM companies
         INNER JOIN users_companies ON companies.company_id = users_companies.company_id
WHERE user_id = :userId AND companies.company_id = :companyId;