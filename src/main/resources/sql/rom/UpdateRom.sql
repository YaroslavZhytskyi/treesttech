UPDATE roms
SET rom_name = :name,
    rom_capacity = :capacity,
    equipment_id = :newId
WHERE equipment_id = :equipmentId AND rom_id = :romId;