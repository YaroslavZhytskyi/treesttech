UPDATE rams
SET ram_name = :name,
    ram_capacity = :capacity,
    equipment_id = :newId
WHERE equipment_id = :equipmentId AND ram_id = :ramId;