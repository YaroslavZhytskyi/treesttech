UPDATE groups
SET group_name = :name,
    group_type = :type
WHERE group_id = :id;