UPDATE motherboards
SET motherboard_name = :name,
    motherboard_cpu_amount = :cpuAmount,
    motherboard_ram_amount = :ramAmount,
    equipment_id = :newId
WHERE equipment_id = :equipmentId;