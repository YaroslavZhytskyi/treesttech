SELECT equipment_id AS id, equipment_name AS name, equipment_group, company_id
FROM equipment
WHERE equipment_id = :id;