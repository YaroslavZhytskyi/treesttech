UPDATE equipment
SET equipment_name = :name,
    equipment_group = :group,
    company_id = :companyId
WHERE equipment_id = :id;