SELECT equipment_id AS id, equipment_name AS name, equipment_group
FROM equipment
WHERE company_id = :companyId;