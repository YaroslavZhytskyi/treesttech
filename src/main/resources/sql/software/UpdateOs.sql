UPDATE os
SET os_name = :name,
    os_bit = :bit,
    equipment_id = :newId
WHERE equipment_id = :equipmentId;