UPDATE cpus
SET cpu_name = :name,
    cpu_capacity = :capacity,
    equipment_id = :newId
WHERE equipment_id = :equipmentId AND cpu_id = :cpuId;