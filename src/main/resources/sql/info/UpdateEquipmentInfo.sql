UPDATE equipment_info
SET inventory_number = :inventory_number,
    serial_number = :serial_number,
    purchase_date = :purchase_date,
    note = :note
WHERE equipment_id = :id;