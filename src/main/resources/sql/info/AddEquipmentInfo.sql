INSERT INTO equipment_info
VALUES (:inventory_number,
        :serial_number,
        :purchase_date,
        :note,
        :equipment_id);