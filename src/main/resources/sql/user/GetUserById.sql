SELECT user_id AS id, users.username AS username, password, user_email AS email,
       string_agg(authority, ';') AS roles, enabled
FROM users
         INNER JOIN authorities ON  authorities.username = users.username
WHERE user_id = :id
GROUP BY user_id;