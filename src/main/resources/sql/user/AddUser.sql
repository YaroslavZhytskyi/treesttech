INSERT INTO users (username, password, user_email, enabled)
VALUES (
           :username,
           :password,
           :email,
           :enabled);
INSERT INTO authorities
VALUES (:username, :role);