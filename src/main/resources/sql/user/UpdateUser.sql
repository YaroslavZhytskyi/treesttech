UPDATE users
SET username=:username,
    password=:password,
    user_email=:email,
    enabled=:enabled
WHERE user_id=:id;
UPDATE authorities
SET authority=:role
WHERE username = (SELECT username FROM users WHERE user_id=:id);