UPDATE employees
SET employee_name = :name,
    employee_surname = :surname,
    employee_position = :position,
    employee_department = :department
WHERE employee_id = :id;