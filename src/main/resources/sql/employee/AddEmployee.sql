INSERT INTO employees (employee_name, employee_surname, employee_position,
                       employee_department, company_id)
VALUES (
        :name,
        :surname,
        :position,
        :department,
        :companyId
       );