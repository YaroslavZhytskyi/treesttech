package org.geekhub.treesttech.web;

import org.geekhub.treesttech.dao.hardware.HardwareDetailsHandler;
import org.geekhub.treesttech.exception.ElementCountException;
import org.geekhub.treesttech.model.User;
import org.geekhub.treesttech.model.hardware.Ram;
import org.geekhub.treesttech.service.company.CompanyService;
import org.geekhub.treesttech.service.user.UserService;
import org.geekhub.treesttech.service.validator.ComponentValidator;
import org.geekhub.treesttech.web.dto.hardware.RamDto;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.Collections;
import java.util.List;

@RestController
@Validated
@RequestMapping("/equipment/")
public class RamController {

    private final HardwareDetailsHandler<Ram> handler;
    private final ComponentValidator componentValidator;
    private final UserService userService;
    private final CompanyService companyService;

    public RamController(@Qualifier("RamDao") HardwareDetailsHandler<Ram> handler,
                         ComponentValidator componentValidator,
                         UserService userService,
                         CompanyService companyService) {
        this.handler = handler;
        this.componentValidator = componentValidator;
        this.userService = userService;
        this.companyService = companyService;
    }

    @GetMapping("{companyId}/{equipmentId}/ram")
    @Validated
    @ResponseStatus(HttpStatus.OK)
    public List<Ram> getRams(@PathVariable @Min(1) int companyId,
                             @PathVariable @Min(1) int equipmentId) {
        if (isCompanyEquipmentExistAndAccessible(companyId, equipmentId)) {
            return handler.getDetailsByEquipmentId(equipmentId);
        }
        return Collections.emptyList();
    }

    @PostMapping("{companyId}/{equipmentId}/ram")
    @Validated
    @ResponseStatus(HttpStatus.CREATED)
    public void addRam(@PathVariable @Min(1) int companyId,
                       @PathVariable @Min(1) int equipmentId,
                       @Valid RamDto ramDto) {
        if (isCompanyEquipmentExistAndAccessible(companyId, equipmentId)) {
            int capacity = ramDto.getRamCapacity() == null ? 0 : ramDto.getRamCapacity();
            handler.addDetails(new Ram(ramDto.getRamName(), capacity, equipmentId));
        }
    }

    @DeleteMapping("{companyId}/{equipmentId}/ram/{ramId}")
    @Validated
    @ResponseStatus(HttpStatus.OK)
    public void removeRam(@PathVariable @Min(1) int companyId,
                          @PathVariable @Min(1) int equipmentId,
                          @PathVariable @Min(1) int ramId) {
        if (isCompanyEquipmentExistAndAccessible(companyId, equipmentId)) {
            if (componentValidator.isRamExist(equipmentId, ramId)
                && handler.countDetails(equipmentId) > 1) {
                handler.removeDetails(equipmentId, ramId);
            } else {
                String msg = "There must be at least one Ram";
                throw new ElementCountException(msg);
            }
        }
    }

    @PutMapping("{companyId}/{equipmentId}/ram/{ramId}")
    @Validated
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateRam(@Valid RamDto ramDto,
                          @PathVariable @Min(1) int companyId,
                          @PathVariable @Min(1) int equipmentId,
                          @PathVariable @Min(1) int ramId) {
        if (isCompanyEquipmentExistAndAccessible(companyId, equipmentId)) {
            if (componentValidator.isRamExist(equipmentId, ramId)) {
                int capacity = ramDto.getRamCapacity() == null ? 0 : ramDto.getRamCapacity();
                var ram = new Ram(ramDto.getRamName(), capacity, equipmentId);
                handler.updateDetails(ram, equipmentId, ramId);
            }
        }
    }

    private boolean isCompanyEquipmentExistAndAccessible(int companyId, int equipmentId) {
        if (companyService.isCompanyExist(companyId)
            && componentValidator.isEquipmentExist(companyId, equipmentId)) {
            User user = userService.getCurrentUser();
            return companyService.hasAccessToCompany(user, companyId);
        }
        return false;
    }
}
