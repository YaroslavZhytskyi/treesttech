package org.geekhub.treesttech.web;

import org.geekhub.treesttech.dao.hardware.MotherBoardHandler;
import org.geekhub.treesttech.model.User;
import org.geekhub.treesttech.model.hardware.MotherBoard;
import org.geekhub.treesttech.service.company.CompanyService;
import org.geekhub.treesttech.service.user.UserService;
import org.geekhub.treesttech.service.validator.ComponentValidator;
import org.geekhub.treesttech.web.dto.hardware.MotherBoardDto;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@RestController
@Validated
@RequestMapping("/equipment/")
public class MotherboardController {

    private final MotherBoardHandler mbHandler;
    private final ComponentValidator componentValidator;
    private final UserService userService;
    private final CompanyService companyService;

    public MotherboardController(MotherBoardHandler mbHandler,
                                 ComponentValidator componentValidator,
                                 UserService userService,
                                 CompanyService companyService) {
        this.mbHandler = mbHandler;
        this.componentValidator = componentValidator;
        this.userService = userService;
        this.companyService = companyService;
    }

    @GetMapping("{companyId}/{equipmentId}/motherboard")
    @Validated
    @ResponseStatus(HttpStatus.OK)
    public MotherBoard getMotherboard(@PathVariable @Min(1) int companyId,
                                      @PathVariable @Min(1) int equipmentId) {
        if (isCompanyEquipmentExistAndAccessible(companyId, equipmentId)) {
            return mbHandler.getMotherBoardById(equipmentId);
        }
        return null;
    }

    @PutMapping("{companyId}/{equipmentId}/motherboard")
    @Validated
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateMotherboard(@Valid MotherBoardDto mbDto,
                                  @PathVariable @Min(1) int companyId,
                                  @PathVariable @Min(1) int equipmentId) {
        if (isCompanyEquipmentExistAndAccessible(companyId, equipmentId)) {
            final int defaultAmount = 1;
            var mbCpuAmount
                = mbDto.getMbCpuAmount() == null ? defaultAmount : mbDto.getMbCpuAmount();
            var mbRamAmount
                = mbDto.getMbRamAmount() == null ? defaultAmount : mbDto.getMbRamAmount();
            var mb = new MotherBoard(mbDto.getMbName(), mbCpuAmount, mbRamAmount, equipmentId);
            mbHandler.updateMotherBoard(mb, equipmentId);
        }
    }

    private boolean isCompanyEquipmentExistAndAccessible(int companyId, int equipmentId) {
        if (companyService.isCompanyExist(companyId)
            && componentValidator.isEquipmentExist(companyId, equipmentId)) {
            User user = userService.getCurrentUser();
            return companyService.hasAccessToCompany(user, companyId);
        }
        return false;
    }
}
