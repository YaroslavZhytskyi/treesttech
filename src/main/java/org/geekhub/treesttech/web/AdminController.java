package org.geekhub.treesttech.web;

import io.swagger.annotations.ApiParam;
import org.geekhub.treesttech.model.User;
import org.geekhub.treesttech.service.user.UserService;
import org.geekhub.treesttech.web.dto.user.UserDto;
import org.geekhub.treesttech.web.dto.validation.OnCreate;
import org.geekhub.treesttech.web.dto.validation.OnUpdate;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;

@Validated
@RestController
@RequestMapping("/admin")
public class AdminController {

    private static final String ROLES = "USER, ADMIN, SUPER_ADMIN";
    private static final String ROLE_PREFIX = "ROLE_";

    private final PasswordEncoder encoder;
    private final UserService userService;

    public AdminController(PasswordEncoder encoder, UserService userService) {
        this.encoder = encoder;
        this.userService = userService;
    }

    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @PostMapping("")
    @Validated(OnCreate.class)
    @ResponseStatus(HttpStatus.CREATED)
    public void createUser(
        @ApiParam(
            name = "role",
            type = "String",
            allowableValues = ROLES,
            required = true)
        @RequestParam String role,
        @Valid UserDto userDto) {
        String encryptPassword = encoder.encode(userDto.getPassword());
        User user = new User(
            userDto.getUsername(),
            encryptPassword,
            userDto.getEmail(),
            ROLE_PREFIX + role,
            userDto.isEnabled()
        );
        userService.createUser(user);
    }

    @PutMapping("/{id}")
    @Validated(OnUpdate.class)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateUser(
        @ApiParam(
            name = "role",
            type = "String",
            allowableValues = ROLES)
        @RequestParam String role, @Valid UserDto userDto, @PathVariable @Min(1) int id) {
        String encryptPassword = encoder.encode(userDto.getPassword());
        User user = new User(
            userDto.getUsername(),
            encryptPassword,
            userDto.getEmail(),
            ROLE_PREFIX + role,
            userDto.isEnabled()
        );
        userService.updateUser(user, id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void removeUser(@PathVariable @Min(1) int id) {
        userService.removeUser(id);
    }
}
