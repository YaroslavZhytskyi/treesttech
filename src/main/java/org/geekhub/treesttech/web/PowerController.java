package org.geekhub.treesttech.web;

import org.geekhub.treesttech.dao.hardware.HardwareDetailsHandler;
import org.geekhub.treesttech.exception.ElementCountException;
import org.geekhub.treesttech.model.User;
import org.geekhub.treesttech.model.hardware.PowerSupply;
import org.geekhub.treesttech.service.company.CompanyService;
import org.geekhub.treesttech.service.user.UserService;
import org.geekhub.treesttech.service.validator.ComponentValidator;
import org.geekhub.treesttech.web.dto.hardware.PowerDto;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.Collections;
import java.util.List;

@RestController
@Validated
@RequestMapping("/equipment/")
public class PowerController {

    private final HardwareDetailsHandler<PowerSupply> handler;
    private final ComponentValidator componentValidator;
    private final UserService userService;
    private final CompanyService companyService;

    public PowerController(@Qualifier("PowerSupply") HardwareDetailsHandler<PowerSupply> handler,
                           ComponentValidator componentValidator,
                           UserService userService,
                           CompanyService companyService) {
        this.handler = handler;
        this.componentValidator = componentValidator;
        this.userService = userService;
        this.companyService = companyService;
    }

    @GetMapping("{companyId}/{equipmentId}/power")
    @Validated
    @ResponseStatus(HttpStatus.OK)
    public List<PowerSupply> getPowers(@PathVariable @Min(1) int companyId,
                                       @PathVariable @Min(1) int equipmentId) {
        if (isCompanyEquipmentExistAndAccessible(companyId, equipmentId)) {
            return handler.getDetailsByEquipmentId(equipmentId);
        }
        return Collections.emptyList();
    }

    @PostMapping("{companyId}/{equipmentId}/power")
    @Validated
    @ResponseStatus(HttpStatus.CREATED)
    public void addPower(@PathVariable @Min(1) int companyId,
                         @PathVariable @Min(1) int equipmentId,
                         @Valid PowerDto powerDto) {
        if (isCompanyEquipmentExistAndAccessible(companyId, equipmentId)) {
            int capacity = powerDto.getPowerCapacity() == null ? 0 : powerDto.getPowerCapacity();
            handler.addDetails(new PowerSupply(powerDto.getPowerName(), capacity, equipmentId));
        }
    }

    @DeleteMapping("{companyId}/{equipmentId}/power/{powerId}")
    @Validated
    @ResponseStatus(HttpStatus.OK)
    public void removePower(@PathVariable @Min(1) int companyId,
                            @PathVariable @Min(1) int equipmentId,
                            @PathVariable @Min(1) int powerId) {
        if (isCompanyEquipmentExistAndAccessible(companyId, equipmentId)) {
            if (componentValidator.isPowerExist(equipmentId, powerId)
                && handler.countDetails(equipmentId) > 1) {
                handler.removeDetails(equipmentId, powerId);
            } else {
                String msg = "There must be at least one PowerSupply";
                throw new ElementCountException(msg);
            }
        }
    }

    @PutMapping("{companyId}/{equipmentId}/power/{powerId}")
    @Validated
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updatePower(@Valid PowerDto powerDto,
                            @PathVariable @Min(1) int companyId,
                            @PathVariable @Min(1) int equipmentId,
                            @PathVariable @Min(1) int powerId) {
        if (isCompanyEquipmentExistAndAccessible(companyId, equipmentId)) {
            if (componentValidator.isPowerExist(equipmentId, powerId)) {
                int capacity = powerDto.getPowerCapacity()
                    == null ? 0 : powerDto.getPowerCapacity();
                var power = new PowerSupply(powerDto.getPowerName(), capacity, equipmentId);
                handler.updateDetails(power, equipmentId, powerId);
            }
        }
    }

    private boolean isCompanyEquipmentExistAndAccessible(int companyId, int equipmentId) {
        if (companyService.isCompanyExist(companyId)
            && componentValidator.isEquipmentExist(companyId, equipmentId)) {
            User user = userService.getCurrentUser();
            return companyService.hasAccessToCompany(user, companyId);
        }
        return false;
    }
}
