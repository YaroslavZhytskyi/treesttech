package org.geekhub.treesttech.web;

import org.geekhub.treesttech.dao.hardware.HardwareDetailsHandler;
import org.geekhub.treesttech.exception.ElementCountException;
import org.geekhub.treesttech.model.User;
import org.geekhub.treesttech.model.hardware.Cpu;
import org.geekhub.treesttech.service.company.CompanyService;
import org.geekhub.treesttech.service.user.UserService;
import org.geekhub.treesttech.service.validator.ComponentValidator;
import org.geekhub.treesttech.web.dto.hardware.CpuDto;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.Collections;
import java.util.List;

@RestController
@Validated
@RequestMapping("/equipment/")
public class CpuController {

    private final HardwareDetailsHandler<Cpu> handler;
    private final ComponentValidator componentValidator;
    private final UserService userService;
    private final CompanyService companyService;

    public CpuController(@Qualifier("CpuDao") HardwareDetailsHandler<Cpu> handler,
                         ComponentValidator componentValidator,
                         UserService userService,
                         CompanyService companyService) {
        this.handler = handler;
        this.componentValidator = componentValidator;
        this.userService = userService;
        this.companyService = companyService;
    }

    @GetMapping("{companyId}/{equipmentId}/cpu")
    @Validated
    @ResponseStatus(HttpStatus.OK)
    public List<Cpu> getCpus(@PathVariable @Min(1) int companyId,
                             @PathVariable @Min(1) int equipmentId) {
        if (isCompanyEquipmentExistAndAccessible(companyId, equipmentId)) {
            return handler.getDetailsByEquipmentId(equipmentId);
        }
        return Collections.emptyList();
    }

    @PostMapping("{companyId}/{equipmentId}/cpu")
    @Validated
    @ResponseStatus(HttpStatus.CREATED)
    public void addCpu(@PathVariable @Min(1) int companyId,
                       @PathVariable @Min(1) int equipmentId,
                       @Valid CpuDto cpuDto) {
        if (isCompanyEquipmentExistAndAccessible(companyId, equipmentId)) {
            int capacity = cpuDto.getCpuCapacity() == null ? 0 : cpuDto.getCpuCapacity();
            handler.addDetails(new Cpu(cpuDto.getCpuName(), capacity, equipmentId));
        }
    }

    @DeleteMapping("{companyId}/{equipmentId}/cpu/{cpuId}")
    @Validated
    @ResponseStatus(HttpStatus.OK)
    public void removeCpu(@PathVariable @Min(1) int companyId,
                          @PathVariable @Min(1) int equipmentId,
                          @PathVariable @Min(1) int cpuId) {
        if (isCompanyEquipmentExistAndAccessible(companyId, equipmentId)) {
            if (componentValidator.isCpuExist(equipmentId, cpuId)
                && handler.countDetails(equipmentId) > 1) {
                handler.removeDetails(equipmentId, cpuId);
            } else {
                String msg = "There must be at least one Cpu";
                throw new ElementCountException(msg);
            }
        }
    }

    @PutMapping("{companyId}/{equipmentId}/cpu/{cpuId}")
    @Validated
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateCpu(@Valid CpuDto cpuDto,
                          @PathVariable @Min(1) int companyId,
                          @PathVariable @Min(1) int equipmentId,
                          @PathVariable @Min(1) int cpuId) {
        if (isCompanyEquipmentExistAndAccessible(companyId, equipmentId)) {
            if (componentValidator.isCpuExist(equipmentId, cpuId)) {
                int capacity = cpuDto.getCpuCapacity() == null ? 0 : cpuDto.getCpuCapacity();
                var cpu = new Cpu(cpuDto.getCpuName(), capacity, equipmentId);
                handler.updateDetails(cpu, equipmentId, cpuId);
            }
        }
    }

    private boolean isCompanyEquipmentExistAndAccessible(int companyId, int equipmentId) {
        if (companyService.isCompanyExist(companyId)
            && componentValidator.isEquipmentExist(companyId, equipmentId)) {
            User user = userService.getCurrentUser();
            return companyService.hasAccessToCompany(user, companyId);
        }
        return false;
    }
}
