package org.geekhub.treesttech.web;

import org.geekhub.treesttech.dao.equipment.EquipmentCommonHandler;
import org.geekhub.treesttech.dao.equipment.EquipmentInfoHandler;
import org.geekhub.treesttech.dao.hardware.HardwareDetailsHandler;
import org.geekhub.treesttech.dao.hardware.MotherBoardHandler;
import org.geekhub.treesttech.dao.software.OsHandler;
import org.geekhub.treesttech.model.User;
import org.geekhub.treesttech.model.equipment.Equipment;
import org.geekhub.treesttech.model.equipment.EquipmentCommon;
import org.geekhub.treesttech.model.hardware.Cpu;
import org.geekhub.treesttech.model.hardware.MotherBoard;
import org.geekhub.treesttech.model.hardware.PowerSupply;
import org.geekhub.treesttech.model.hardware.Ram;
import org.geekhub.treesttech.model.hardware.Rom;
import org.geekhub.treesttech.model.software.OperatingSystem;
import org.geekhub.treesttech.model.software.OperatingSystemBit;
import org.geekhub.treesttech.service.EquipmentHandler;
import org.geekhub.treesttech.service.company.CompanyService;
import org.geekhub.treesttech.service.user.UserService;
import org.geekhub.treesttech.service.validator.ComponentValidator;
import org.geekhub.treesttech.web.dto.equipment.EquipmentCommonDto;
import org.geekhub.treesttech.web.dto.hardware.CpuDto;
import org.geekhub.treesttech.web.dto.hardware.MotherBoardDto;
import org.geekhub.treesttech.web.dto.hardware.PowerDto;
import org.geekhub.treesttech.web.dto.hardware.RamDto;
import org.geekhub.treesttech.web.dto.hardware.RomDto;
import org.geekhub.treesttech.web.dto.software.OsDto;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.Collections;
import java.util.List;

@RestController
@Validated
@RequestMapping("/equipment/")
public class EquipmentController {

    private final EquipmentCommonHandler eqCommonHandler;
    private final EquipmentHandler handler;
    private final HardwareDetailsHandler<Ram> ramHandler;
    private final HardwareDetailsHandler<Rom> romHandler;
    private final HardwareDetailsHandler<Cpu> cpuHandler;
    private final HardwareDetailsHandler<PowerSupply> powerHandler;
    private final MotherBoardHandler mbHandler;
    private final OsHandler osHandler;
    private final EquipmentInfoHandler infoHandler;
    private final ComponentValidator componentValidator;
    private final UserService userService;
    private final CompanyService companyService;

    public EquipmentController(EquipmentCommonHandler eqCommonHandler,
                               EquipmentHandler handler,
                               HardwareDetailsHandler<Ram> ramHandler,
                               HardwareDetailsHandler<Rom> romHandler,
                               HardwareDetailsHandler<Cpu> cpuHandler,
                               HardwareDetailsHandler<PowerSupply> powerHandler,
                               MotherBoardHandler mbHandler,
                               OsHandler osHandler,
                               EquipmentInfoHandler infoHandler,
                               ComponentValidator componentValidator,
                               UserService userService,
                               CompanyService companyService) {
        this.eqCommonHandler = eqCommonHandler;
        this.handler = handler;
        this.ramHandler = ramHandler;
        this.romHandler = romHandler;
        this.cpuHandler = cpuHandler;
        this.powerHandler = powerHandler;
        this.mbHandler = mbHandler;
        this.osHandler = osHandler;
        this.infoHandler = infoHandler;
        this.componentValidator = componentValidator;
        this.userService = userService;
        this.companyService = companyService;
    }

    @PostMapping("{companyId}/")
    @Validated
    @Transactional
    @ResponseStatus(HttpStatus.CREATED)
    public void addEquipment(@PathVariable @Min(1) int companyId,
                             @Valid EquipmentCommonDto eqCommon,
                             @Valid CpuDto cpu,
                             @Valid MotherBoardDto mb,
                             @Valid RamDto ram,
                             @Valid RomDto rom,
                             @Valid PowerDto power,
                             @Valid OsDto osDto) {
        if (isCompanyGroupExistAndAccessible(companyId, eqCommon.getGroupId())) {
            EquipmentCommon common = new EquipmentCommon(
                eqCommon.getEquipmentName(),
                eqCommon.getGroupId(),
                companyId);
            int equipmentId = eqCommonHandler.addEquipment(common).getId();

            ramHandler.addDetails(
                new Ram(ram.getRamName(),
                    checkIntOnNull(ram.getRamCapacity()), equipmentId));

            romHandler.addDetails(
                new Rom(rom.getRomName(),
                    checkIntOnNull(rom.getRomCapacity()), equipmentId));

            cpuHandler.addDetails(
                new Cpu(cpu.getCpuName(),
                    checkIntOnNull(cpu.getCpuCapacity()), equipmentId));

            powerHandler.addDetails(
                new PowerSupply(power.getPowerName(),
                    checkIntOnNull(power.getPowerCapacity()), equipmentId));

            mbHandler.addMotherBoard(new MotherBoard(mb.getMbName(),
                checkIntOnNull(mb.getMbCpuAmount()),
                checkIntOnNull(mb.getMbRamAmount()), equipmentId));

            OperatingSystemBit bit = osDto.getBit();
            if (bit == null) {
                bit = OperatingSystemBit.x64;
            }
            osHandler.addOs(new OperatingSystem(
                osDto.getOsName(), bit, equipmentId));

            infoHandler.addEmptyEquipmentInfo(equipmentId);
        }
    }

    @DeleteMapping("{companyId}/")
    @ResponseStatus(HttpStatus.OK)
    public void removeEquipmentById(@PathVariable @Min(1) int companyId,
                                    @RequestParam @Min(1) int equipmentId) {
        if (isCompanyEquipmentExistAndAccessible(companyId, equipmentId)) {
            eqCommonHandler.removeEquipmentById(equipmentId);
        }
    }

    @GetMapping("{companyId}/")
    @ResponseStatus(HttpStatus.OK)
    List<EquipmentCommon> getAllEquipment(@PathVariable @Min(1) int companyId) {
        if (companyService.isCompanyExist(companyId)) {
            User user = userService.getCurrentUser();
            if (companyService.hasAccessToCompany(user, companyId)) {
                return handler.getAllEquipment(companyId);
            }
        }
        return Collections.emptyList();
    }

    @GetMapping("{companyId}/{equipmentId}")
    @ResponseStatus(HttpStatus.OK)
    public Equipment getEquipmentById(@PathVariable @Min(1) int companyId,
                                      @PathVariable @Min(1) int equipmentId) {
        if (isCompanyEquipmentExistAndAccessible(companyId, equipmentId)) {
            return handler.getEquipmentById(equipmentId);
        }
        return null;
    }

    @PutMapping("{companyId}/{equipmentId}")
    @Validated
    @ResponseStatus(HttpStatus.OK)
    public void updateEquipmentCommon(@PathVariable @Min(1) int companyId,
                                      @PathVariable @Min(1) int equipmentId,
                                      @Valid EquipmentCommonDto commonDto) {
        if (isCompanyEquipmentExistAndAccessible(companyId, equipmentId)) {
            int groupId = commonDto.getGroupId();
            if (componentValidator.isGroupExist(groupId)) {
                EquipmentCommon common = new EquipmentCommon(
                    commonDto.getEquipmentName(), groupId, companyId);
                eqCommonHandler.updateEquipment(common, equipmentId);
            }
        }
    }

    private int checkIntOnNull(Integer input) {
        if (input == null) {
            return 0;
        }
        return input;
    }

    private boolean isCompanyGroupExistAndAccessible(int companyId, int groupId) {
        if (companyService.isCompanyExist(companyId)
            && componentValidator.isGroupExist(groupId)) {
            User user = userService.getCurrentUser();
            return companyService.hasAccessToCompany(user, companyId);
        }
        return false;
    }

    private boolean isCompanyEquipmentExistAndAccessible(int companyId, int equipmentId) {
        if (companyService.isCompanyExist(companyId)
            && componentValidator.isEquipmentExist(companyId, equipmentId)) {
            User user = userService.getCurrentUser();
            return companyService.hasAccessToCompany(user, companyId);
        }
        return false;
    }
}
