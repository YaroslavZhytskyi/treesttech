package org.geekhub.treesttech.web;

import org.geekhub.treesttech.model.company.Company;
import org.geekhub.treesttech.service.company.CompanyService;
import org.geekhub.treesttech.web.dto.company.CompanyDto;
import org.geekhub.treesttech.web.dto.validation.OnCreate;
import org.geekhub.treesttech.web.dto.validation.OnUpdate;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;

@RestController
@Validated
@RequestMapping("/company")
public class CompanyController {

    private final CompanyService companyService;

    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    public List<Company> getCompaniesByUser() {
        return companyService.getCompaniesByUser();
    }

    @PostMapping("")
    @Validated(OnCreate.class)
    @ResponseStatus(HttpStatus.CREATED)
    public void createCompany(@Valid CompanyDto companyDto) {
        companyService.addCompany(new Company(companyDto.getCompanyName()));
    }

    @PutMapping("")
    @Validated
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void assignCompanyToUser(@RequestParam @Min(1) int companyId,
                                    @RequestParam @Min(1) int userId) {
        companyService.assignCompanyToUser(companyId, userId);
    }

    @DeleteMapping("")
    @ResponseStatus(HttpStatus.OK)
    public void removeCompany(@RequestParam @Min(1) int companyId) {
        companyService.removeCompany(companyId);
    }

    @PutMapping("/{companyId}")
    @Validated(OnUpdate.class)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateCompany(@Valid CompanyDto companyDto, @PathVariable @Min(1) int companyId) {
        Company company = new Company(companyDto.getCompanyName());
        companyService.updateCompany(company, companyId);
    }

    @GetMapping("/{companyId}")
    @ResponseStatus(HttpStatus.OK)
    public Company getCompanyById(@PathVariable @Min(1) int companyId) {
        return companyService.getCompanyById(companyId);
    }
}
