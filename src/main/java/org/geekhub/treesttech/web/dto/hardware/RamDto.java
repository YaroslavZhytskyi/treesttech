package org.geekhub.treesttech.web.dto.hardware;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

public class RamDto {

    private static final int MAX_LENGTH = 100;

    @Size(max = MAX_LENGTH)
    private final String ramName;

    @Min(0)
    private final Integer ramCapacity;

    public RamDto(String ramName, Integer ramCapacity) {
        this.ramName = ramName;
        this.ramCapacity = ramCapacity;
    }

    public String getRamName() {
        return ramName;
    }

    public Integer getRamCapacity() {
        return ramCapacity;
    }
}
