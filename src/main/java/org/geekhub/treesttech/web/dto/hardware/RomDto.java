package org.geekhub.treesttech.web.dto.hardware;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

public class RomDto {

    private static final int MAX_LENGTH = 100;

    @Size(max = MAX_LENGTH)
    private final String romName;

    @Min(1)
    private final Integer romCapacity;

    public RomDto(String romName, Integer romCapacity) {
        this.romName = romName;
        this.romCapacity = romCapacity;
    }

    public String getRomName() {
        return romName;
    }

    public Integer getRomCapacity() {
        return romCapacity;
    }
}
