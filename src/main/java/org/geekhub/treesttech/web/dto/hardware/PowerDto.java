package org.geekhub.treesttech.web.dto.hardware;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

public class PowerDto {

    private static final int MAX_LENGTH = 100;

    @Size(max = MAX_LENGTH)
    private final String powerName;

    @Min(1)
    private final Integer powerCapacity;

    public PowerDto(String powerName, Integer powerCapacity) {
        this.powerName = powerName;
        this.powerCapacity = powerCapacity;
    }

    public String getPowerName() {
        return powerName;
    }

    public Integer getPowerCapacity() {
        return powerCapacity;
    }
}
