package org.geekhub.treesttech.web.dto.company;

import org.geekhub.treesttech.web.dto.validation.OnCreate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class CompanyDto {

    private static final int MAX_LENGTH = 200;

    @NotBlank(groups = OnCreate.class)
    @Size(max = MAX_LENGTH, message = "Company name can't be more than 200" + MAX_LENGTH)
    private final String companyName;

    public CompanyDto(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyName() {
        return companyName;
    }
}
