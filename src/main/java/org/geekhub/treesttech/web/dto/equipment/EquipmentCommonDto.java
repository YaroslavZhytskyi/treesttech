package org.geekhub.treesttech.web.dto.equipment;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class EquipmentCommonDto {

    private static final int MAX_LENGTH = 50;

    @NotBlank
    @Size(max = MAX_LENGTH, message = "Max length is" + MAX_LENGTH)
    private final String equipmentName;

    @NotNull
    @Min(1)
    private final int groupId;

    public EquipmentCommonDto(String equipmentName, int groupId) {
        this.equipmentName = equipmentName;
        this.groupId = groupId;
    }

    public String getEquipmentName() {
        return equipmentName;
    }

    public int getGroupId() {
        return groupId;
    }
}
