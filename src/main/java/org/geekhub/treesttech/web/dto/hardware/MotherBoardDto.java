package org.geekhub.treesttech.web.dto.hardware;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

public class MotherBoardDto {

    private static final int MAX_LENGTH = 100;

    @Size(max = MAX_LENGTH)
    private final String mbName;

    @Min(1)
    private final Integer mbCpuAmount;

    @Min(1)
    private final Integer mbRamAmount;

    public MotherBoardDto(String mbName, Integer mbCpuAmount, Integer mbRamAmount) {
        this.mbName = mbName;
        this.mbCpuAmount = mbCpuAmount;
        this.mbRamAmount = mbRamAmount;
    }

    public String getMbName() {
        return mbName;
    }

    public Integer getMbCpuAmount() {
        return mbCpuAmount;
    }

    public Integer getMbRamAmount() {
        return mbRamAmount;
    }
}
