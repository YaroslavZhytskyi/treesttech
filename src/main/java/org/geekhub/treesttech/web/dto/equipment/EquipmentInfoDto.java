package org.geekhub.treesttech.web.dto.equipment;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@ApiModel
public class EquipmentInfoDto {

    private static final int MAX_LENGTH = 50;
    private static final int MAX_LENGTH_NOTE = 250;
    private static final String DATE_PATTERN = "^\\d{4}-\\d{2}-\\d{2}$";

    @Size(max = MAX_LENGTH)
    private final String inventoryNumber;

    @Size(max = MAX_LENGTH)
    private final String serialNumber;

    @ApiModelProperty(value = "example: 1900-01-01")
    @Pattern(regexp = DATE_PATTERN)
    private final String purchaseDate;

    @Size(max = MAX_LENGTH_NOTE)
    private final String note;

    public EquipmentInfoDto(
        String inventoryNumber, String serialNumber, String purchaseDate, String note) {
        this.inventoryNumber = inventoryNumber;
        this.serialNumber = serialNumber;
        this.purchaseDate = purchaseDate;
        this.note = note;
    }

    public String getInventoryNumber() {
        return inventoryNumber;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public String getNote() {
        return note;
    }
}
