package org.geekhub.treesttech.web.dto.software;

import org.geekhub.treesttech.model.software.OperatingSystemBit;

import javax.validation.constraints.Size;

public class OsDto {

    private static final int MAX_LENGTH = 50;

    @Size(max = MAX_LENGTH)
    private final String osName;

    private final OperatingSystemBit bit;

    public OsDto(String osName, OperatingSystemBit bit) {
        this.osName = osName;
        this.bit = bit;
    }

    public String getOsName() {
        return osName;
    }

    public OperatingSystemBit getBit() {
        return bit;
    }
}
