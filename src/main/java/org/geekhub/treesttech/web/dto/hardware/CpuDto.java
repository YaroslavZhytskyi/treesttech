package org.geekhub.treesttech.web.dto.hardware;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

public class CpuDto {

    private static final int MAX_LENGTH = 100;

    @Size(max = MAX_LENGTH)
    private final String cpuName;

    @Min(1)
    private final Integer cpuCapacity;

    public CpuDto(String cpuName, Integer cpuCapacity) {
        this.cpuName = cpuName;
        this.cpuCapacity = cpuCapacity;
    }

    public String getCpuName() {
        return cpuName;
    }

    public Integer getCpuCapacity() {
        return cpuCapacity;
    }
}
