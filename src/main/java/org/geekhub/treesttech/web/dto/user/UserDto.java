package org.geekhub.treesttech.web.dto.user;

import org.geekhub.treesttech.web.dto.validation.OnCreate;
import org.geekhub.treesttech.web.dto.validation.OnUpdate;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserDto {

    private static final String MAIL_PATTERN =
        "^[\\w!#$%&’*+/=?`{|}~^-]+(?:\\.[\\w!#$%&’*+/=?`{|}~^-]+)*"
            + "@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";

    @NotBlank(groups = OnCreate.class, message = "Username can't be empty")
    private final String username;

    @Pattern(regexp = MAIL_PATTERN)
    @Email
    private final String email;

    @NotBlank(groups = OnCreate.class)
    @NotNull(groups = OnUpdate.class)
    @Size(groups = OnCreate.class, min = 4, max = 40, message = "Password from 4 to 40 symbols")
    private String password;

    @NotNull
    private boolean enabled;

    public UserDto(String username, String email, boolean enabled) {
        this.username = username;
        this.email = email;
        this.enabled = enabled;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public boolean isEnabled() {
        return enabled;
    }
}
