package org.geekhub.treesttech.web.dto.group;

import org.geekhub.treesttech.model.equipment.EquipmentType;
import org.geekhub.treesttech.web.dto.validation.OnCreate;
import org.geekhub.treesttech.web.dto.validation.OnUpdate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class GroupDto {

    private static final int MAX_LENGTH = 50;

    @NotBlank(groups = OnCreate.class)
    @Size(max = MAX_LENGTH, message = "Company name can't be more than" + MAX_LENGTH)
    private final String groupName;

    @NotNull(groups = {OnCreate.class, OnUpdate.class})
    private final EquipmentType type;

    public GroupDto(String groupName, EquipmentType type) {
        this.groupName = groupName;
        this.type = type;
    }

    public String getGroupName() {
        return groupName;
    }

    public EquipmentType getType() {
        return type;
    }
}
