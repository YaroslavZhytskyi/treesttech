package org.geekhub.treesttech.web;

import org.geekhub.treesttech.dao.software.OsHandler;
import org.geekhub.treesttech.model.User;
import org.geekhub.treesttech.model.software.OperatingSystem;
import org.geekhub.treesttech.service.company.CompanyService;
import org.geekhub.treesttech.service.user.UserService;
import org.geekhub.treesttech.service.validator.ComponentValidator;
import org.geekhub.treesttech.web.dto.software.OsDto;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@RestController
@Validated
@RequestMapping("/equipment/")
public class OsController {

    private final OsHandler osHandler;
    private final ComponentValidator componentValidator;
    private final UserService userService;
    private final CompanyService companyService;

    public OsController(OsHandler osHandler,
                        ComponentValidator componentValidator,
                        UserService userService,
                        CompanyService companyService) {
        this.osHandler = osHandler;
        this.componentValidator = componentValidator;
        this.userService = userService;
        this.companyService = companyService;
    }

    @GetMapping("{companyId}/{equipmentId}/os")
    @Validated
    @ResponseStatus(HttpStatus.OK)
    public OperatingSystem getOs(@PathVariable @Min(1) int companyId,
                                 @PathVariable @Min(1) int equipmentId) {
        if (isCompanyEquipmentExistAndAccessible(companyId, equipmentId)) {
            return osHandler.getOsById(equipmentId);
        }
        return null;
    }

    @PutMapping("{companyId}/{equipmentId}/os")
    @Validated
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateOs(@Valid OsDto osDto,
                         @PathVariable @Min(1) int companyId,
                         @PathVariable @Min(1) int equipmentId) {
        if (isCompanyEquipmentExistAndAccessible(companyId, equipmentId)) {
            var os = new OperatingSystem(osDto.getOsName(), osDto.getBit(), equipmentId);
            osHandler.updateOs(os, equipmentId);
        }
    }

    private boolean isCompanyEquipmentExistAndAccessible(int companyId, int equipmentId) {
        if (companyService.isCompanyExist(companyId)
            && componentValidator.isEquipmentExist(companyId, equipmentId)) {
            User user = userService.getCurrentUser();
            return companyService.hasAccessToCompany(user, companyId);
        }
        return false;
    }
}
