package org.geekhub.treesttech.web;

import org.geekhub.treesttech.dao.group.GroupHandler;
import org.geekhub.treesttech.exception.PageNotFoundException;
import org.geekhub.treesttech.model.equipment.EquipmentGroup;
import org.geekhub.treesttech.web.dto.group.GroupDto;
import org.geekhub.treesttech.web.dto.validation.OnCreate;
import org.geekhub.treesttech.web.dto.validation.OnUpdate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@Validated
@RequestMapping("/group")
public class GroupController {

    private static final Logger logger = LoggerFactory.getLogger(GroupController.class);

    private final GroupHandler handler;

    public GroupController(GroupHandler handler) {
        this.handler = handler;
    }

    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    public List<EquipmentGroup> getAllGroups() {
        return handler.getAllGroups();
    }

    @PostMapping("")
    @Validated(OnCreate.class)
    @ResponseStatus(HttpStatus.CREATED)
    public void createGroup(@Valid GroupDto groupDto) {
        handler.addGroup(
            new EquipmentGroup(groupDto.getGroupName(), groupDto.getType()));
    }

    @DeleteMapping("")
    @ResponseStatus(HttpStatus.OK)
    public void removeGroup(@RequestParam @Min(1) int id) {
        if (isGroupActual(id)) {
            handler.removeGroup(id);
        }
    }

    @PutMapping("/{groupId}")
    @Validated(OnUpdate.class)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateGroup(@Valid GroupDto groupDto,
                            @PathVariable @Min(1) int groupId) {
        if (isGroupActual(groupId)) {
            handler.updateGroup(
                new EquipmentGroup(groupDto.getGroupName(), groupDto.getType()),
                groupId);
        }
    }

    @GetMapping("/{groupId}")
    @ResponseStatus(HttpStatus.OK)
    public EquipmentGroup getGroupById(@PathVariable @Min(1) int groupId) {
        if (isGroupActual(groupId)) {
            return handler.getGroupById(groupId);
        }
        throw new PageNotFoundException();
    }

    private boolean isGroupActual(int id) {
        if (handler.getAllGroups()
            .stream().anyMatch(c -> c.getId() == id)) {
            return true;
        } else {
            logger.warn("There is no EquipmentType with id " + id);
            throw new NoSuchElementException("There is no EquipmentType with id " + id);
        }
    }
}
