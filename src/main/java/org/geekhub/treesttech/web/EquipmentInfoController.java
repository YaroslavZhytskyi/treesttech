package org.geekhub.treesttech.web;

import org.geekhub.treesttech.dao.equipment.EquipmentInfoHandler;
import org.geekhub.treesttech.model.User;
import org.geekhub.treesttech.model.equipment.EquipmentInfo;
import org.geekhub.treesttech.service.company.CompanyService;
import org.geekhub.treesttech.service.user.UserService;
import org.geekhub.treesttech.service.validator.ComponentValidator;
import org.geekhub.treesttech.service.validator.DateValidator;
import org.geekhub.treesttech.web.dto.equipment.EquipmentInfoDto;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Min;
import java.time.LocalDate;

@RestController
@Validated
@RequestMapping("/equipment/")
public class EquipmentInfoController {

    private static final String DEFAULT_DATE = "1900-01-01";

    private final DateValidator dateValidator;
    private final ComponentValidator componentValidator;
    private final EquipmentInfoHandler infoHandler;
    private final UserService userService;
    private final CompanyService companyService;

    public EquipmentInfoController(DateValidator dateValidator,
                                   ComponentValidator componentValidator,
                                   EquipmentInfoHandler infoHandler,
                                   UserService userService,
                                   CompanyService companyService) {
        this.dateValidator = dateValidator;
        this.componentValidator = componentValidator;
        this.infoHandler = infoHandler;
        this.userService = userService;
        this.companyService = companyService;
    }

    @GetMapping("{companyId}/{equipmentId}/info")
    @Validated
    @ResponseStatus(HttpStatus.OK)
    public EquipmentInfo getEquipmentInfo(
        @PathVariable int companyId, @PathVariable @Min(1) int equipmentId) {
        if (isCompanyEquipmentExistAndAccessible(companyId, equipmentId)) {
            return infoHandler.getEquipmentInfo(equipmentId);
        }
        return null;
    }

    @PutMapping("{companyId}/{equipmentId}/info")
    @Validated
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateEquipmentInfo(@PathVariable @Min(1) int companyId,
                                    @PathVariable @Min(1) int equipmentId,
                                    EquipmentInfoDto infoDto) {
        if (isCompanyEquipmentExistAndAccessible(companyId, equipmentId)) {
            LocalDate purchaseDate = LocalDate.parse(DEFAULT_DATE);
            if (dateValidator.isValid(infoDto.getPurchaseDate())) {
                purchaseDate = LocalDate.parse(infoDto.getPurchaseDate());
            }
            infoHandler.updateEquipmentInfo(
                new EquipmentInfo(
                    infoDto.getInventoryNumber(),
                    infoDto.getSerialNumber(),
                    purchaseDate,
                    infoDto.getNote(),
                    equipmentId
                ), equipmentId
            );
        }
    }

    private boolean isCompanyEquipmentExistAndAccessible(int companyId, int equipmentId) {
        if (companyService.isCompanyExist(companyId)
            && componentValidator.isEquipmentExist(companyId, equipmentId)) {
            User user = userService.getCurrentUser();
            return companyService.hasAccessToCompany(user, companyId);
        }
        return false;
    }
}
