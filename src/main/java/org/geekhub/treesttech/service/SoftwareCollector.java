package org.geekhub.treesttech.service;

import org.geekhub.treesttech.dao.software.OsHandler;
import org.geekhub.treesttech.model.software.Software;
import org.springframework.stereotype.Service;

@Service
public class SoftwareCollector implements SoftwareCollectorHandler {

    private final OsHandler osHandler;

    public SoftwareCollector(OsHandler osHandler) {
        this.osHandler = osHandler;
    }

    @Override
    public Software getSoftware(int equipmentId) {
        return new Software(osHandler.getOsById(equipmentId),
            equipmentId);
    }
}
