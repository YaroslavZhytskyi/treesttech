package org.geekhub.treesttech.service;

import org.geekhub.treesttech.model.hardware.Hardware;

public interface HardwareCollectorHandler {

    Hardware getHardware(int equipmentId);
}
