package org.geekhub.treesttech.service;

import org.geekhub.treesttech.model.software.Software;

public interface SoftwareCollectorHandler {

    Software getSoftware(int equipmentId);
}
