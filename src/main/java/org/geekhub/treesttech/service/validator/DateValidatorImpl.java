package org.geekhub.treesttech.service.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

@Service
public class DateValidatorImpl implements DateValidator {

    private static final Logger logger = LoggerFactory.getLogger(DateValidatorImpl.class);

    private final DateTimeFormatter dateFormatter;

    public DateValidatorImpl(DateTimeFormatter dateFormatter) {
        this.dateFormatter = dateFormatter;
    }

    @Override
    public boolean isValid(String dateStr) {
        try {
            LocalDate.parse(dateStr, this.dateFormatter);
        } catch (NullPointerException | DateTimeParseException e) {
            logger.warn(e.getMessage(), e);
            return false;
        }
        return true;
    }
}
