package org.geekhub.treesttech.service.validator;

public interface DateValidator {

    boolean isValid(String dateStr);
}
