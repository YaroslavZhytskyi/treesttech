package org.geekhub.treesttech.service.validator;

import org.geekhub.treesttech.model.User;

public interface AccessValidator {

    boolean hasFullAccess(User user);
}
