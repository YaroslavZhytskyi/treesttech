package org.geekhub.treesttech.service.validator;

import org.geekhub.treesttech.dao.equipment.EquipmentCommonHandler;
import org.geekhub.treesttech.dao.group.GroupHandler;
import org.geekhub.treesttech.dao.hardware.HardwareDetailsHandler;
import org.geekhub.treesttech.model.hardware.Cpu;
import org.geekhub.treesttech.model.hardware.PowerSupply;
import org.geekhub.treesttech.model.hardware.Ram;
import org.geekhub.treesttech.model.hardware.Rom;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class ComponentValidatorImpl implements ComponentValidator {

    private final GroupHandler groupHandler;
    private final HardwareDetailsHandler<Ram> ramHandler;
    private final HardwareDetailsHandler<Rom> romHandler;
    private final HardwareDetailsHandler<Cpu> cpuHandler;
    private final HardwareDetailsHandler<PowerSupply> powerHandler;
    private final EquipmentCommonHandler equipmentHandler;

    public ComponentValidatorImpl(GroupHandler groupHandler,
                                  HardwareDetailsHandler<Ram> ramHandler,
                                  HardwareDetailsHandler<Rom> romHandler,
                                  HardwareDetailsHandler<Cpu> cpuHandler,
                                  HardwareDetailsHandler<PowerSupply> powerHandler,
                                  EquipmentCommonHandler equipmentHandler) {
        this.groupHandler = groupHandler;
        this.ramHandler = ramHandler;
        this.romHandler = romHandler;
        this.cpuHandler = cpuHandler;
        this.powerHandler = powerHandler;
        this.equipmentHandler = equipmentHandler;
    }

    @Override
    public boolean isGroupExist(int groupId) {
        if (groupHandler.getAllGroups().stream().anyMatch(g -> g.getId() == groupId)) {
            return true;
        } else {
            String warningMsg = String.format("There is no group with id %d", groupId);
            throw new NoSuchElementException(warningMsg);
        }
    }

    @Override
    public boolean isEquipmentExist(int companyId, int equipmentId) {
        List<Integer> allEq = equipmentHandler.getAllEquipmentIdByCompany(companyId);
        if (allEq.contains(equipmentId)) {
            return true;
        } else {
            String warningMsg = String.format("There is no equipment with id %d", equipmentId);
            throw new NoSuchElementException(warningMsg);
        }
    }

    @Override
    public boolean isRamExist(int equipmentId, int ramId) {
        List<Integer> allEq = ramHandler.getDetailsIdByEquipment(equipmentId);
        if (allEq.contains(ramId)) {
            return true;
        } else {
            String warningMsg = String.format("There is no ram with id %d", ramId);
            throw new NoSuchElementException(warningMsg);
        }
    }

    @Override
    public boolean isRomExist(int equipmentId, int romId) {
        List<Integer> allEq = romHandler.getDetailsIdByEquipment(equipmentId);
        if (allEq.contains(romId)) {
            return true;
        } else {
            String warningMsg = String.format("There is no rom with id %d", romId);
            throw new NoSuchElementException(warningMsg);
        }
    }

    @Override
    public boolean isCpuExist(int equipmentId, int cpuId) {
        List<Integer> allEq = cpuHandler.getDetailsIdByEquipment(equipmentId);
        if (allEq.contains(cpuId)) {
            return true;
        } else {
            String warningMsg = String.format("There is no cpu with id %d", cpuId);
            throw new NoSuchElementException(warningMsg);
        }
    }

    @Override
    public boolean isPowerExist(int equipmentId, int powerId) {
        List<Integer> allEq = powerHandler.getDetailsIdByEquipment(equipmentId);
        if (allEq.contains(powerId)) {
            return true;
        } else {
            String warningMsg = String.format("There is no power supply with id %d", powerId);
            throw new NoSuchElementException(warningMsg);
        }
    }
}
