package org.geekhub.treesttech.service.validator;

import org.geekhub.treesttech.model.User;
import org.springframework.stereotype.Service;

@Service
public class AccessValidatorImpl implements AccessValidator {

    private static final String ADMIN = "ROLE_ADMIN";
    private static final String SUPER = "ROLE_SUPER_ADMIN";

    @Override
    public boolean hasFullAccess(User user) {
        return (user.getRole().equals(ADMIN) || user.getRole().equals(SUPER));
    }
}
