package org.geekhub.treesttech.service.validator;

public interface ComponentValidator {

    boolean isGroupExist(int groupId);

    boolean isEquipmentExist(int companyId, int equipmentId);

    boolean isRamExist(int equipmentId, int ramId);

    boolean isRomExist(int equipmentId, int romId);

    boolean isCpuExist(int equipmentId, int cpuId);

    boolean isPowerExist(int equipmentId, int powerId);
}
