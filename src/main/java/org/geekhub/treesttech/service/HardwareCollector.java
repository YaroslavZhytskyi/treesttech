package org.geekhub.treesttech.service;

import org.geekhub.treesttech.dao.hardware.HardwareDetailsHandler;
import org.geekhub.treesttech.dao.hardware.MotherBoardHandler;
import org.geekhub.treesttech.model.hardware.Cpu;
import org.geekhub.treesttech.model.hardware.Hardware;
import org.geekhub.treesttech.model.hardware.PowerSupply;
import org.geekhub.treesttech.model.hardware.Ram;
import org.geekhub.treesttech.model.hardware.Rom;
import org.springframework.stereotype.Service;

@Service
public class HardwareCollector implements HardwareCollectorHandler {

    private final MotherBoardHandler motherBoardHandler;
    private final HardwareDetailsHandler<Ram> ramHandler;
    private final HardwareDetailsHandler<Rom> romHandler;
    private final HardwareDetailsHandler<Cpu> cpuHandler;
    private final HardwareDetailsHandler<PowerSupply> powerHandler;

    public HardwareCollector(MotherBoardHandler motherBoardHandler,
                             HardwareDetailsHandler<Ram> ramHandler,
                             HardwareDetailsHandler<Rom> romHandler,
                             HardwareDetailsHandler<Cpu> cpuHandler,
                             HardwareDetailsHandler<PowerSupply> powerHandler) {
        this.motherBoardHandler = motherBoardHandler;
        this.ramHandler = ramHandler;
        this.romHandler = romHandler;
        this.cpuHandler = cpuHandler;
        this.powerHandler = powerHandler;
    }

    @Override
    public Hardware getHardware(int equipmentId) {
        return new Hardware(
            cpuHandler.getDetailsByEquipmentId(equipmentId),
            ramHandler.getDetailsByEquipmentId(equipmentId),
            motherBoardHandler.getMotherBoardById(equipmentId),
            romHandler.getDetailsByEquipmentId(equipmentId),
            powerHandler.getDetailsByEquipmentId(equipmentId),
            equipmentId
        );
    }
}
