package org.geekhub.treesttech.service.company;

import org.geekhub.treesttech.model.User;
import org.geekhub.treesttech.model.company.Company;

import java.util.List;

public interface CompanyService {

    List<Company> getCompaniesByUser();

    void addCompany(Company company);

    void assignCompanyToUser(int companyId, int userId);

    void removeCompany(int companyId);

    void updateCompany(Company company, int companyId);

    boolean isCompanyExist(int companyId);

    Company getCompanyById(int companyId);

    Company getCompanyByIdAndUser(int companyId, int userId);

    boolean hasAccessToCompany(User user, int companyId);
}
