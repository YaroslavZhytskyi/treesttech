package org.geekhub.treesttech.service.company;

import org.geekhub.treesttech.dao.company.CompanyHandler;
import org.geekhub.treesttech.model.User;
import org.geekhub.treesttech.model.company.Company;
import org.geekhub.treesttech.service.user.UserService;
import org.geekhub.treesttech.service.validator.AccessValidator;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class CompanyServiceImpl implements CompanyService {

    private final CompanyHandler handler;
    private final UserService userService;
    private final AccessValidator accessValidator;

    public CompanyServiceImpl(CompanyHandler handler,
                              UserService userService,
                              AccessValidator accessValidator) {
        this.handler = handler;
        this.userService = userService;
        this.accessValidator = accessValidator;
    }

    @Override
    public List<Company> getCompaniesByUser() {
        User user = userService.getCurrentUser();
        if (accessValidator.hasFullAccess(user)) {
            return handler.getAllCompanies();
        }
        return handler.getCompaniesByUserId(user.getId());
    }

    @Override
    public void addCompany(Company company) {
        User user = userService.getCurrentUser();
        handler.addCompany(company, user.getId());
    }

    @Override
    public void assignCompanyToUser(int companyId, int userId) {
        if (accessValidator.hasFullAccess(userService.getCurrentUser())) {
            if (isCompanyExist(companyId)) {
                handler.assignCompanyToUser(companyId, userId);
            }
        } else {
            throw new AccessDeniedException("Permission denied");
        }
    }

    @Override
    public void removeCompany(int companyId) {
        if (isCompanyExist(companyId)) {
            User user = userService.getCurrentUser();
            if (hasAccessToCompany(user, companyId)) {
                handler.removeCompany(companyId);
            }
        }
    }

    @Override
    public void updateCompany(Company company, int companyId) {
        if (isCompanyExist(companyId)) {
            User user = userService.getCurrentUser();
            if (hasAccessToCompany(user, companyId)) {
                handler.updateCompany(company, companyId);
            }
        }
    }

    @Override
    public Company getCompanyById(int companyId) {
        User user = userService.getCurrentUser();
        if (hasAccessToCompany(user, companyId)) {
            try {
                return handler.getCompanyById(companyId);
            } catch (EmptyResultDataAccessException e) {
                String notFoundMsg = String.format("There is no company with id - %d", companyId);
                throw new NoSuchElementException(notFoundMsg);
            }
        }
        String notFoundMsg = String.format("There is no company with id - %d", companyId);
        throw new NoSuchElementException(notFoundMsg);
    }

    @Override
    public Company getCompanyByIdAndUser(int companyId, int userId) {
        try {
            return handler.getCompanyByIdAndUser(companyId, userId);
        } catch (EmptyResultDataAccessException e) {
            String notFoundMsg = String.format("There is no company with id - %d", companyId);
            throw new NoSuchElementException(notFoundMsg);
        }
    }

    @Override
    public boolean isCompanyExist(int companyId) {
        if (handler.getCompanyById(companyId) != null) {
            return true;
        } else {
            String warningMsg = String.format("There is no company with id %d", companyId);
            throw new NoSuchElementException(warningMsg);
        }
    }

    @Override
    public boolean hasAccessToCompany(User user, int companyId) {
        if (accessValidator.hasFullAccess(user)) {
            return true;
        }

        if (getCompanyByIdAndUser(user.getId(), companyId) != null) {
            return true;
        }

        String warningMsg = String.format(
            "You don't have access to company with id %d", companyId);
        throw new AccessDeniedException(warningMsg);
    }
}
