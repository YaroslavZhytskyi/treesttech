package org.geekhub.treesttech.service;

import org.geekhub.treesttech.dao.company.CompanyHandler;
import org.geekhub.treesttech.dao.equipment.EquipmentCommonHandler;
import org.geekhub.treesttech.dao.equipment.EquipmentInfoHandler;
import org.geekhub.treesttech.dao.group.GroupHandler;
import org.geekhub.treesttech.model.equipment.Equipment;
import org.geekhub.treesttech.model.equipment.EquipmentCommon;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class EquipmentCollector implements EquipmentHandler {

    private final EquipmentCommonHandler commonHandler;
    private final CompanyHandler companyHandler;
    private final GroupHandler groupHandler;
    private final HardwareCollectorHandler hardwareCollectorHandler;
    private final SoftwareCollectorHandler softwareCollectorHandler;
    private final EquipmentInfoHandler infoHandler;

    public EquipmentCollector(EquipmentCommonHandler commonHandler,
                              CompanyHandler companyHandler,
                              GroupHandler groupHandler,
                              HardwareCollectorHandler hardwareCollectorHandler,
                              SoftwareCollectorHandler softwareCollectorHandler,
                              EquipmentInfoHandler infoHandler) {
        this.commonHandler = commonHandler;
        this.companyHandler = companyHandler;
        this.groupHandler = groupHandler;
        this.hardwareCollectorHandler = hardwareCollectorHandler;
        this.softwareCollectorHandler = softwareCollectorHandler;
        this.infoHandler = infoHandler;
    }

    @Override
    public Equipment getEquipmentById(int id) {
        EquipmentCommon common = commonHandler.getEquipmentById(id);
        if (common == null) {
            String msg = String.format("There is no equipment with id - %d", id);
            throw new NoSuchElementException(msg);
        }
        return new Equipment(
            common.getName(),
            companyHandler.getCompanyById(common.getCompanyId()),
            groupHandler.getGroupById(common.getGroupId()),
            infoHandler.getEquipmentInfo(id),
            hardwareCollectorHandler.getHardware(id),
            softwareCollectorHandler.getSoftware(id)
        );
    }

    @Override
    public List<EquipmentCommon> getAllEquipment(int companyId) {
        return commonHandler.getAllEquipmentByCompanyId(companyId);
    }
}
