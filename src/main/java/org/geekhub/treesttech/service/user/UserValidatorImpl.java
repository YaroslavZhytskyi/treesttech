package org.geekhub.treesttech.service.user;

import org.geekhub.treesttech.dao.user.UserHandler;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
public class UserValidatorImpl implements UserValidator {

    private final UserHandler userHandler;

    public UserValidatorImpl(UserHandler userHandler) {
        this.userHandler = userHandler;
    }

    @Override
    public boolean isUserExist(int id) {
        try {
            userHandler.getUserId(id);
        } catch (EmptyResultDataAccessException e) {
            String notFoundMsg = String.format("There is no user with id - %d", id);
            throw new NoSuchElementException(notFoundMsg);
        }
        return true;
    }
}
