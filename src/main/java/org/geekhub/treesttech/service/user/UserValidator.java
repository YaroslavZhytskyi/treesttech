package org.geekhub.treesttech.service.user;

public interface UserValidator {

    boolean isUserExist(int id);
}
