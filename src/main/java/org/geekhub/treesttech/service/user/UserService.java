package org.geekhub.treesttech.service.user;

import org.geekhub.treesttech.model.User;

import java.util.List;

public interface UserService {

    User getCurrentUser();

    List<User> getAllUsers();

    void createUser(User user);

    void updateUser(User user, int id);

    void removeUser(int id);
}
