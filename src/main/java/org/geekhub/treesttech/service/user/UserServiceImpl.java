package org.geekhub.treesttech.service.user;

import org.geekhub.treesttech.dao.user.UserHandler;
import org.geekhub.treesttech.model.User;
import org.geekhub.treesttech.security.CustomUserDetails;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserHandler userHandler;
    private final UserValidator userValidator;

    public UserServiceImpl(UserHandler userHandler, UserValidator userValidator) {
        this.userHandler = userHandler;
        this.userValidator = userValidator;
    }

    @Override
    public User getCurrentUser() {
        CustomUserDetails userDetails = (CustomUserDetails) SecurityContextHolder
            .getContext().getAuthentication().getPrincipal();
        return userDetails.getUser();
    }

    @Override
    public List<User> getAllUsers() {
        return userHandler.getAllUsers();
    }

    @Override
    public void createUser(User user) {
        userHandler.addUser(user);
    }

    @Override
    public void updateUser(User user, int id) {
        if (userValidator.isUserExist(id)) {
            userHandler.updateUser(user, id);
        }
    }

    @Override
    public void removeUser(int id) {
        if (userValidator.isUserExist(id)) {
            userHandler.removeUserById(id);
        }
    }
}
