package org.geekhub.treesttech.service;

import org.geekhub.treesttech.model.equipment.Equipment;
import org.geekhub.treesttech.model.equipment.EquipmentCommon;

import java.util.List;

public interface EquipmentHandler {

    Equipment getEquipmentById(int id);

    List<EquipmentCommon> getAllEquipment(int companyId);
}
