package org.geekhub.treesttech.service;

public interface SqlFileHandler {

    String readSql(String fileName);
}
