package org.geekhub.treesttech.service;

import org.geekhub.treesttech.exception.FileReadException;
import org.geekhub.treesttech.exception.SqlScriptNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class SqlFileHandlerImpl implements SqlFileHandler {

    private static final Logger logger = LoggerFactory.getLogger(SqlFileHandlerImpl.class);

    @Override
    public String readSql(String fileName) {
        String sql;
        try (InputStream is =
                 SqlFileHandlerImpl.class.getClassLoader().getResourceAsStream(fileName)) {
            if (is == null) {
                String errMsg =
                    String.format("Sql script is not found with such name - %s", fileName);
                logger.error(errMsg);
                throw new SqlScriptNotFoundException(errMsg);
            }
            sql = Stream.of(is.readAllBytes()).map(String::new).collect(Collectors.joining());
        } catch (IOException e) {
            String errMsg = String.format("Error with reading file %s", fileName);
            logger.error(errMsg, e);
            throw new FileReadException(errMsg);
        }
        return sql;
    }
}
