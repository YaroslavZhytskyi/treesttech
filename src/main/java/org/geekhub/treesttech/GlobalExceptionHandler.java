package org.geekhub.treesttech;

import org.geekhub.treesttech.exception.ElementCountException;
import org.geekhub.treesttech.exception.EquipmentNotFoundException;
import org.geekhub.treesttech.exception.PageNotFoundException;
import org.geekhub.treesttech.model.ErrorMessage;
import org.geekhub.treesttech.web.dto.ErrorDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Path.Node;
import java.time.LocalDate;
import java.util.NoSuchElementException;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> onConstraintValidationException(
        ConstraintViolationException e) {
        StringBuilder sb = new StringBuilder();
        for (ConstraintViolation<?> violation : e.getConstraintViolations()) {
            String fieldName = "";
            for (Node node : violation.getPropertyPath()) {
                fieldName = node.getName();
            }
            sb.append(String.format("%s - %s", fieldName, violation.getMessage()));
        }
        logger.warn(sb.toString());
        ErrorDto errorDto = new ErrorDto(BAD_REQUEST, sb.toString());
        return buildResponseEntity(errorDto);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> onMethodArgumentNotValidException(
        MethodArgumentNotValidException e) {
        StringBuilder sb = new StringBuilder();
        for (FieldError fieldError : e.getBindingResult().getFieldErrors()) {
            sb.append(String.format("%s - %s",
                fieldError.getField(), fieldError.getDefaultMessage()));
        }
        logger.warn(sb.toString());
        ErrorDto errorDto = new ErrorDto(BAD_REQUEST, sb.toString());
        return buildResponseEntity(errorDto);
    }

    @ExceptionHandler({NoSuchElementException.class,
        PageNotFoundException.class, EquipmentNotFoundException.class})
    public ResponseEntity<Object> noSuchElement(RuntimeException ex) {
        ErrorDto errorDto = new ErrorDto(NOT_FOUND, ex.getMessage());
        logger.warn(ex.getMessage(), ex);
        return buildResponseEntity(errorDto);
    }

    @ExceptionHandler(ElementCountException.class)
    public ResponseEntity<Object> countException(ElementCountException ex) {
        ErrorDto errorDto = new ErrorDto(BAD_REQUEST, ex.getMessage());
        logger.warn(ex.getMessage(), ex);
        return buildResponseEntity(errorDto);
    }

    @ExceptionHandler(DuplicateKeyException.class)
    public ResponseEntity<Object> duplicateKey(RuntimeException ex) {
        logger.warn(ex.getMessage(), ex);
        String errorMsgDto = "Duplicate values";
        ErrorDto errorDto = new ErrorDto(CONFLICT, errorMsgDto);
        return buildResponseEntity(errorDto);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<Object> accessDenied(Exception ex) {
        ErrorDto errorDto = new ErrorDto(FORBIDDEN, ex.getMessage());
        logger.warn(ex.getMessage(), ex);
        return buildResponseEntity(errorDto);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> globalExceptionHandler(Exception ex, WebRequest request) {
        String exceptionMsg = ex.getMessage();
        ErrorMessage message = new ErrorMessage(
            INTERNAL_SERVER_ERROR.value(),
            LocalDate.now(),
            exceptionMsg,
            request.getDescription(false));
        logger.error(message.toString());

        ErrorDto errorDto = new ErrorDto(INTERNAL_SERVER_ERROR, exceptionMsg);
        return buildResponseEntity(errorDto);
    }

    private ResponseEntity<Object> buildResponseEntity(ErrorDto errorDto) {
        return new ResponseEntity<>(errorDto, errorDto.getStatus());
    }
}
