package org.geekhub.treesttech.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import javax.sql.DataSource;
import org.flywaydb.core.Flyway;
import org.geekhub.treesttech.dao.ConfigHandler;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.time.format.DateTimeFormatter;


@Configuration
@PropertySource("classpath:db.properties")
public class AppConfig {

    @Bean
    public DataSource dataSource(ConfigHandler configHandler) {
        HikariConfig config = configHandler.getConfig();
        config.setPoolName("TreestTechPool");
        config.setMaximumPoolSize(10);
        config.setMaxLifetime(60000);
        config.setConnectionTimeout(500);
        return new HikariDataSource(config);
    }

    @Bean(name = "jdbc-Template")
    public NamedParameterJdbcTemplate jdbcTemplate(DataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }

    @Bean
    public Flyway flyway(DataSource dataSource) {
        return Flyway.configure()
            .baselineOnMigrate(true)
            .locations("classpath:db/migration")
            .dataSource(dataSource)
            .load();
    }

    @Bean
    public InitializingBean flywayMigrate(Flyway flyway) {
        return flyway::migrate;
    }

    @Bean
    public DateTimeFormatter dateTimeFormatter() {
        return DateTimeFormatter.ofPattern("yyyy-MM-dd");
    }
}
