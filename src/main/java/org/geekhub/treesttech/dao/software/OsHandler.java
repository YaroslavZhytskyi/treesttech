package org.geekhub.treesttech.dao.software;

import org.geekhub.treesttech.model.software.OperatingSystem;

public interface OsHandler {

    OperatingSystem getOsById(int id);

    void addOs(OperatingSystem os);

    void removeOs(int id);

    void updateOs(OperatingSystem os, int id);
}
