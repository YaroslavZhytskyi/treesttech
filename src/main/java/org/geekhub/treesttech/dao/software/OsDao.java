package org.geekhub.treesttech.dao.software;

import org.geekhub.treesttech.model.software.OperatingSystem;
import org.geekhub.treesttech.model.software.OperatingSystemBit;
import org.geekhub.treesttech.service.SqlFileHandler;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class OsDao implements OsHandler {

    private static final String PATH_PREFIX = "sql/software/";

    private static final String SQL_GET_OS_BY_EQUIPMENT_ID = PATH_PREFIX + "GetOsByEquipmentId.sql";

    private static final String SQL_ADD_OS = PATH_PREFIX + "AddOs.sql";

    private static final String SQL_REMOVE_OS_BY_ID = PATH_PREFIX + "RemoveOsById.sql";

    private static final String SQL_UPDATE_OS = PATH_PREFIX + "UpdateOs.sql";

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final SqlFileHandler sqlFileHandler;

    public OsDao(NamedParameterJdbcTemplate jdbcTemplate, SqlFileHandler sqlFileHandler) {
        this.jdbcTemplate = jdbcTemplate;
        this.sqlFileHandler = sqlFileHandler;
    }

    @Override
    public OperatingSystem getOsById(int id) {
        return jdbcTemplate.queryForObject(sqlFileHandler.readSql(SQL_GET_OS_BY_EQUIPMENT_ID),
            Map.of("id", id),
            (rs, rowNum) -> new OperatingSystem(
                rs.getString("os_name"),
                OperatingSystemBit.from(rs.getString("os_bit")),
                rs.getInt("equipment_id"))
        );
    }

    @Override
    public void addOs(OperatingSystem os) {
        MapSqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("name", os.getName())
            .addValue("bit", os.getBit().name())
            .addValue("equipmentId", os.getEquipmentId());
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_ADD_OS), parameters);
    }

    @Override
    public void removeOs(int id) {
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_REMOVE_OS_BY_ID),
            Map.of("id", id));
    }

    @Override
    public void updateOs(OperatingSystem os, int id) {
        MapSqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("name", os.getName())
            .addValue("bit", os.getBit().name())
            .addValue("newId", os.getEquipmentId())
            .addValue("equipmentId", id);

        jdbcTemplate.update(sqlFileHandler.readSql(SQL_UPDATE_OS), parameters);
    }
}
