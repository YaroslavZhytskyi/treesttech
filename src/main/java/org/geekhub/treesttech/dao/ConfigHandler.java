package org.geekhub.treesttech.dao;

import com.zaxxer.hikari.HikariConfig;

public interface ConfigHandler {

    HikariConfig getConfig();
}
