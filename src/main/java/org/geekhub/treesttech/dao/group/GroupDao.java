package org.geekhub.treesttech.dao.group;

import org.geekhub.treesttech.model.equipment.EquipmentGroup;
import org.geekhub.treesttech.model.equipment.EquipmentType;
import org.geekhub.treesttech.service.SqlFileHandler;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class GroupDao implements GroupHandler {

    private static final String PATH_PREFIX = "sql/group/";

    private static final String SQL_GET_GROUP_BY_ID = PATH_PREFIX + "GetGroupById.sql";

    private static final String SQL_GET_ALL_GROUPS = PATH_PREFIX + "GetAllGroups.sql";

    private static final String SQL_ADD_GROUP = PATH_PREFIX + "AddGroup.sql";

    private static final String SQL_REMOVE_GROUP = PATH_PREFIX + "RemoveGroup.sql";

    private static final String SQL_UPDATE_GROUP = PATH_PREFIX + "UpdateGroup.sql";

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final SqlFileHandler sqlFileHandler;

    public GroupDao(NamedParameterJdbcTemplate jdbcTemplate, SqlFileHandler sqlFileHandler) {
        this.jdbcTemplate = jdbcTemplate;
        this.sqlFileHandler = sqlFileHandler;
    }

    @Override
    public EquipmentGroup getGroupById(int id) {
        return jdbcTemplate.queryForObject(sqlFileHandler.readSql(SQL_GET_GROUP_BY_ID),
            Map.of("id", id),
            (rs, rowNum) -> {
                var entity = new EquipmentGroup(
                    rs.getString("name"),
                    EquipmentType.from(rs.getString("type")));
                entity.setId(rs.getInt("id"));
                return entity;
            }
        );
    }

    @Override
    public List<EquipmentGroup> getAllGroups() {
        return jdbcTemplate.query(sqlFileHandler.readSql(SQL_GET_ALL_GROUPS),
            (rs, rowNum) -> {
                var entity = new EquipmentGroup(
                    rs.getString("name"),
                    EquipmentType.from(rs.getString("type")));
                entity.setId(rs.getInt("id"));
                return entity;
            }
        );
    }

    @Override
    public void addGroup(EquipmentGroup group) {
        MapSqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("name", group.getName())
            .addValue("type", group.getType().name());
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_ADD_GROUP), parameters);
    }

    @Override
    public void removeGroup(int id) {
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_REMOVE_GROUP),
            Map.of("id", id));
    }

    @Override
    public void updateGroup(EquipmentGroup group, int id) {
        MapSqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("name", group.getName())
            .addValue("type", group.getType().name())
            .addValue("id", id);
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_UPDATE_GROUP), parameters);
    }
}
