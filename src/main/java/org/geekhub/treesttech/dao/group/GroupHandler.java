package org.geekhub.treesttech.dao.group;

import org.geekhub.treesttech.model.equipment.EquipmentGroup;

import java.util.List;

public interface GroupHandler {

    EquipmentGroup getGroupById(int id);

    List<EquipmentGroup> getAllGroups();

    void addGroup(EquipmentGroup group);

    void removeGroup(int id);

    void updateGroup(EquipmentGroup group, int id);
}
