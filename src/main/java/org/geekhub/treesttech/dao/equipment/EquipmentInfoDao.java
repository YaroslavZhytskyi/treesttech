package org.geekhub.treesttech.dao.equipment;

import org.geekhub.treesttech.model.equipment.EquipmentInfo;
import org.geekhub.treesttech.service.SqlFileHandler;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Map;

@Repository
public class EquipmentInfoDao implements EquipmentInfoHandler {

    private static final String PATH_PREFIX = "sql/info/";

    private static final String DEFAULT_DATE = "1900-01-01";

    private static final String SQL_GET_EQUIPMENT_INFO = PATH_PREFIX + "GetEquipmentInfo.sql";

    private static final String SQL_ADD_EQUIPMENT_INFO = PATH_PREFIX + "AddEquipmentInfo.sql";

    private static final String SQL_REMOVE_EQUIPMENT_INFO = PATH_PREFIX + "RemoveEquipmentInfo.sql";

    private static final String SQL_UPDATE_EQUIPMENT_INFO = PATH_PREFIX + "UpdateEquipmentInfo.sql";

    private static final String SQL_ADD_EMPTY_EQUIPMENT_INFO =
        PATH_PREFIX + "AddEmptyEquipmentInfo.sql";

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final SqlFileHandler sqlFileHandler;

    public EquipmentInfoDao(NamedParameterJdbcTemplate jdbcTemplate,
                            SqlFileHandler sqlFileHandler) {
        this.jdbcTemplate = jdbcTemplate;
        this.sqlFileHandler = sqlFileHandler;
    }

    @Override
    public EquipmentInfo getEquipmentInfo(int id) {
        return jdbcTemplate.queryForObject(sqlFileHandler.readSql(SQL_GET_EQUIPMENT_INFO),
            Map.of("id", id),
            (rs, rowNum) -> new EquipmentInfo(
                rs.getString("inventory_number"),
                rs.getString("serial_number"),
                LocalDate.parse(rs.getString("purchase_date")),
                rs.getString("note"),
                rs.getInt("equipment_id"))
        );
    }

    @Override
    public void addEquipmentInfo(EquipmentInfo info) {
        MapSqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("inventory_number", info.getInventoryNumber())
            .addValue("serial_number", info.getSerialNumber())
            .addValue("purchase_date", info.getPurchaseDate().toString())
            .addValue("note", info.getNote())
            .addValue("equipment_id", info.getEquipmentId());
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_ADD_EQUIPMENT_INFO), parameters);
    }

    @Override
    public void removeEquipmentInfo(int id) {
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_REMOVE_EQUIPMENT_INFO),
            Map.of("equipmentId", id));
    }

    @Override
    public void updateEquipmentInfo(EquipmentInfo info, int id) {
        MapSqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("inventory_number", info.getInventoryNumber())
            .addValue("serial_number", info.getSerialNumber())
            .addValue("purchase_date", info.getPurchaseDate().toString())
            .addValue("note", info.getNote())
            .addValue("id", id);
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_UPDATE_EQUIPMENT_INFO), parameters);
    }

    @Override
    public void addEmptyEquipmentInfo(int equipmentId) {
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_ADD_EMPTY_EQUIPMENT_INFO),
            Map.of("default_purchase_date", DEFAULT_DATE,
                "equipment_id", equipmentId));
    }
}
