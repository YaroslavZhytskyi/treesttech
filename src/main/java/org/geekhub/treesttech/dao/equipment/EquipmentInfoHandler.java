package org.geekhub.treesttech.dao.equipment;

import org.geekhub.treesttech.model.equipment.EquipmentInfo;

public interface EquipmentInfoHandler {

    EquipmentInfo getEquipmentInfo(int id);

    void addEquipmentInfo(EquipmentInfo equipmentInfo);

    void removeEquipmentInfo(int id);

    void updateEquipmentInfo(EquipmentInfo equipmentInfo, int id);

    void addEmptyEquipmentInfo(int equipmentId);
}
