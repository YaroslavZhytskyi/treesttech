package org.geekhub.treesttech.dao.equipment;

import org.geekhub.treesttech.model.equipment.EquipmentCommon;

import java.util.List;

public interface EquipmentCommonHandler {

    EquipmentCommon getEquipmentById(int id);

    List<EquipmentCommon> getAllEquipment();

    List<EquipmentCommon> getAllEquipmentByCompanyId(int companyId);

    EquipmentCommon addEquipment(EquipmentCommon equipment);

    void removeEquipmentById(int id);

    void updateEquipment(EquipmentCommon equipment, int id);

    List<Integer> getAllEquipmentIdByCompany(int companyId);
}
