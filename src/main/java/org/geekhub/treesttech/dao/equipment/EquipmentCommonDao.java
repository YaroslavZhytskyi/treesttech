package org.geekhub.treesttech.dao.equipment;

import org.geekhub.treesttech.model.equipment.EquipmentCommon;
import org.geekhub.treesttech.service.SqlFileHandler;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@Repository
public class EquipmentCommonDao implements EquipmentCommonHandler {

    private static final String PATH_PREFIX = "sql/equipment/";

    private static final String SQL_GET_EQUIPMENT_BY_ID = PATH_PREFIX + "GetEquipmentById.sql";

    private static final String SQL_GET_ALL_EQUIPMENT = PATH_PREFIX + "GetAllEquipment.sql";

    private static final String SQL_GET_ALL_EQUIPMENT_BY_COMPANY = PATH_PREFIX
        + "GetAllEquipmentByCompany.sql";

    private static final String SQL_ADD_EQUIPMENT = PATH_PREFIX + "AddEquipment.sql";

    private static final String SQL_REMOVE_EQUIPMENT_BY_ID = PATH_PREFIX
        + "RemoveEquipmentById.sql";

    private static final String SQL_UPDATE_EQUIPMENT = PATH_PREFIX + "UpdateEquipment.sql";

    private static final String SQL_GET_ALL_EQUIPMENT_ID_BY_COMPANY =
        PATH_PREFIX + "GetAllEquipmentIdByCompany.sql";

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final SqlFileHandler sqlFileHandler;

    public EquipmentCommonDao(NamedParameterJdbcTemplate jdbcTemplate,
                              SqlFileHandler sqlFileHandler) {
        this.jdbcTemplate = jdbcTemplate;
        this.sqlFileHandler = sqlFileHandler;
    }

    @Override
    public EquipmentCommon getEquipmentById(int id) {
        return jdbcTemplate.queryForObject(sqlFileHandler.readSql(SQL_GET_EQUIPMENT_BY_ID),
            Map.of("id", id),
            (rs, rowNum) -> {
                var eq = new EquipmentCommon(
                    rs.getString("name"),
                    rs.getInt("equipment_group"),
                    rs.getInt("company_id"));
                eq.setId(rs.getInt("id"));
                return eq;
            }
        );
    }

    @Override
    public List<EquipmentCommon> getAllEquipment() {
        return jdbcTemplate.query(sqlFileHandler.readSql(SQL_GET_ALL_EQUIPMENT),
            (rs, rowNum) -> {
                var eqCommon = new EquipmentCommon(
                    rs.getString("name"),
                    rs.getInt("equipment_group"),
                    rs.getInt("company_id"));
                eqCommon.setId(rs.getInt("id"));
                return eqCommon;
            });
    }

    @Override
    public List<EquipmentCommon> getAllEquipmentByCompanyId(int companyId) {
        return jdbcTemplate.query(sqlFileHandler.readSql(SQL_GET_ALL_EQUIPMENT_BY_COMPANY),
            Map.of("companyId", companyId),
            (rs, rowNum) -> {
                var eqCommon = new EquipmentCommon(
                    rs.getString("name"),
                    rs.getInt("equipment_group"),
                    companyId);
                eqCommon.setId(rs.getInt("id"));
                return eqCommon;
            }
        );
    }

    @Override
    public EquipmentCommon addEquipment(EquipmentCommon equipment) {
        KeyHolder holder = new GeneratedKeyHolder();
        MapSqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("name", equipment.getName())
            .addValue("group", equipment.getGroupId())
            .addValue("companyId", equipment.getCompanyId());
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_ADD_EQUIPMENT),
            parameters,
            holder,
            new String[]{"equipment_id"});
        equipment.setId((Objects.requireNonNull(holder.getKey())).intValue());
        return equipment;
    }

    @Override
    public void removeEquipmentById(int id) {
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_REMOVE_EQUIPMENT_BY_ID),
            Map.of("id", id));
    }

    @Override
    public void updateEquipment(EquipmentCommon equipment, int id) {
        MapSqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("name", equipment.getName())
            .addValue("group", equipment.getGroupId())
            .addValue("companyId", equipment.getCompanyId())
            .addValue("id", id);
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_UPDATE_EQUIPMENT), parameters);
    }

    @Override
    public List<Integer> getAllEquipmentIdByCompany(int companyId) {
        return jdbcTemplate.query(sqlFileHandler.readSql(SQL_GET_ALL_EQUIPMENT_ID_BY_COMPANY),
            Map.of("companyId", companyId),
            (rs, rowNum) -> rs.getInt("equipment_id"));
    }
}
