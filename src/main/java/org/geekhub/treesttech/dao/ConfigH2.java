package org.geekhub.treesttech.dao;

import com.zaxxer.hikari.HikariConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("dev")
public class ConfigH2 implements ConfigHandler {

    private static final String H2_PREFIX = "jdbc:h2:mem";
    private static final String H2_DRIVER = "org.h2.Driver";

    @Value("${db.login}")
    private String dbLogin;
    @Value("${db.password}")
    private String dbPassword;
    @Value("${db.name}")
    private String dbName;

    @Override
    public HikariConfig getConfig() {
        HikariConfig config = new HikariConfig();
        String dbUrl = String.format("%s:%s", H2_PREFIX, dbName);

        config.setJdbcUrl(dbUrl);
        config.setDriverClassName(H2_DRIVER);
        config.setUsername(dbLogin);
        config.setPassword(dbPassword);
        return config;
    }
}
