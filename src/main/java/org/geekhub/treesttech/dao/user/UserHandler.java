package org.geekhub.treesttech.dao.user;

import org.geekhub.treesttech.model.User;

import java.util.List;
import java.util.Optional;

public interface UserHandler {

    List<User> getAllUsers();

    User getUserById(int id);

    User getUserByName(String name);

    void removeUserById(int id);

    void addUser(User user);

    void updateUser(User user, int id);

    void changeStateOfUser(boolean state, int id);

    List<String> getAllRoles();

    Optional<Integer> getUserId(int id);
}
