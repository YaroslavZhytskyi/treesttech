package org.geekhub.treesttech.dao.user;

import org.geekhub.treesttech.model.User;
import org.geekhub.treesttech.service.SqlFileHandler;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public class UserDao implements UserHandler {

    private static final String PATH_PREFIX = "sql/user/";

    private static final String SQL_GET_ALL_USERS = PATH_PREFIX + "GetAllUsers.sql";

    private static final String SQL_GET_USER_BY_ID = PATH_PREFIX + "GetUserById.sql";

    private static final String SQL_GET_USER_BY_NAME = PATH_PREFIX + "GetUserByName.sql";

    private static final String SQL_REMOVE_USER_BY_ID = PATH_PREFIX + "RemoveUserById.sql";

    private static final String SQL_ADD_USER = PATH_PREFIX + "AddUser.sql";

    private static final String SQL_UPDATE_USER = PATH_PREFIX + "UpdateUser.sql";

    private static final String SQL_CHANGE_STATE_USER = PATH_PREFIX + "ChangeStateUser.sql";

    private static final String SQL_GET_ALL_ROLES = PATH_PREFIX + "GetAllRoles.sql";

    private static final String SQL_GET_USER_ID = PATH_PREFIX + "GetUserId.sql";

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final SqlFileHandler sqlFileHandler;

    public UserDao(NamedParameterJdbcTemplate jdbcTemplate, SqlFileHandler sqlFileHandler) {
        this.jdbcTemplate = jdbcTemplate;
        this.sqlFileHandler = sqlFileHandler;
    }

    @Override
    public List<User> getAllUsers() {
        return jdbcTemplate.query(sqlFileHandler.readSql(SQL_GET_ALL_USERS),
            (rs, rowNum) -> {
                var user = new User(
                    rs.getString("username"),
                    rs.getString("password"),
                    rs.getString("email"),
                    rs.getString("roles"),
                    rs.getBoolean("enabled")
                );
                user.setId(rs.getInt("id"));
                return user;
            }
        );
    }

    @Override
    public User getUserById(int id) {
        return jdbcTemplate.queryForObject(sqlFileHandler.readSql(SQL_GET_USER_BY_ID),
            Map.of("id", id),
            (rs, rowNum) -> {
                var user = new User(
                    rs.getString("username"),
                    rs.getString("password"),
                    rs.getString("email"),
                    rs.getString("roles"),
                    rs.getBoolean("enabled")
                );
                user.setId(rs.getInt("id"));
                return user;
            }
        );
    }

    @Override
    public void removeUserById(int id) {
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_REMOVE_USER_BY_ID),
            Map.of("id", id));
    }

    @Override
    public void addUser(User user) {
        MapSqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("username", user.getUsername())
            .addValue("password", user.getPassword())
            .addValue("email", user.getEmail())
            .addValue("enabled", user.isEnabled())
            .addValue("role", user.getRole());
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_ADD_USER), parameters);
    }

    @Override
    public void updateUser(User user, int id) {
        MapSqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("username", user.getUsername())
            .addValue("password", user.getPassword())
            .addValue("email", user.getEmail())
            .addValue("enabled", user.isEnabled())
            .addValue("id", id)
            .addValue("role", user.getRole());
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_UPDATE_USER), parameters);
    }

    @Override
    public void changeStateOfUser(boolean state, int id) {
        MapSqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("enabled", state)
            .addValue("id", id);
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_CHANGE_STATE_USER), parameters);
    }

    @Override
    public List<String> getAllRoles() {
        return jdbcTemplate.query(sqlFileHandler.readSql(SQL_GET_ALL_ROLES),
            (rs, rowNum) ->
                rs.getString("role")
        );
    }

    @Override
    public User getUserByName(String name) {
        return jdbcTemplate.queryForObject(sqlFileHandler.readSql(SQL_GET_USER_BY_NAME),
            Map.of("name", name),
            (rs, rowNum) -> {
                var user = new User(
                    rs.getString("username"),
                    rs.getString("password"),
                    rs.getString("email"),
                    rs.getString("roles"),
                    rs.getBoolean("enabled")
                );
                user.setId(rs.getInt("id"));
                return user;
            }
        );
    }

    @Override
    public Optional<Integer> getUserId(int id) {
        return Optional.ofNullable(
            jdbcTemplate.queryForObject(sqlFileHandler.readSql(SQL_GET_USER_ID),
                Map.of("id", id),
                (rs, rowNum) -> rs.getInt("user_id")));
    }
}
