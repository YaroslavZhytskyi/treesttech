package org.geekhub.treesttech.dao.company;

import org.geekhub.treesttech.model.company.Company;
import org.geekhub.treesttech.service.SqlFileHandler;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class CompanyDao implements CompanyHandler {

    private static final String PATH_PREFIX = "sql/company/";

    private static final String SQL_GET_COMPANY_BY_ID = PATH_PREFIX + "GetCompanyById.sql";

    private static final String SQL_GET_ALL_COMPANIES = PATH_PREFIX + "GetAllCompanies.sql";

    private static final String SQL_GET_COMPANIES_BY_USER = PATH_PREFIX + "GetCompaniesByUser.sql";

    private static final String SQL_ADD_COMPANY = PATH_PREFIX + "AddCompany.sql";

    private static final String SQL_REMOVE_COMPANY_BY_ID = PATH_PREFIX + "RemoveCompanyById.sql";

    private static final String SQL_UPDATE_COMPANY = PATH_PREFIX + "UpdateCompany.sql";

    private static final String SQL_GET_COMPANY_BY_ID_AND_USER =
        PATH_PREFIX + "GetCompanyByIdAndUser.sql";

    private static final String SQL_ASSIGN_COMPANY_TO_USER =
        PATH_PREFIX + "AssignCompanyToUser.sql";

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final SqlFileHandler sqlFileHandler;

    public CompanyDao(NamedParameterJdbcTemplate jdbcTemplate, SqlFileHandler sqlFileHandler) {
        this.jdbcTemplate = jdbcTemplate;
        this.sqlFileHandler = sqlFileHandler;
    }

    @Override
    public List<Company> getAllCompanies() {
        return jdbcTemplate.query(sqlFileHandler.readSql(SQL_GET_ALL_COMPANIES),
            (rs, rowNum) -> {
                var company = new Company(
                    rs.getString("name"));
                company.setId(rs.getInt("id"));
                return company;
            }
        );
    }

    @Override
    public Company getCompanyById(int id) {
        return jdbcTemplate.queryForObject(sqlFileHandler.readSql(SQL_GET_COMPANY_BY_ID),
            Map.of("id", id),
            (rs, rowNum) -> {
                var company = new Company(
                    rs.getString("name"));
                company.setId(rs.getInt("id"));
                return company;
            }
        );
    }

    @Override
    public List<Company> getCompaniesByUserId(int userId) {
        return jdbcTemplate.query(sqlFileHandler.readSql(SQL_GET_COMPANIES_BY_USER),
            Map.of("id", userId),
            (rs, rowNum) -> {
                var company = new Company(
                    rs.getString("name"));
                company.setId(rs.getInt("id"));
                return company;
            }
        );
    }

    @Override
    public void addCompany(Company company, int userId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("companyName", company.getName())
            .addValue("userId", userId);
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_ADD_COMPANY), parameters);
    }

    @Override
    public void removeCompany(int id) {
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_REMOVE_COMPANY_BY_ID),
            Map.of("id", id));
    }

    @Override
    public void updateCompany(Company updatedCompany, int id) {
        MapSqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("name", updatedCompany.getName())
            .addValue("id", id);
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_UPDATE_COMPANY), parameters);
    }

    @Override
    public void assignCompanyToUser(int companyId, int userId) {
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_ASSIGN_COMPANY_TO_USER),
            Map.of("companyId", companyId,
                "userId", userId));
    }

    @Override
    public Company getCompanyByIdAndUser(int companyId, int userId) {
        return jdbcTemplate.queryForObject(sqlFileHandler.readSql(SQL_GET_COMPANY_BY_ID_AND_USER),
            Map.of("userId", userId, "companyId", companyId),
            (rs, rowNum) -> {
                var company = new Company(
                    rs.getString("name"));
                company.setId(rs.getInt("id"));
                return company;
            }
        );
    }
}
