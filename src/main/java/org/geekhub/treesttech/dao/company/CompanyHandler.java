package org.geekhub.treesttech.dao.company;

import org.geekhub.treesttech.model.company.Company;

import java.util.List;

public interface CompanyHandler {

    Company getCompanyById(int id);

    List<Company> getCompaniesByUserId(int userId);

    List<Company> getAllCompanies();

    void addCompany(Company company, int userId);

    void removeCompany(int id);

    void updateCompany(Company updatedCompany, int id);

    void assignCompanyToUser(int companyId, int userId);

    Company getCompanyByIdAndUser(int companyId, int userId);
}
