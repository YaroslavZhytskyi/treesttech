package org.geekhub.treesttech.dao.company;

import org.geekhub.treesttech.model.company.Employee;
import org.geekhub.treesttech.service.SqlFileHandler;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class EmployeeDao implements EmployeeHandler {

    private static final String PATH_PREFIX = "sql/employee/";

    private static final String SQL_GET_EMPLOYEE_BY_ID = PATH_PREFIX + "GetEmployeeById.sql";

    private static final String SQL_GET_ALL_EMPLOYEES = PATH_PREFIX + "GetAllEmployees.sql";

    private static final String SQL_GET_ALL_EMPLOYEES_BY_COMPANY =
        PATH_PREFIX + "GetAllEmployeesByCompany.sql";

    private static final String SQL_ADD_EMPLOYEE = PATH_PREFIX + "AddEmployee.sql";

    private static final String SQL_REMOVE_EMPLOYEE = PATH_PREFIX + "RemoveEmployee.sql";

    private static final String SQL_UPDATE_EMPLOYEE = PATH_PREFIX + "UpdateEmployee.sql";

    private static final String SQL_ASSIGN_EQUIPMENT = PATH_PREFIX + "AssignEquipment.sql";

    private static final String SQL_DISASSIGN_EQUIPMENT = PATH_PREFIX + "DisAssignEquipment.sql";

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final SqlFileHandler sqlFileHandler;

    public EmployeeDao(NamedParameterJdbcTemplate jdbcTemplate, SqlFileHandler sqlFileHandler) {
        this.jdbcTemplate = jdbcTemplate;
        this.sqlFileHandler = sqlFileHandler;
    }

    @Override
    public Employee getEmployeeById(int id) {
        return jdbcTemplate.queryForObject(sqlFileHandler.readSql(SQL_GET_EMPLOYEE_BY_ID),
            Map.of("id", id),
            (rs, rowNum) -> {
                var employee = new Employee(
                    rs.getString("employee_name"),
                    rs.getString("employee_surname"),
                    rs.getString("employee_position"),
                    rs.getString("employee_department"),
                    rs.getInt("company_id"));
                employee.setId(rs.getInt("employee_id"));
                return employee;
            }
        );
    }

    @Override
    public List<Employee> getAllEmployees() {
        return jdbcTemplate.query(sqlFileHandler.readSql(SQL_GET_ALL_EMPLOYEES),
            (rs, rowNum) -> {
                var employee = new Employee(
                    rs.getString("employee_name"),
                    rs.getString("employee_surname"),
                    rs.getString("employee_position"),
                    rs.getString("employee_department"),
                    rs.getInt("company_id"));
                employee.setId(rs.getInt("employee_id"));
                return employee;
            }
        );
    }

    @Override
    public List<Employee> getAllEmployeesByCompany(int companyId) {
        return jdbcTemplate.query(sqlFileHandler.readSql(SQL_GET_ALL_EMPLOYEES_BY_COMPANY),
            Map.of("id", companyId),
            (rs, rowNum) -> {
                var employee = new Employee(
                    rs.getString("employee_name"),
                    rs.getString("employee_surname"),
                    rs.getString("employee_position"),
                    rs.getString("employee_department"),
                    rs.getInt("company_id"));
                employee.setId(rs.getInt("employee_id"));
                return employee;
            }
        );
    }

    @Override
    public void addEmployee(Employee employee) {
        MapSqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("name", employee.getName())
            .addValue("surname", employee.getSurname())
            .addValue("position", employee.getPosition())
            .addValue("department", employee.getDepartment())
            .addValue("companyId", employee.getCompanyId());
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_ADD_EMPLOYEE), parameters);
    }

    @Override
    public void removeEmployee(int id) {
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_REMOVE_EMPLOYEE),
            Map.of("id", id));
    }

    @Override
    public void updateEmployee(Employee employee, int id) {
        MapSqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("name", employee.getName())
            .addValue("surname", employee.getSurname())
            .addValue("position", employee.getPosition())
            .addValue("department", employee.getDepartment())
            .addValue("id", id);
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_UPDATE_EMPLOYEE), parameters);
    }

    @Override
    public void assignEquipment(int employeeId, int equipmentId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("employeeId", employeeId)
            .addValue("equipmentId", equipmentId);
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_ASSIGN_EQUIPMENT), parameters);
    }

    @Override
    public void disAssignEquipment(int employeeId, int equipmentId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("employeeId", employeeId)
            .addValue("equipmentId", equipmentId);
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_DISASSIGN_EQUIPMENT), parameters);
    }
}
