package org.geekhub.treesttech.dao.company;

import org.geekhub.treesttech.model.company.Employee;

import java.util.List;

public interface EmployeeHandler {

    Employee getEmployeeById(int id);

    List<Employee> getAllEmployees();

    List<Employee> getAllEmployeesByCompany(int companyId);

    void addEmployee(Employee employee);

    void removeEmployee(int id);

    void updateEmployee(Employee employee, int id);

    void assignEquipment(int employeeId, int equipmentId);

    void disAssignEquipment(int employeeId, int equipmentId);
}
