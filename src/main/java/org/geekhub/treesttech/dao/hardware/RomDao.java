package org.geekhub.treesttech.dao.hardware;

import org.geekhub.treesttech.exception.EquipmentNotFoundException;
import org.geekhub.treesttech.model.hardware.Rom;
import org.geekhub.treesttech.service.SqlFileHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository("RomDao")
public class RomDao implements HardwareDetailsHandler<Rom> {

    private static final String PATH_PREFIX = "sql/rom/";

    private static final String SQL_GET_ROM_BY_EQUIPMENT_ID =
        PATH_PREFIX + "GeyRomByEquipmentId.sql";

    private static final String SQL_ADD_ROM = PATH_PREFIX + "AddRom.sql";

    private static final String SQL_REMOVE_ROM_BY_ID = PATH_PREFIX + "RemoveRomById.sql";

    private static final String SQL_UPDATE_ROM = PATH_PREFIX + "UpdateRom.sql";

    private static final String SQL_GET_MAX_ID = PATH_PREFIX + "GetMaxId.sql";

    private static final String SQL_COUNT = PATH_PREFIX + "Count.sql";

    private static final String SQL_GET_ALL_ID = PATH_PREFIX + "GetAllId.sql";

    private static final Logger logger = LoggerFactory.getLogger(RamDao.class);

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final SqlFileHandler sqlFileHandler;

    public RomDao(NamedParameterJdbcTemplate jdbcTemplate, SqlFileHandler sqlFileHandler) {
        this.jdbcTemplate = jdbcTemplate;
        this.sqlFileHandler = sqlFileHandler;
    }

    @Override
    public List<Rom> getDetailsByEquipmentId(int id) {
        return jdbcTemplate.query(sqlFileHandler.readSql(SQL_GET_ROM_BY_EQUIPMENT_ID),
            Map.of("id", id),
            (rs, rowNum) -> {
                var rom = new Rom(
                    rs.getString("rom_name"),
                    rs.getInt("rom_capacity"),
                    rs.getInt("equipment_id"));
                rom.setId(rs.getInt("rom_id"));
                return rom;
            }
        );
    }

    @Override
    public void addDetails(Rom details) {
        int equipmentId = details.getEquipmentId();
        MapSqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("id", (getMaxId(equipmentId) + 1))
            .addValue("name", details.getName())
            .addValue("capacity", details.getCapacity())
            .addValue("equipmentId", equipmentId);
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_ADD_ROM), parameters);
    }

    @Override
    public void removeDetails(int equipmentId, int romId) {
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_REMOVE_ROM_BY_ID),
            Map.of("equipmentId", equipmentId, "romId", romId));
    }

    @Override
    public void updateDetails(Rom details, int equipmentId, int romId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("name", details.getName())
            .addValue("capacity", details.getCapacity())
            .addValue("newId", details.getEquipmentId())
            .addValue("equipmentId", equipmentId)
            .addValue("romId", romId);

        jdbcTemplate.update(sqlFileHandler.readSql(SQL_UPDATE_ROM), parameters);
    }

    @Override
    public int countDetails(int equipmentId) {
        var count = jdbcTemplate.queryForObject(sqlFileHandler.readSql(SQL_COUNT),
            Map.of("equipmentId", equipmentId), Integer.class);
        if (count == null) {
            String msg = String.format("There is no Rom for equipment: %d", equipmentId);
            logger.info(msg);
            throw new EquipmentNotFoundException(msg, getClass().getSimpleName());
        }
        return count;
    }

    @Override
    public List<Integer> getDetailsIdByEquipment(int equipmentId) {
        return jdbcTemplate.query(sqlFileHandler.readSql(SQL_GET_ALL_ID),
            Map.of("equipmentId", equipmentId),
            (rs, rowNum) -> rs.getInt("rom_id"));
    }

    private int getMaxId(int id) {
        var maxId = jdbcTemplate.query(sqlFileHandler.readSql(SQL_GET_MAX_ID),
            Map.of("id", id),
            (rs, rowNum) -> rs.getObject("rom_id")
        );
        if (maxId.size() == 0) {
            return 0;
        }
        return (int) maxId.get(0);
    }
}
