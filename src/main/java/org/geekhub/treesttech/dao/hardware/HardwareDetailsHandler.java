package org.geekhub.treesttech.dao.hardware;

import org.geekhub.treesttech.model.hardware.HardwareDetails;

import java.util.List;

public interface HardwareDetailsHandler<T extends HardwareDetails> {

    List<T> getDetailsByEquipmentId(int id);

    void addDetails(T details);

    void removeDetails(int equipmentId, int id);

    void updateDetails(T details, int equipmentId, int id);

    int countDetails(int equipmentId);

    List<Integer> getDetailsIdByEquipment(int equipmentId);
}
