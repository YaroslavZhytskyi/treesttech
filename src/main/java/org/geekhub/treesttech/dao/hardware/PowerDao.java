package org.geekhub.treesttech.dao.hardware;

import org.geekhub.treesttech.exception.EquipmentNotFoundException;
import org.geekhub.treesttech.model.hardware.PowerSupply;
import org.geekhub.treesttech.service.SqlFileHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository("PowerSupply")
public class PowerDao implements HardwareDetailsHandler<PowerSupply> {

    private static final String PATH_PREFIX = "sql/power/";

    private static final String SQL_GET_POWER_BY_EQUIPMENT_ID = PATH_PREFIX
        + "GetPowerByEquipmentId.sql";

    private static final String SQL_ADD_POWER = PATH_PREFIX + "AddPower.sql";

    private static final String SQL_REMOVE_POWER_BY_ID = PATH_PREFIX + "RemovePowerById.sql";

    private static final String SQL_UPDATE_POWER = PATH_PREFIX + "UpdatePower.sql";

    private static final String SQL_GET_MAX_ID = PATH_PREFIX + "GetMaxId.sql";

    private static final String SQL_COUNT = PATH_PREFIX + "Count.sql";

    private static final String SQL_GET_ALL_ID = PATH_PREFIX + "GetAllId.sql";

    private static final Logger logger = LoggerFactory.getLogger(RamDao.class);

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final SqlFileHandler sqlFileHandler;

    public PowerDao(NamedParameterJdbcTemplate jdbcTemplate, SqlFileHandler sqlFileHandler) {
        this.jdbcTemplate = jdbcTemplate;
        this.sqlFileHandler = sqlFileHandler;
    }

    @Override
    public List<PowerSupply> getDetailsByEquipmentId(int id) {
        return jdbcTemplate.query(sqlFileHandler.readSql(SQL_GET_POWER_BY_EQUIPMENT_ID),
            Map.of("id", id),
            (rs, rowNum) -> {
                var power = new PowerSupply(
                    rs.getString("power_name"),
                    rs.getInt("power_capacity"),
                    rs.getInt("equipment_id"));
                power.setId(rs.getInt("power_id"));
                return power;
            }
        );
    }

    @Override
    public void addDetails(PowerSupply details) {
        int equipmentId = details.getEquipmentId();
        MapSqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("id", (getMaxId(equipmentId) + 1))
            .addValue("name", details.getName())
            .addValue("capacity", details.getCapacity())
            .addValue("equipmentId", equipmentId);
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_ADD_POWER), parameters);
    }

    @Override
    public void removeDetails(int equipmentId, int powerId) {
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_REMOVE_POWER_BY_ID),
            Map.of("equipmentId", equipmentId, "powerId", powerId));
    }

    @Override
    public void updateDetails(PowerSupply details, int equipmentId, int powerId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("name", details.getName())
            .addValue("capacity", details.getCapacity())
            .addValue("newId", details.getEquipmentId())
            .addValue("equipmentId", equipmentId)
            .addValue("powerId", powerId);

        jdbcTemplate.update(sqlFileHandler.readSql(SQL_UPDATE_POWER), parameters);
    }

    @Override
    public int countDetails(int equipmentId) {
        var count = jdbcTemplate.queryForObject(sqlFileHandler.readSql(SQL_COUNT),
            Map.of("equipmentId", equipmentId), Integer.class);
        if (count == null) {
            String msg = String.format("There is no Power for equipment: %d", equipmentId);
            logger.info(msg);
            throw new EquipmentNotFoundException(msg, getClass().getSimpleName());
        }
        return count;
    }

    @Override
    public List<Integer> getDetailsIdByEquipment(int equipmentId) {
        return jdbcTemplate.query(sqlFileHandler.readSql(SQL_GET_ALL_ID),
            Map.of("equipmentId", equipmentId),
            (rs, rowNum) -> rs.getInt("power_id"));
    }

    private int getMaxId(int id) {
        var maxId = jdbcTemplate.query(sqlFileHandler.readSql(SQL_GET_MAX_ID),
            Map.of("id", id),
            (rs, rowNum) -> rs.getObject("power_id")
        );
        if (maxId.size() == 0) {
            return 0;
        }
        return (int) maxId.get(0);
    }
}
