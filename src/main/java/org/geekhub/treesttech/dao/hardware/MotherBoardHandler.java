package org.geekhub.treesttech.dao.hardware;

import org.geekhub.treesttech.model.hardware.MotherBoard;

public interface MotherBoardHandler {

    MotherBoard getMotherBoardById(int id);

    void addMotherBoard(MotherBoard motherboard);

    void removeMotherBoard(int id);

    void updateMotherBoard(MotherBoard motherBoard, int id);
}
