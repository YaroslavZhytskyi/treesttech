package org.geekhub.treesttech.dao.hardware;

import org.geekhub.treesttech.exception.EquipmentNotFoundException;
import org.geekhub.treesttech.model.hardware.Cpu;
import org.geekhub.treesttech.service.SqlFileHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository("CpuDao")
public class CpuDao implements HardwareDetailsHandler<Cpu> {

    private static final String PATH_PREFIX = "sql/cpu/";

    private static final String SQL_GET_CPU_BY_EQUIPMENT_ID = PATH_PREFIX
        + "GetCpuByEquipmentId.sql";

    private static final String SQL_ADD_CPU = PATH_PREFIX + "AddCpu.sql";

    private static final String SQL_REMOVE_CPU_BY_ID = PATH_PREFIX + "RemoveCpuById.sql";

    private static final String SQL_UPDATE_CPU = PATH_PREFIX + "UpdateCpu.sql";

    private static final String SQL_GET_MAX_ID = PATH_PREFIX + "GetMaxId.sql";

    private static final String SQL_COUNT = PATH_PREFIX + "Count.sql";

    private static final String SQL_GET_ALL_ID = PATH_PREFIX + "GetAllId.sql";

    private static final Logger logger = LoggerFactory.getLogger(CpuDao.class);

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final SqlFileHandler sqlFileHandler;

    public CpuDao(NamedParameterJdbcTemplate jdbcTemplate, SqlFileHandler sqlFileHandler) {
        this.jdbcTemplate = jdbcTemplate;
        this.sqlFileHandler = sqlFileHandler;
    }

    @Override
    public List<Cpu> getDetailsByEquipmentId(int id) {
        return jdbcTemplate.query(sqlFileHandler.readSql(SQL_GET_CPU_BY_EQUIPMENT_ID),
            Map.of("id", id),
            (rs, rowNum) -> {
                var cpu = new Cpu(
                    rs.getString("cpu_name"),
                    rs.getInt("cpu_capacity"),
                    rs.getInt("equipment_id"));
                cpu.setId(rs.getInt("cpu_id"));
                return cpu;
            }
        );
    }

    @Override
    public void addDetails(Cpu details) {
        int equipmentId = details.getEquipmentId();
        MapSqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("id", (getMaxId(equipmentId) + 1))
            .addValue("name", details.getName())
            .addValue("capacity", details.getCapacity())
            .addValue("equipmentId", equipmentId);
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_ADD_CPU), parameters);
    }

    @Override
    public void removeDetails(int equipmentId, int cpuId) {
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_REMOVE_CPU_BY_ID),
            Map.of("equipmentId", equipmentId, "cpuId", cpuId));
    }

    @Override
    public void updateDetails(Cpu details, int equipmentId, int cpuId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("name", details.getName())
            .addValue("capacity", details.getCapacity())
            .addValue("newId", details.getEquipmentId())
            .addValue("equipmentId", equipmentId)
            .addValue("cpuId", cpuId);

        jdbcTemplate.update(sqlFileHandler.readSql(SQL_UPDATE_CPU), parameters);
    }

    @Override
    public int countDetails(int equipmentId) {
        var count = jdbcTemplate.queryForObject(sqlFileHandler.readSql(SQL_COUNT),
            Map.of("equipmentId", equipmentId), Integer.class);
        if (count == null) {
            String msg = String.format("There is no Cpu for equipment: %d", equipmentId);
            logger.info(msg);
            throw new EquipmentNotFoundException(msg, getClass().getSimpleName());
        }
        return count;
    }

    @Override
    public List<Integer> getDetailsIdByEquipment(int equipmentId) {
        return jdbcTemplate.query(sqlFileHandler.readSql(SQL_GET_ALL_ID),
            Map.of("equipmentId", equipmentId),
            (rs, rowNum) -> rs.getInt("cpu_id"));
    }

    private int getMaxId(int id) {
        var maxId = jdbcTemplate.query(sqlFileHandler.readSql(SQL_GET_MAX_ID),
            Map.of("id", id),
            (rs, rowNum) -> rs.getObject("cpu_id")
        );
        if (maxId.size() == 0) {
            return 0;
        }
        return (int) maxId.get(0);
    }
}
