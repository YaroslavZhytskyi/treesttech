package org.geekhub.treesttech.dao.hardware;

import org.geekhub.treesttech.exception.EquipmentNotFoundException;
import org.geekhub.treesttech.model.hardware.Ram;
import org.geekhub.treesttech.service.SqlFileHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository("RamDao")
public class RamDao implements HardwareDetailsHandler<Ram> {

    private static final String PATH_PREFIX = "sql/ram/";

    private static final String SQL_GET_RAM_BY_EQUIPMENT_ID
        = PATH_PREFIX + "GetRamByEquipmentId.sql";

    private static final String SQL_ADD_RAM = PATH_PREFIX + "AddRam.sql";

    private static final String SQL_REMOVE_RAM_BY_ID = PATH_PREFIX + "RemoveRamById.sql";

    private static final String SQL_UPDATE_RAM = PATH_PREFIX + "UpdateRam.sql";

    private static final String SQL_GET_MAX_ID = PATH_PREFIX + "GetMaxId.sql";

    private static final String SQL_COUNT = PATH_PREFIX + "Count.sql";

    private static final String SQL_GET_ALL_ID = PATH_PREFIX + "GetAllId.sql";

    private static final Logger logger = LoggerFactory.getLogger(RamDao.class);
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final SqlFileHandler sqlFileHandler;

    public RamDao(NamedParameterJdbcTemplate jdbcTemplate, SqlFileHandler sqlFileHandler) {
        this.jdbcTemplate = jdbcTemplate;
        this.sqlFileHandler = sqlFileHandler;
    }

    @Override
    public List<Ram> getDetailsByEquipmentId(int id) {
        return jdbcTemplate.query(sqlFileHandler.readSql(SQL_GET_RAM_BY_EQUIPMENT_ID),
            Map.of("id", id),
            (rs, rowNum) -> {
                var ram = new Ram(
                    rs.getString("ram_name"),
                    rs.getInt("ram_capacity"),
                    rs.getInt("equipment_id"));
                ram.setId(rs.getInt("ram_id"));
                return ram;
            }
        );
    }

    @Override
    public void addDetails(Ram details) {
        int equipmentId = details.getEquipmentId();
        MapSqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("id", (getMaxId(equipmentId) + 1))
            .addValue("name", details.getName())
            .addValue("capacity", details.getCapacity())
            .addValue("equipmentId", equipmentId);
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_ADD_RAM), parameters);
    }

    @Override
    public void removeDetails(int equipmentId, int ramId) {
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_REMOVE_RAM_BY_ID),
            Map.of("equipmentId", equipmentId, "ramId", ramId));
    }

    @Override
    public void updateDetails(Ram details, int equipmentId, int ramId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("name", details.getName())
            .addValue("capacity", details.getCapacity())
            .addValue("newId", details.getEquipmentId())
            .addValue("equipmentId", equipmentId)
            .addValue("ramId", ramId);

        jdbcTemplate.update(sqlFileHandler.readSql(SQL_UPDATE_RAM), parameters);
    }

    @Override
    public int countDetails(int equipmentId) {
        var count = jdbcTemplate.queryForObject(sqlFileHandler.readSql(SQL_COUNT),
            Map.of("equipmentId", equipmentId), Integer.class);
        if (count == null) {
            String msg = String.format("There is no Ram for equipment: %d", equipmentId);
            logger.info(msg);
            throw new EquipmentNotFoundException(msg, getClass().getSimpleName());
        }
        return count;
    }

    @Override
    public List<Integer> getDetailsIdByEquipment(int equipmentId) {
        return jdbcTemplate.query(sqlFileHandler.readSql(SQL_GET_ALL_ID),
            Map.of("equipmentId", equipmentId),
            (rs, rowNum) -> rs.getInt("ram_id"));
    }

    private int getMaxId(int id) {
        var maxId = jdbcTemplate.query(sqlFileHandler.readSql(SQL_GET_MAX_ID),
            Map.of("id", id),
            (rs, rowNum) -> rs.getObject("ram_id")
        );
        if (maxId.size() == 0) {
            return 0;
        }
        return (int) maxId.get(0);
    }
}
