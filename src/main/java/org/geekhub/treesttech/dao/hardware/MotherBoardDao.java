package org.geekhub.treesttech.dao.hardware;

import org.geekhub.treesttech.model.hardware.MotherBoard;
import org.geekhub.treesttech.service.SqlFileHandler;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class MotherBoardDao implements MotherBoardHandler {

    private static final String PATH_PREFIX = "sql/motherboard/";

    private static final String SQL_GET_MB_BY_EQUIPMENT_ID = PATH_PREFIX + "GetMbByEquipmentId.sql";

    private static final String SQL_ADD_MB = PATH_PREFIX + "AddMb.sql";

    private static final String SQL_REMOVE_MB_BY_ID = PATH_PREFIX + "RemoveMbById.sql";

    private static final String SQL_UPDATE_MB = PATH_PREFIX + "UpdateMb.sql";

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final SqlFileHandler sqlFileHandler;

    public MotherBoardDao(NamedParameterJdbcTemplate jdbcTemplate, SqlFileHandler sqlFileHandler) {
        this.jdbcTemplate = jdbcTemplate;
        this.sqlFileHandler = sqlFileHandler;
    }

    @Override
    public MotherBoard getMotherBoardById(int id) {
        return jdbcTemplate.queryForObject(sqlFileHandler.readSql(SQL_GET_MB_BY_EQUIPMENT_ID),
            Map.of("id", id),
            (rs, rowNum) -> new MotherBoard(
                rs.getString("motherboard_name"),
                rs.getInt("motherboard_cpu_amount"),
                rs.getInt("motherboard_ram_amount"),
                rs.getInt("equipment_id"))
        );
    }

    @Override
    public void addMotherBoard(MotherBoard motherboard) {
        MapSqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("name", motherboard.getName())
            .addValue("cpuAmount", motherboard.getCpuSockets())
            .addValue("ramAmount", motherboard.getRamSlots())
            .addValue("equipmentId", motherboard.getEquipmentId());
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_ADD_MB), parameters);
    }

    @Override
    public void removeMotherBoard(int id) {
        jdbcTemplate.update(sqlFileHandler.readSql(SQL_REMOVE_MB_BY_ID),
            Map.of("id", id));
    }

    @Override
    public void updateMotherBoard(MotherBoard motherBoard, int id) {
        MapSqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("name", motherBoard.getName())
            .addValue("cpuAmount", motherBoard.getCpuSockets())
            .addValue("ramAmount", motherBoard.getRamSlots())
            .addValue("newId", motherBoard.getEquipmentId())
            .addValue("equipmentId", id);

        jdbcTemplate.update(sqlFileHandler.readSql(SQL_UPDATE_MB), parameters);
    }
}
