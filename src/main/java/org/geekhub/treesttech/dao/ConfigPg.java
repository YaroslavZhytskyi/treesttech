package org.geekhub.treesttech.dao;

import com.zaxxer.hikari.HikariConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile({"prod", "default"})
public class ConfigPg implements ConfigHandler {

    private static final String PG_DRIVER = "org.postgresql.Driver";

    @Value("${db.login}")
    private String dbLogin;
    @Value("${db.password}")
    private String dbPassword;
    @Value("${db.url}")
    private String dbUrl;

    @Override
    public HikariConfig getConfig() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(dbUrl);
        config.setDriverClassName(PG_DRIVER);
        config.setUsername(dbLogin);
        config.setPassword(dbPassword);
        return config;
    }
}
