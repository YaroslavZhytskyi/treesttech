package org.geekhub.treesttech.security;

import org.springframework.security.core.GrantedAuthority;

import java.util.Objects;

public final class GrantedAuthorityImpl implements GrantedAuthority {

    private final String role;

    GrantedAuthorityImpl(String role) {
        this.role = role;
    }

    public String role() {
        return role;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        var that = (GrantedAuthorityImpl) obj;
        return Objects.equals(this.role, that.role);
    }

    @Override
    public int hashCode() {
        return Objects.hash(role);
    }

    @Override
    public String toString() {
        return "GrantedAuthorityImpl["
            + "role=" + role + ']';
    }

    @Override
    public String getAuthority() {
        return role;
    }
}
