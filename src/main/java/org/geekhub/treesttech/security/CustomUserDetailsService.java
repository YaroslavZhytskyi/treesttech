package org.geekhub.treesttech.security;

import org.geekhub.treesttech.dao.user.UserHandler;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("userDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

    private final UserHandler handler;

    public CustomUserDetailsService(UserHandler handler) {
        this.handler = handler;
    }

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        try {
            return new CustomUserDetails(handler.getUserByName(username));
        } catch (EmptyResultDataAccessException e) {
            throw new UsernameNotFoundException("User does not exist");
        } catch (IncorrectResultSizeDataAccessException e) {
            throw new RuntimeException("More than one users with the same name .......");
        }
    }
}
