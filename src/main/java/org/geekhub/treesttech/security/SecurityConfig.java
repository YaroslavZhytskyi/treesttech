package org.geekhub.treesttech.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsService;
    private final DataSource dataSource;

    public SecurityConfig(DataSource dataSource,
                          @Qualifier("userDetailsService") UserDetailsService userDetailsService) {
        this.dataSource = dataSource;
        this.userDetailsService = userDetailsService;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public RoleHierarchy roleHierarchy() {
        RoleHierarchyImpl roleHierarchy = new RoleHierarchyImpl();

        roleHierarchy.setHierarchy("ROLE_SUPER_ADMIN > ROLE_ADMIN > ROLE_USER");

        return roleHierarchy;
    }

    @Autowired
    protected void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
            .userDetailsService(userDetailsService)
            .and()
            .jdbcAuthentication()
            .dataSource(dataSource);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
            .mvcMatchers(HttpMethod.GET, "/admin/**").hasRole("ADMIN")
            .mvcMatchers(HttpMethod.POST, "/admin/**").hasRole("SUPER_ADMIN")
            .mvcMatchers(HttpMethod.PUT, "/admin/**").hasRole("SUPER_ADMIN")
            .mvcMatchers(HttpMethod.DELETE, "/admin/**").hasRole("SUPER_ADMIN")
            .mvcMatchers("/actuator/**").hasRole("ADMIN")
            .mvcMatchers("/h2-console/**").hasAnyRole("ADMIN", "SUPER_ADMIN")
            .anyRequest().authenticated()
            .and()
            .formLogin()
                .permitAll()
            .and()
                .logout()
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID")
                .permitAll()
            .and()
            .rememberMe()
                .key("uniqueAndSecret")
                .rememberMeCookieName("remember-me")
                .rememberMeParameter("remember-me");

        http
            .csrf().disable();

        http
            .headers()
            .frameOptions()
            .sameOrigin();
    }
}
