package org.geekhub.treesttech.exception;

public class EquipmentNotFoundException extends RuntimeException {

    private final String msgToUser;

    public EquipmentNotFoundException(String message, String type) {
        super(message);
        this.msgToUser = type;
    }

    public String getMsgToUser() {
        return msgToUser;
    }
}
