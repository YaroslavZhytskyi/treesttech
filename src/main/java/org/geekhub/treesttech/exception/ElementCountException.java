package org.geekhub.treesttech.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_ACCEPTABLE, reason = "amount is not correct")
public class ElementCountException extends RuntimeException {

    public ElementCountException(String message) {
        super(message);
    }
}
