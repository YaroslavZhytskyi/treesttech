package org.geekhub.treesttech.exception;

public class EquipmentTypeNotSupportedException extends RuntimeException {

    public EquipmentTypeNotSupportedException(String message) {
        super(message);
    }
}
