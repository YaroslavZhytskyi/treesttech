package org.geekhub.treesttech.exception;

public class SqlScriptNotFoundException extends RuntimeException {

    public SqlScriptNotFoundException(String message) {
        super(message);
    }

    public SqlScriptNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
