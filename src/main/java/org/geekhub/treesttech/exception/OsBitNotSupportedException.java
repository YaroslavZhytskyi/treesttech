package org.geekhub.treesttech.exception;

public class OsBitNotSupportedException extends RuntimeException {

    public OsBitNotSupportedException(String message) {
        super(message);
    }
}
