package org.geekhub.treesttech.model.equipment;

import org.geekhub.treesttech.exception.EquipmentTypeNotSupportedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum EquipmentType {

    NETWORK, SERVER, ENDPOINT;

    public static EquipmentType from(String type) {
        final Logger logger = LoggerFactory.getLogger(EquipmentType.class);
        for (var equipmentType : EquipmentType.values()) {
            if (equipmentType.name().equals(type)) {
                return equipmentType;
            }
        }
        String errMessage = String.format("%s is not supported as Equipment type", type);
        logger.warn(errMessage);
        throw new EquipmentTypeNotSupportedException(errMessage);
    }
}