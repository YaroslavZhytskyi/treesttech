package org.geekhub.treesttech.model.equipment;

import java.time.LocalDate;
import java.util.Objects;

public final class EquipmentInfo {

    private final String inventoryNumber;
    private final String serialNumber;
    private final LocalDate purchaseDate;
    private final String note;
    private final int equipmentId;

    public EquipmentInfo(String inventoryNumber, String serialNumber, LocalDate purchaseDate,
                         String note, int equipmentId) {
        this.inventoryNumber = inventoryNumber;
        this.serialNumber = serialNumber;
        this.purchaseDate = purchaseDate;
        this.note = note;
        this.equipmentId = equipmentId;
    }

    public String getInventoryNumber() {
        return inventoryNumber;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public LocalDate getPurchaseDate() {
        return purchaseDate;
    }

    public String getNote() {
        return note;
    }

    public int getEquipmentId() {
        return equipmentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EquipmentInfo info = (EquipmentInfo) o;

        if (equipmentId != info.equipmentId) {
            return false;
        }
        if (!Objects.equals(inventoryNumber, info.inventoryNumber)) {
            return false;
        }
        if (!Objects.equals(serialNumber, info.serialNumber)) {
            return false;
        }
        if (!Objects.equals(purchaseDate, info.purchaseDate)) {
            return false;
        }
        return Objects.equals(note, info.note);
    }

    @Override
    public int hashCode() {
        int result = inventoryNumber != null ? inventoryNumber.hashCode() : 0;
        result = 31 * result + (serialNumber != null ? serialNumber.hashCode() : 0);
        result = 31 * result + (purchaseDate != null ? purchaseDate.hashCode() : 0);
        result = 31 * result + (note != null ? note.hashCode() : 0);
        result = 31 * result + equipmentId;
        return result;
    }

    @Override
    public String toString() {
        return "EquipmentInfo{"
            + "inventoryNumber='" + inventoryNumber + '\''
            + ", serialNumber='" + serialNumber + '\''
            + ", purchaseDate=" + purchaseDate
            + ", note='" + note + '\''
            + ", equipmentId=" + equipmentId
            + '}';
    }
}
