package org.geekhub.treesttech.model.equipment;

import org.geekhub.treesttech.model.company.Company;
import org.geekhub.treesttech.model.hardware.Hardware;
import org.geekhub.treesttech.model.software.Software;

import java.util.Objects;

public final class Equipment {
    private int id;
    private final String name;
    private final Company company;
    private final EquipmentGroup group;
    private final EquipmentInfo info;
    private final Hardware hardware;
    private final Software software;

    public Equipment(String name,
                     Company company,
                     EquipmentGroup group,
                     EquipmentInfo info,
                     Hardware hardware,
                     Software software) {
        this.name = name;
        this.company = company;
        this.group = group;
        this.info = info;
        this.hardware = hardware;
        this.software = software;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Company getCompany() {
        return this.company;
    }

    public EquipmentGroup getGroup() {
        return this.group;
    }

    public EquipmentInfo getInfo() {
        return this.info;
    }

    public Hardware getHardware() {
        return this.hardware;
    }

    public Software getSoftware() {
        return this.software;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        var that = (Equipment) obj;
        return this.id == that.id
            && Objects.equals(this.name, that.name)
            && Objects.equals(this.company, that.company)
            && Objects.equals(this.group, that.group)
            && Objects.equals(this.info, that.info)
            && Objects.equals(this.hardware, that.hardware)
            && Objects.equals(this.software, that.software);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, company, group, info, hardware, software);
    }

    @Override
    public String toString() {
        return "Equipment{"
            + "id=" + id
            + ", name='" + name + '\''
            + ", company=" + company
            + ", entity=" + group
            + ", info=" + info
            + ", hardware=" + hardware
            + ", software=" + software
            + '}';
    }
}
