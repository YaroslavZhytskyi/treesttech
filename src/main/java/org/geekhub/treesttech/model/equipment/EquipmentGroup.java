package org.geekhub.treesttech.model.equipment;

import java.util.Objects;

public final class EquipmentGroup {

    private int id;
    private final String name;
    private final EquipmentType type;

    public EquipmentGroup(String name, EquipmentType type) {
        this.name = name;
        this.type = type;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public EquipmentType getType() {
        return this.type;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        var that = (EquipmentGroup) obj;
        return this.id == that.id
            && Objects.equals(this.name, that.name)
            && Objects.equals(this.type, that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, type);
    }

    @Override
    public String toString() {
        return "EquipmentEntity{"
            + "id=" + id
            + ", name='" + name + '\''
            + ", type=" + type
            + '}';
    }
}
