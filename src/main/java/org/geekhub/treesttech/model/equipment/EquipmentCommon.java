package org.geekhub.treesttech.model.equipment;

import java.util.Objects;

public class EquipmentCommon {

    private int id;
    private String name;
    private final int groupId;
    private final int companyId;

    public EquipmentCommon(String name, int groupId, int companyId) {
        this.name = name;
        this.groupId = groupId;
        this.companyId = companyId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getGroupId() {
        return groupId;
    }

    public int getCompanyId() {
        return companyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EquipmentCommon that = (EquipmentCommon) o;

        if (id != that.id) {
            return false;
        }
        if (groupId != that.groupId) {
            return false;
        }
        if (companyId != that.companyId) {
            return false;
        }
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + groupId;
        result = 31 * result + companyId;
        return result;
    }

    @Override
    public String toString() {
        return "EquipmentCommon{"
            + "id=" + id
            + ", name='" + name + '\''
            + ", groupId=" + groupId
            + ", companyId=" + companyId
            + '}';
    }
}
