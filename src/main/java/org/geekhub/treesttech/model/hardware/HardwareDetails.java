package org.geekhub.treesttech.model.hardware;

import java.util.Objects;

public abstract class HardwareDetails {

    private int id;
    private final String name;
    private final int capacity;
    private final int equipmentId;

    public HardwareDetails(String name, int capacity, int equipmentId) {
        this.name = name;
        this.capacity = capacity;
        this.equipmentId = equipmentId;
    }

    public String getName() {
        return name;
    }

    public int getCapacity() {
        return capacity;
    }

    public int getEquipmentId() {
        return equipmentId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        HardwareDetails that = (HardwareDetails) o;

        if (id != that.id) {
            return false;
        }
        if (capacity != that.capacity) {
            return false;
        }
        if (equipmentId != that.equipmentId) {
            return false;
        }
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + capacity;
        result = 31 * result + equipmentId;
        return result;
    }

    @Override
    public String toString() {
        return "HardwareDetails{"
            + "id=" + id
            + ", name='" + name + '\''
            + ", capacity=" + capacity
            + ", equipmentId=" + equipmentId
            + '}';
    }
}
