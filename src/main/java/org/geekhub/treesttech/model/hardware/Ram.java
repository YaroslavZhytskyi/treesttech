package org.geekhub.treesttech.model.hardware;

public class Ram extends HardwareDetails {

    public Ram(String name, int capacity, int equipmentId) {
        super(name, capacity, equipmentId);
    }
}
