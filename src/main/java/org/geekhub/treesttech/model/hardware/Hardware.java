package org.geekhub.treesttech.model.hardware;

import java.util.List;
import java.util.Objects;

public class Hardware {

    private final List<Cpu> cpuList;
    private final List<Ram> ramList;
    private final MotherBoard motherboard;
    private final List<Rom> romList;
    private final List<PowerSupply> powerSupplyList;
    private final int equipmentId;

    public Hardware(List<Cpu> cpuList,
                    List<Ram> ramList,
                    MotherBoard motherboard,
                    List<Rom> romList,
                    List<PowerSupply> powerSupplyList,
                    int equipmentId) {
        this.cpuList = cpuList;
        this.ramList = ramList;
        this.motherboard = motherboard;
        this.romList = romList;
        this.powerSupplyList = powerSupplyList;
        this.equipmentId = equipmentId;
    }

    public List<Cpu> getCpuList() {
        return cpuList;
    }

    public List<Ram> getRamList() {
        return ramList;
    }

    public MotherBoard getMotherboard() {
        return motherboard;
    }

    public List<Rom> getRomList() {
        return romList;
    }

    public List<PowerSupply> getPowerSupplyList() {
        return powerSupplyList;
    }

    public int equipmentId() {
        return equipmentId;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        var that = (Hardware) obj;
        return Objects.equals(this.cpuList, that.cpuList)
            && Objects.equals(this.ramList, that.ramList)
            && Objects.equals(this.motherboard, that.motherboard)
            && Objects.equals(this.romList, that.romList)
            && Objects.equals(this.powerSupplyList, that.powerSupplyList)
            && this.equipmentId == that.equipmentId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(cpuList, ramList, motherboard, romList, powerSupplyList, equipmentId);
    }

    @Override
    public String toString() {
        return "Hardware{"
            + "cpuList=" + cpuList
            + ", ramList=" + ramList
            + ", motherboard=" + motherboard
            + ", romList=" + romList
            + ", powerSupplyList=" + powerSupplyList
            + ", equipmentId=" + equipmentId
            + '}';
    }
}
