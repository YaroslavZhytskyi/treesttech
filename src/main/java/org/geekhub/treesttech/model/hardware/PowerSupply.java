package org.geekhub.treesttech.model.hardware;

public class PowerSupply extends HardwareDetails {

    public PowerSupply(String name, int capacity, int equipmentId) {
        super(name, capacity, equipmentId);
    }
}
