package org.geekhub.treesttech.model.hardware;

import java.util.Objects;

public class MotherBoard {

    private final String name;
    private final int cpuSockets;
    private final int ramSlots;
    private final int equipmentId;

    public MotherBoard(String name, int cpuSockets, int ramSlots, int equipmentId) {
        this.name = name;
        this.cpuSockets = cpuSockets;
        this.ramSlots = ramSlots;
        this.equipmentId = equipmentId;
    }

    public String getName() {
        return name;
    }

    public int getCpuSockets() {
        return cpuSockets;
    }

    public int getRamSlots() {
        return ramSlots;
    }

    public int getEquipmentId() {
        return equipmentId;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        var that = (MotherBoard) obj;
        return Objects.equals(this.name, that.name)
            && this.cpuSockets == that.cpuSockets
            && this.ramSlots == that.ramSlots
            && this.equipmentId == that.equipmentId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, cpuSockets, ramSlots, equipmentId);
    }

    @Override
    public String toString() {
        return "MotherBoard{"
            + "name='" + name + '\''
            + ", cpuSockets=" + cpuSockets
            + ", ramSlots=" + ramSlots
            + ", equipmentId=" + equipmentId
            + '}';
    }
}
