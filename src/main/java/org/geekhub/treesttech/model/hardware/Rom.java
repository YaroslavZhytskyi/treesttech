package org.geekhub.treesttech.model.hardware;

public class Rom extends HardwareDetails {

    public Rom(String name, int capacity, int equipmentId) {
        super(name, capacity, equipmentId);
    }
}
