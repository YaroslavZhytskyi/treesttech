package org.geekhub.treesttech.model.hardware;

public class Cpu extends HardwareDetails {

    public Cpu(String name, int capacity, int equipmentId) {
        super(name, capacity, equipmentId);
    }
}
