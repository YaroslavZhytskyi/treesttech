package org.geekhub.treesttech.model;

import java.time.LocalDate;

public class ErrorMessage {
    private final int statusCode;
    private final LocalDate date;
    private final String message;
    private final String description;

    public ErrorMessage(int statusCode, LocalDate date, String message, String description) {
        this.statusCode = statusCode;
        this.date = date;
        this.message = message;
        this.description = description;
    }

    @Override
    public String toString() {
        return "ErrorMessage{"
            + "statusCode=" + statusCode
            + ", date=" + date
            + ", message='" + message + '\''
            + ", description='" + description + '\''
            + '}';
    }
}
