package org.geekhub.treesttech.model.company;

import java.util.Objects;

public final class Employee {

    private int id;
    private String name;
    private final String surname;
    private final String position;
    private final String department;
    private final int companyId;

    public Employee(String name,
                    String surname,
                    String position,
                    String department,
                    int companyId) {
        this.name = name;
        this.surname = surname;
        this.position = position;
        this.department = department;
        this.companyId = companyId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPosition() {
        return position;
    }

    public String getDepartment() {
        return department;
    }

    public int getCompanyId() {
        return companyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Employee employee = (Employee) o;

        if (id != employee.id) {
            return false;
        }
        if (companyId != employee.companyId) {
            return false;
        }
        if (!Objects.equals(name, employee.name)) {
            return false;
        }
        if (!Objects.equals(surname, employee.surname)) {
            return false;
        }
        if (!Objects.equals(position, employee.position)) {
            return false;
        }
        return Objects.equals(department, employee.department);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (position != null ? position.hashCode() : 0);
        result = 31 * result + (department != null ? department.hashCode() : 0);
        result = 31 * result + companyId;
        return result;
    }

    @Override
    public String toString() {
        return "Employee{"
            + "id=" + id
            + ", name='" + name + '\''
            + ", surname='" + surname + '\''
            + ", position='" + position + '\''
            + ", department='" + department + '\''
            + ", companyId=" + companyId
            + '}';
    }
}
