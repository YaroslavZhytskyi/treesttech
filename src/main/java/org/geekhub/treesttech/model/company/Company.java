package org.geekhub.treesttech.model.company;

import java.util.Objects;

public final class Company {

    private final String name;
    private int id;

    public Company(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        var that = (Company) obj;
        return Objects.equals(this.name, that.name)
            && this.id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, id);
    }

    @Override
    public String toString() {
        return "Company{"
            + "name='" + name + '\''
            + ", id=" + id
            + '}';
    }
}