package org.geekhub.treesttech.model.software;

public final class Software {

    private final OperatingSystem operatingSystem;
    private final int equipmentId;

    public Software(OperatingSystem operatingSystem, int equipmentId) {
        this.operatingSystem = operatingSystem;
        this.equipmentId = equipmentId;
    }

    public OperatingSystem getOperatingSystem() {
        return operatingSystem;
    }

    public int getEquipmentId() {
        return equipmentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Software software = (Software) o;

        if (equipmentId != software.equipmentId) {
            return false;
        }
        return operatingSystem != null ? operatingSystem.equals(software.operatingSystem)
            : software.operatingSystem == null;
    }

    @Override
    public int hashCode() {
        int result = operatingSystem != null ? operatingSystem.hashCode() : 0;
        result = 31 * result + equipmentId;
        return result;
    }

    @Override
    public String toString() {
        return "Software{"
            + "operatingSystem=" + operatingSystem
            + ", equipmentId=" + equipmentId
            + '}';
    }
}
