package org.geekhub.treesttech.model.software;

public final class OperatingSystem {

    private final String name;
    private final OperatingSystemBit bit;
    private final int equipmentId;

    public OperatingSystem(String name, OperatingSystemBit bit, int equipmentId) {
        this.name = name;
        this.bit = bit;
        this.equipmentId = equipmentId;
    }

    public String getName() {
        return name;
    }

    public OperatingSystemBit getBit() {
        return bit;
    }

    public int getEquipmentId() {
        return equipmentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OperatingSystem that = (OperatingSystem) o;

        if (equipmentId != that.equipmentId) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        return bit == that.bit;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (bit != null ? bit.hashCode() : 0);
        result = 31 * result + equipmentId;
        return result;
    }

    @Override
    public String toString() {
        return "OperatingSystem{"
            + "name='" + name + '\''
            + ", bit=" + bit
            + ", equipmentId=" + equipmentId
            + '}';
    }
}
