package org.geekhub.treesttech.model.software;

import org.geekhub.treesttech.exception.OsBitNotSupportedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum OperatingSystemBit {

    x64,
    x86;

    public static OperatingSystemBit from(String type) {
        final Logger logger = LoggerFactory.getLogger(OperatingSystemBit.class);
        for (var osBit : OperatingSystemBit.values()) {
            if (osBit.name().equals(type)) {
                return osBit;
            }
        }
        String errMessage = String.format("%s is not supported as OS bit", type);
        logger.warn(errMessage);
        throw new OsBitNotSupportedException(errMessage);
    }
}
