package org.geekhub.treesttech;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class TreesttechApplicationTests {

    @Autowired
    TreesttechApplication application;

    @Test
    void contextLoads() {
        assertNotNull(application);
    }

}
