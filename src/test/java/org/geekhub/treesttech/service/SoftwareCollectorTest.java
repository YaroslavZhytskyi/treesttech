package org.geekhub.treesttech.service;

import org.geekhub.treesttech.dao.software.OsHandler;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SoftwareCollectorTest {

    @Mock
    private OsHandler osHandler;

    private SoftwareCollectorHandler handler;

    @BeforeEach
    void setUp() {
        handler = new SoftwareCollector(osHandler);
    }

    @Test
    void getSoftware_inputNull_shouldReturnNotNull() {
        when(osHandler.getOsById(anyInt())).thenReturn(null);
        int someId = anyInt();
        assertNotNull(handler.getSoftware(someId));
    }

    @Test
    void getSoftware_inputNullWithRealId_shouldReturnSoftwareWithProperlyId() {
        when(osHandler.getOsById(anyInt())).thenReturn(null);
        int someId = 10;
        assertEquals(someId, handler.getSoftware(someId).getEquipmentId());
    }
}