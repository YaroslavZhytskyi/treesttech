package org.geekhub.treesttech.service;

import org.geekhub.treesttech.dao.company.CompanyHandler;
import org.geekhub.treesttech.dao.equipment.EquipmentCommonHandler;
import org.geekhub.treesttech.dao.equipment.EquipmentInfoHandler;
import org.geekhub.treesttech.dao.group.GroupHandler;
import org.geekhub.treesttech.model.equipment.EquipmentCommon;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class EquipmentCollectorTest {

    @Mock private EquipmentCommonHandler commonHandler;
    @Mock private CompanyHandler companyHandler;
    @Mock private GroupHandler groupHandler;
    @Mock private HardwareCollectorHandler hardwareCollectorHandler;
    @Mock private SoftwareCollectorHandler softwareCollectorHandler;
    @Mock private EquipmentInfoHandler infoHandler;

    private EquipmentHandler handler;

    @BeforeEach
    void setUp() {
        handler = new EquipmentCollector(commonHandler, companyHandler,
            groupHandler, hardwareCollectorHandler, softwareCollectorHandler, infoHandler);
    }

    @Test
    void getEquipmentById_inputEquipmentCommon_shouldReturnNotNull() {
        EquipmentCommon common = new EquipmentCommon("", 1, 1);
        when(commonHandler.getEquipmentById(anyInt())).thenReturn(common);
        when(companyHandler.getCompanyById(anyInt())).thenReturn(null);
        when(groupHandler.getGroupById(anyInt())).thenReturn(null);
        when(infoHandler.getEquipmentInfo(anyInt())).thenReturn(null);
        when(hardwareCollectorHandler.getHardware(anyInt())).thenReturn(null);
        when(softwareCollectorHandler.getSoftware(anyInt())).thenReturn(null);
        int someId = anyInt();

        assertNotNull(handler.getEquipmentById(someId));
    }

    @Test
    void getEquipmentById_inputEquipmentCommon_shouldReturnCorrectName() {
        String name = "Test";
        EquipmentCommon common = new EquipmentCommon(name, 1, 1);
        when(commonHandler.getEquipmentById(anyInt())).thenReturn(common);
        when(companyHandler.getCompanyById(anyInt())).thenReturn(null);
        when(groupHandler.getGroupById(anyInt())).thenReturn(null);
        when(infoHandler.getEquipmentInfo(anyInt())).thenReturn(null);
        when(hardwareCollectorHandler.getHardware(anyInt())).thenReturn(null);
        when(softwareCollectorHandler.getSoftware(anyInt())).thenReturn(null);
        int someId = anyInt();

        assertEquals(name, handler.getEquipmentById(someId).getName());
    }

    @Test
    void getEquipmentById_inputEquipmentCommon_shouldReturnZeroAsId() {
        EquipmentCommon common = new EquipmentCommon("", 1, 1);
        when(commonHandler.getEquipmentById(anyInt())).thenReturn(common);
        when(companyHandler.getCompanyById(anyInt())).thenReturn(null);
        when(groupHandler.getGroupById(anyInt())).thenReturn(null);
        when(infoHandler.getEquipmentInfo(anyInt())).thenReturn(null);
        when(hardwareCollectorHandler.getHardware(anyInt())).thenReturn(null);
        when(softwareCollectorHandler.getSoftware(anyInt())).thenReturn(null);
        int someId = anyInt();
        int notInitializedId = 0;

        assertEquals(notInitializedId, handler.getEquipmentById(someId).getId());
    }

    @Test
    void getEquipmentById_inputEquipmentCommonNull_shouldThrowNoSuchElementException() {
        when(commonHandler.getEquipmentById(anyInt())).thenReturn(null);
        int someId = anyInt();

        assertThrows(NoSuchElementException.class,
            () -> handler.getEquipmentById(someId));
    }

    @Test
    void getAllEquipment_shouldReturnListOfEquipmentCommon() {
        EquipmentCommon common = new EquipmentCommon("", 1, 1);
        List<EquipmentCommon> expected = List.of(common);
        when(commonHandler.getAllEquipmentByCompanyId(anyInt())).thenReturn(expected);
        int someId = anyInt();

        assertEquals(expected, handler.getAllEquipment(someId));
    }
}