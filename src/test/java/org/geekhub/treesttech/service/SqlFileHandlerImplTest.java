package org.geekhub.treesttech.service;

import org.geekhub.treesttech.exception.SqlScriptNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SqlFileHandlerImplTest {

    private SqlFileHandler handler;

    @BeforeEach
    void setUp() {
        handler = new SqlFileHandlerImpl();
    }

    @Test
    void readSql_inputNonExistFileName_shouldThrowSqlScriptNotFoundException() {
        String wrongFileName = "12345";
        assertThrows(SqlScriptNotFoundException.class,
            () -> handler.readSql(wrongFileName));
    }

    @Test
    void readSql_inputTestFileName_shouldReturnString() {
        String fileName = "test";
        String fileData = "TEST";
        assertEquals(fileData, handler.readSql(fileName));
    }
}