package org.geekhub.treesttech.service;

import org.geekhub.treesttech.dao.hardware.HardwareDetailsHandler;
import org.geekhub.treesttech.dao.hardware.MotherBoardHandler;
import org.geekhub.treesttech.model.hardware.Cpu;
import org.geekhub.treesttech.model.hardware.PowerSupply;
import org.geekhub.treesttech.model.hardware.Ram;
import org.geekhub.treesttech.model.hardware.Rom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class HardwareCollectorTest {

    @Mock private MotherBoardHandler motherBoardHandler;
    @Mock private HardwareDetailsHandler<Ram> ramHandler;
    @Mock private HardwareDetailsHandler<Rom> romHandler;
    @Mock private HardwareDetailsHandler<Cpu> cpuHandler;
    @Mock private HardwareDetailsHandler<PowerSupply> powerHandler;

    private HardwareCollectorHandler handler;

    @BeforeEach
    void setUp() {
        handler = new HardwareCollector(motherBoardHandler,
            ramHandler, romHandler, cpuHandler, powerHandler);
    }

    @Test
    void getHardware_inputNull_shouldReturnNotNull() {
        when(motherBoardHandler.getMotherBoardById(anyInt())).thenReturn(null);
        when(ramHandler.getDetailsByEquipmentId(anyInt())).thenReturn(null);
        when(romHandler.getDetailsByEquipmentId(anyInt())).thenReturn(null);
        when(cpuHandler.getDetailsByEquipmentId(anyInt())).thenReturn(null);
        when(powerHandler.getDetailsByEquipmentId(anyInt())).thenReturn(null);
        int someId = anyInt();
        assertNotNull(handler.getHardware(someId));
    }

    @Test
    void getHardware_inputNullWithRealId_shouldReturnHardwareWithProperlyId() {
        when(motherBoardHandler.getMotherBoardById(anyInt())).thenReturn(null);
        when(ramHandler.getDetailsByEquipmentId(anyInt())).thenReturn(null);
        when(romHandler.getDetailsByEquipmentId(anyInt())).thenReturn(null);
        when(cpuHandler.getDetailsByEquipmentId(anyInt())).thenReturn(null);
        when(powerHandler.getDetailsByEquipmentId(anyInt())).thenReturn(null);
        int someId = 10;
        assertEquals(someId, handler.getHardware(someId).equipmentId());
    }
}