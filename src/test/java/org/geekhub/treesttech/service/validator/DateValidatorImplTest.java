package org.geekhub.treesttech.service.validator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DateValidatorImplTest {

    private final DateTimeFormatter dateFormatter = DateTimeFormatter.ISO_DATE;

    private DateValidator validator;

    @BeforeEach
    void setUp() {
        validator = new DateValidatorImpl(dateFormatter);
    }

    @Test
    void isValid_inputCorrectString_shouldReturnTrue() {
        String input = "1900-01-01";
        assertTrue(validator.isValid(input));
    }

    @Test
    void isValid_inputInCorrectString_shouldReturnFalse() {
        String input = "19000101";
        assertFalse(validator.isValid(input));
    }

    @Test
    void isValid_inputNull_shouldReturnFalse() {
        assertFalse(validator.isValid(null));
    }
}