package org.geekhub.treesttech.service.validator;

import org.geekhub.treesttech.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AccessValidatorImplTest {

    private static final String ADMIN = "ROLE_ADMIN";
    private static final String SUPER = "ROLE_SUPER_ADMIN";

    private AccessValidator validator;

    @BeforeEach
    void setUp() {
        validator = new AccessValidatorImpl();
    }

    @Test
    void hasFullAccess_checkAdminInput_shouldReturnTrue() {
        User user = new User("admin", "", "", ADMIN, true);
        assertTrue(validator.hasFullAccess(user));
    }

    @Test
    void hasFullAccess_checkSuperInput_shouldReturnTrue() {
        User user = new User("admin", "", "", SUPER, true);
        assertTrue(validator.hasFullAccess(user));
    }

    @Test
    void hasFullAccess_checkUserInput_shouldReturnFalse() {
        User user = new User("admin", "", "", "ROLE_USER", true);
        assertFalse(validator.hasFullAccess(user));
    }

    @Test
    void hasFullAccess_checkEmptyInput_shouldReturnFalse() {
        User user = new User("admin", "", "", "", true);
        assertFalse(validator.hasFullAccess(user));
    }
}