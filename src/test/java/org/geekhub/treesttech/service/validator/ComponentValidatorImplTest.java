package org.geekhub.treesttech.service.validator;

import org.geekhub.treesttech.dao.equipment.EquipmentCommonHandler;
import org.geekhub.treesttech.dao.group.GroupHandler;
import org.geekhub.treesttech.dao.hardware.HardwareDetailsHandler;
import org.geekhub.treesttech.model.equipment.EquipmentGroup;
import org.geekhub.treesttech.model.equipment.EquipmentType;
import org.geekhub.treesttech.model.hardware.Cpu;
import org.geekhub.treesttech.model.hardware.PowerSupply;
import org.geekhub.treesttech.model.hardware.Ram;
import org.geekhub.treesttech.model.hardware.Rom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ComponentValidatorImplTest {

    @Mock private GroupHandler groupHandler;
    @Mock private HardwareDetailsHandler<Ram> ramHandler;
    @Mock private HardwareDetailsHandler<Rom> romHandler;
    @Mock private HardwareDetailsHandler<Cpu> cpuHandler;
    @Mock private HardwareDetailsHandler<PowerSupply> powerHandler;
    @Mock private EquipmentCommonHandler equipmentHandler;

    private ComponentValidator validator;

    @BeforeEach
    void setUp() {
        validator = new ComponentValidatorImpl(
            groupHandler,
            ramHandler,
            romHandler,
            cpuHandler,
            powerHandler,
            equipmentHandler
        );
    }

    @Test
    void isGroupExist_inputValidId_shouldReturnTrue() {
        EquipmentGroup group = new EquipmentGroup("", EquipmentType.ENDPOINT);
        int groupId = 1;
        group.setId(groupId);
        when(groupHandler.getAllGroups()).thenReturn(List.of(group));
        int searchingId = 1;
        assertTrue(validator.isGroupExist(searchingId));
    }

    @Test
    void isGroupExist_inputInValidId_shouldThrowNoSuchElementException() {
        EquipmentGroup group = new EquipmentGroup("", EquipmentType.ENDPOINT);
        int groupId = 1;
        group.setId(groupId);
        when(groupHandler.getAllGroups()).thenReturn(List.of(group));
        int searchingId = 2;
        assertThrows(NoSuchElementException.class,
            () -> validator.isGroupExist(searchingId));
    }

    @Test
    void isEquipmentExist_inputValidId_shouldReturnTrue() {
        int companyId = 1;
        int equipmentId = 1;
        when(equipmentHandler.getAllEquipmentIdByCompany(anyInt()))
            .thenReturn(List.of(equipmentId));
        assertTrue(validator.isEquipmentExist(companyId, equipmentId));
    }

    @Test
    void isEquipmentExist_inputInValidId_shouldThrowNoSuchElementException() {
        int companyId = 1;
        int equipmentId = 1;
        when(equipmentHandler.getAllEquipmentIdByCompany(anyInt()))
            .thenReturn(List.of(equipmentId));
        int searchingId = 2;
        assertThrows(NoSuchElementException.class,
            () -> validator.isEquipmentExist(companyId, searchingId));
    }

    @Test
    void isRamExist_inputValidId_shouldReturnTrue() {
        int equipmentId = 1;
        int ramId = 1;
        when(ramHandler.getDetailsIdByEquipment(anyInt()))
            .thenReturn(List.of(ramId));
        assertTrue(validator.isRamExist(equipmentId, ramId));
    }

    @Test
    void isRamExist_inputInValidId_shouldThrowNoSuchElementException() {
        int equipmentId = 1;
        int ramId = 1;
        when(ramHandler.getDetailsIdByEquipment(anyInt()))
            .thenReturn(List.of(ramId));
        int searchingId = 2;
        assertThrows(NoSuchElementException.class,
            () -> validator.isRamExist(equipmentId, searchingId));
    }

    @Test
    void isRomExist_inputValidId_shouldReturnTrue() {
        int equipmentId = 1;
        int romId = 1;
        when(romHandler.getDetailsIdByEquipment(anyInt()))
            .thenReturn(List.of(romId));
        assertTrue(validator.isRomExist(equipmentId, romId));
    }

    @Test
    void isRomExist_inputInValidId_shouldThrowNoSuchElementException() {
        int equipmentId = 1;
        int romId = 1;
        when(romHandler.getDetailsIdByEquipment(anyInt()))
            .thenReturn(List.of(romId));
        int searchingId = 2;
        assertThrows(NoSuchElementException.class,
            () -> validator.isRomExist(equipmentId, searchingId));
    }

    @Test
    void isCpuExist_inputValidId_shouldReturnTrue() {
        int equipmentId = 1;
        int cpuId = 1;
        when(cpuHandler.getDetailsIdByEquipment(anyInt()))
            .thenReturn(List.of(cpuId));
        assertTrue(validator.isCpuExist(equipmentId, cpuId));
    }

    @Test
    void isCpuExist_inputInValidId_shouldThrowNoSuchElementException() {
        int equipmentId = 1;
        int cpuId = 1;
        when(cpuHandler.getDetailsIdByEquipment(anyInt()))
            .thenReturn(List.of(cpuId));
        int searchingId = 2;
        assertThrows(NoSuchElementException.class,
            () -> validator.isCpuExist(equipmentId, searchingId));
    }

    @Test
    void isPowerExist_inputValidId_shouldReturnTrue() {
        int equipmentId = 1;
        int powerId = 1;
        when(powerHandler.getDetailsIdByEquipment(anyInt()))
            .thenReturn(List.of(powerId));
        assertTrue(validator.isPowerExist(equipmentId, powerId));
    }

    @Test
    void isPowerExist_inputInValidId_shouldThrowNoSuchElementException() {
        int equipmentId = 1;
        int powerId = 1;
        when(powerHandler.getDetailsIdByEquipment(anyInt()))
            .thenReturn(List.of(powerId));
        int searchingId = 2;
        assertThrows(NoSuchElementException.class,
            () -> validator.isPowerExist(equipmentId, searchingId));
    }
}