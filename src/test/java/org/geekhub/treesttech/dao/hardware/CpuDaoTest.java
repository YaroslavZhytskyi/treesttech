package org.geekhub.treesttech.dao.hardware;

import org.geekhub.treesttech.model.hardware.Cpu;
import org.geekhub.treesttech.service.SqlFileHandlerImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.DataJdbcTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJdbcTest
class CpuDaoTest {

    private final NamedParameterJdbcTemplate namedJdbcTemplate;
    private final HardwareDetailsHandler<Cpu> handler;

    @Autowired
    public CpuDaoTest(NamedParameterJdbcTemplate namedJdbcTemplate) {
        this.namedJdbcTemplate = namedJdbcTemplate;
        this.handler = new CpuDao(namedJdbcTemplate, new SqlFileHandlerImpl());
    }

    @BeforeEach
    void setUp() {
        String sqlBefore = """
            INSERT INTO companies (company_name) VALUES ('test');
            INSERT INTO equipment (equipment_name, equipment_group, company_id)
                VALUES (' ', 1, 1);
            """;
        namedJdbcTemplate.update(sqlBefore, Collections.emptyMap());
    }

    @AfterEach
    void tearDown() {
        String sqlAfter = """
            DELETE FROM companies;
            ALTER TABLE companies ALTER COLUMN company_id RESTART WITH 1;
            ALTER TABLE equipment ALTER COLUMN equipment_id RESTART WITH 1;
            """;
        namedJdbcTemplate.update(sqlAfter, Collections.emptyMap());
    }

    @Test
    void getDetailsByEquipmentId_nothingAdding_shouldThrowNothing() {
        int equipmentId = 1;

        assertDoesNotThrow(() -> handler.getDetailsByEquipmentId(equipmentId));
    }

    @Test
    void getDetailsByEquipmentId_addingCpu_shouldReturnCpu() {
        int equipmentId = 1;
        int detailsId = 1;
        Cpu cpu = new Cpu("", 0, equipmentId);
        handler.addDetails(cpu);
        cpu.setId(detailsId);
        List<Cpu> expected = List.of(cpu);

        assertEquals(expected, handler.getDetailsByEquipmentId(equipmentId));
    }

    @Test
    void addDetails_equipmentExist_shouldReturnCpu() {
        int equipmentId = 1;
        int detailsId = 1;
        Cpu cpu = new Cpu("", 0, equipmentId);
        handler.addDetails(cpu);
        cpu.setId(detailsId);
        List<Cpu> expected = List.of(cpu);

        assertEquals(expected, handler.getDetailsByEquipmentId(equipmentId));
    }

    @Test
    void addDetails_equipmentNonExist_shouldThrowDataIntegrityViolationException() {
        int equipmentId = 2;
        Cpu cpu = new Cpu("", 0, equipmentId);

        assertThrows(DataIntegrityViolationException.class, () -> handler.addDetails(cpu));
    }

    @Test
    void removeDetails_cpuDosNotExist_shouldNotThrowAnyException() {
        int equipmentId = 1;
        int detailsId = 1;

        assertDoesNotThrow(() -> handler.removeDetails(equipmentId, detailsId));
    }

    @Test
    void removeDetails_addingCpu_shouldReturnEmptyList() {
        int equipmentId = 1;
        int detailsId = 1;
        Cpu cpu = new Cpu("", 0, equipmentId);
        handler.addDetails(cpu);
        handler.removeDetails(equipmentId, detailsId);

        assertEquals(Collections.emptyList(), handler.getDetailsByEquipmentId(equipmentId));
    }

    @Test
    void updateDetails_nothingAdding_shouldNotThrowAnyException() {
        int equipmentId = 1;
        int detailsId = 1;
        Cpu cpu = new Cpu("", 0, equipmentId);

        assertDoesNotThrow(() -> handler.updateDetails(cpu, equipmentId, detailsId));
    }

    @Test
    void updateDetails_addingCpu_shouldReturnListOfUpdatedCpu() {
        int equipmentId = 1;
        int detailsId = 1;
        Cpu cpu = new Cpu("", 0, equipmentId);
        handler.addDetails(cpu);
        Cpu cpuUpdated = new Cpu("", 1, equipmentId);
        cpuUpdated.setId(detailsId);
        handler.updateDetails(cpuUpdated, equipmentId, detailsId);
        List<Cpu> expected = List.of(cpuUpdated);

        assertEquals(expected, handler.getDetailsByEquipmentId(equipmentId));
    }

    @Test
    void countDetails_cpuDosNotExist_shouldReturnZero() {
        int equipmentId = 1;

        assertEquals(0, handler.countDetails(equipmentId));
    }

    @Test
    void countDetails_cpuExist_shouldReturnOne() {
        int equipmentId = 1;
        Cpu cpu = new Cpu("", 0, equipmentId);
        handler.addDetails(cpu);

        assertEquals(1, handler.countDetails(equipmentId));
    }

    @Test
    void getDetailsIdByEquipment_cpuDosNotExist_shouldReturnEmptyList() {
        int equipmentId = 1;

        assertEquals(Collections.emptyList(), handler.getDetailsIdByEquipment(equipmentId));
    }

    @Test
    void getDetailsIdByEquipment_cpuExist_shouldReturnListOfInteger() {
        int equipmentId = 1;
        int detailsId = 1;
        Cpu cpu = new Cpu("", 0, equipmentId);
        handler.addDetails(cpu);
        List<Integer> expected = List.of(detailsId);

        assertEquals(expected, handler.getDetailsIdByEquipment(equipmentId));
    }
}