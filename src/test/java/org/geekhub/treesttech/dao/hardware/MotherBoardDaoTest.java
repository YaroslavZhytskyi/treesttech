package org.geekhub.treesttech.dao.hardware;

import org.geekhub.treesttech.model.hardware.MotherBoard;
import org.geekhub.treesttech.service.SqlFileHandlerImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.DataJdbcTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

@DataJdbcTest
class MotherBoardDaoTest {

    private final NamedParameterJdbcTemplate namedJdbcTemplate;
    private final MotherBoardHandler handler;

    @Autowired
    public MotherBoardDaoTest(NamedParameterJdbcTemplate namedJdbcTemplate) {
        this.namedJdbcTemplate = namedJdbcTemplate;
        this.handler = new MotherBoardDao(namedJdbcTemplate, new SqlFileHandlerImpl());
    }

    @BeforeEach
    void setUp() {
        String sqlBefore = """
            INSERT INTO companies (company_name) VALUES ('test');
            INSERT INTO equipment (equipment_name, equipment_group, company_id)
                VALUES (' ', 1, 1);
            """;
        namedJdbcTemplate.update(sqlBefore, Collections.emptyMap());
    }

    @AfterEach
    void tearDown() {
        String sqlAfter = """
            DELETE FROM companies;
            ALTER TABLE companies ALTER COLUMN company_id RESTART WITH 1;
            ALTER TABLE equipment ALTER COLUMN equipment_id RESTART WITH 1;
            """;
        namedJdbcTemplate.update(sqlAfter, Collections.emptyMap());
    }

    @Test
    void getMotherBoardById_nothingAdding_shouldThrowEmptyResultDataAccessException() {
        int equipmentId = 1;

        assertThrows(EmptyResultDataAccessException.class, () -> handler.getMotherBoardById(equipmentId));
    }

    @Test
    void getMotherBoardById_addingMotherboard_shouldReturnMotherboard() {
        int equipmentId = 1;
        MotherBoard motherBoard = new MotherBoard("", 1, 1, equipmentId);
        handler.addMotherBoard(motherBoard);

        assertEquals(motherBoard, handler.getMotherBoardById(equipmentId));
    }

    @Test
    void addMotherBoard_equipmentExist_shouldReturnMotherboard() {
        int equipmentId = 1;
        MotherBoard motherBoard = new MotherBoard("", 1, 1, equipmentId);
        handler.addMotherBoard(motherBoard);

        assertEquals(motherBoard, handler.getMotherBoardById(equipmentId));
    }

    @Test
    void addMotherBoard_equipmentNotExist_shouldThrowDataIntegrityViolationException() {
        int equipmentId = 2;
        MotherBoard motherBoard = new MotherBoard("", 1, 1, equipmentId);

        assertThrows(DataIntegrityViolationException.class, () -> handler.addMotherBoard(motherBoard));
    }

    @Test
    void removeMotherBoard_motherboardDosNotExist_shouldNotThrowAnyException() {
        int equipmentId = 1;

        assertDoesNotThrow(() -> handler.removeMotherBoard(equipmentId));
    }

    @Test
    void removeMotherBoard_addingMotherboard_shouldThrowEmptyResultDataAccessException() {
        int equipmentId = 1;
        MotherBoard motherBoard = new MotherBoard("", 1, 1, equipmentId);
        handler.addMotherBoard(motherBoard);
        handler.removeMotherBoard(equipmentId);

        assertThrows(EmptyResultDataAccessException.class, () -> handler.getMotherBoardById(equipmentId));
    }

    @Test
    void updateMotherBoard_nothingAdding_shouldNotThrowAnyException() {
        int equipmentId = 1;
        MotherBoard motherBoard = new MotherBoard("", 1, 1, equipmentId);

        assertDoesNotThrow(() -> handler.updateMotherBoard(motherBoard, equipmentId));
    }

    @Test
    void updateMotherBoard_addingMotherboard_shouldReturnUpdatedMotherboard() {
        int equipmentId = 1;
        MotherBoard motherBoard = new MotherBoard("", 1, 1, equipmentId);
        handler.addMotherBoard(motherBoard);
        MotherBoard motherBoardUpdated = new MotherBoard("", 1, 2, equipmentId);
        handler.updateMotherBoard(motherBoardUpdated, equipmentId);

        assertEquals(motherBoardUpdated, handler.getMotherBoardById(equipmentId));
    }
}