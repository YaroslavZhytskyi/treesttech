package org.geekhub.treesttech.dao.hardware;

import org.geekhub.treesttech.model.hardware.Rom;
import org.geekhub.treesttech.service.SqlFileHandlerImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.DataJdbcTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJdbcTest
class RomDaoTest {

    private final NamedParameterJdbcTemplate namedJdbcTemplate;
    private final HardwareDetailsHandler<Rom> handler;

    @Autowired
    public RomDaoTest(NamedParameterJdbcTemplate namedJdbcTemplate) {
        this.namedJdbcTemplate = namedJdbcTemplate;
        this.handler = new RomDao(namedJdbcTemplate, new SqlFileHandlerImpl());
    }

    @BeforeEach
    void setUp() {
        String sqlBefore = """
            INSERT INTO companies (company_name) VALUES ('test');
            INSERT INTO equipment (equipment_name, equipment_group, company_id)
                VALUES (' ', 1, 1);
            """;
        namedJdbcTemplate.update(sqlBefore, Collections.emptyMap());
    }

    @AfterEach
    void tearDown() {
        String sqlAfter = """
            DELETE FROM companies;
            ALTER TABLE companies ALTER COLUMN company_id RESTART WITH 1;
            ALTER TABLE equipment ALTER COLUMN equipment_id RESTART WITH 1;
            """;
        namedJdbcTemplate.update(sqlAfter, Collections.emptyMap());
    }

    @Test
    void getDetailsByEquipmentId_nothingAdding_shouldThrowNothing() {
        int equipmentId = 1;

        assertDoesNotThrow(() -> handler.getDetailsByEquipmentId(equipmentId));
    }

    @Test
    void getDetailsByEquipmentId_addingRom_shouldReturnRom() {
        int equipmentId = 1;
        int detailsId = 1;
        Rom rom = new Rom("", 0, equipmentId);
        handler.addDetails(rom);
        rom.setId(detailsId);
        List<Rom> expected = List.of(rom);

        assertEquals(expected, handler.getDetailsByEquipmentId(equipmentId));
    }

    @Test
    void addDetails_equipmentExist_shouldReturnRom() {
        int equipmentId = 1;
        int detailsId = 1;
        Rom rom = new Rom("", 0, equipmentId);
        handler.addDetails(rom);
        rom.setId(detailsId);
        List<Rom> expected = List.of(rom);

        assertEquals(expected, handler.getDetailsByEquipmentId(equipmentId));
    }

    @Test
    void addDetails_equipmentNonExist_shouldThrowDataIntegrityViolationException() {
        int equipmentId = 2;
        Rom rom = new Rom("", 0, equipmentId);

        assertThrows(DataIntegrityViolationException.class, () -> handler.addDetails(rom));
    }

    @Test
    void removeDetails_romDosNotExist_shouldNotThrowAnyException() {
        int equipmentId = 1;
        int detailsId = 1;

        assertDoesNotThrow(() -> handler.removeDetails(equipmentId, detailsId));
    }

    @Test
    void removeDetails_addingRom_shouldReturnEmptyList() {
        int equipmentId = 1;
        int detailsId = 1;
        Rom rom = new Rom("", 0, equipmentId);
        handler.addDetails(rom);
        handler.removeDetails(equipmentId, detailsId);

        assertEquals(Collections.emptyList(), handler.getDetailsByEquipmentId(equipmentId));
    }

    @Test
    void updateDetails_nothingAdding_shouldNotThrowAnyException() {
        int equipmentId = 1;
        int detailsId = 1;
        Rom rom = new Rom("", 0, equipmentId);

        assertDoesNotThrow(() -> handler.updateDetails(rom, equipmentId, detailsId));
    }

    @Test
    void updateDetails_addingRom_shouldReturnListOfUpdatedRom() {
        int equipmentId = 1;
        int detailsId = 1;
        Rom rom = new Rom("", 0, equipmentId);
        handler.addDetails(rom);
        Rom romUpdated = new Rom("", 1, equipmentId);
        romUpdated.setId(detailsId);
        handler.updateDetails(romUpdated, equipmentId, detailsId);
        List<Rom> expected = List.of(romUpdated);

        assertEquals(expected, handler.getDetailsByEquipmentId(equipmentId));
    }

    @Test
    void countDetails_romDosNotExist_shouldReturnZero() {
        int equipmentId = 1;

        assertEquals(0, handler.countDetails(equipmentId));
    }

    @Test
    void countDetails_romExist_shouldReturnOne() {
        int equipmentId = 1;
        Rom rom = new Rom("", 0, equipmentId);
        handler.addDetails(rom);

        assertEquals(1, handler.countDetails(equipmentId));
    }

    @Test
    void getDetailsIdByEquipment_romDosNotExist_shouldReturnEmptyList() {
        int equipmentId = 1;

        assertEquals(Collections.emptyList(), handler.getDetailsIdByEquipment(equipmentId));
    }

    @Test
    void getDetailsIdByEquipment_romExist_shouldReturnListOfInteger() {
        int equipmentId = 1;
        int detailsId = 1;
        Rom rom = new Rom("", 0, equipmentId);
        handler.addDetails(rom);
        List<Integer> expected = List.of(detailsId);

        assertEquals(expected, handler.getDetailsIdByEquipment(equipmentId));
    }

}