package org.geekhub.treesttech.dao.hardware;

import org.geekhub.treesttech.model.hardware.Ram;
import org.geekhub.treesttech.service.SqlFileHandlerImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.DataJdbcTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJdbcTest
class RamDaoTest {

    private final NamedParameterJdbcTemplate namedJdbcTemplate;
    private final HardwareDetailsHandler<Ram> handler;

    @Autowired
    public RamDaoTest(NamedParameterJdbcTemplate namedJdbcTemplate) {
        this.namedJdbcTemplate = namedJdbcTemplate;
        this.handler = new RamDao(namedJdbcTemplate, new SqlFileHandlerImpl());
    }

    @BeforeEach
    void setUp() {
        String sqlBefore = """
            INSERT INTO companies (company_name) VALUES ('test');
            INSERT INTO equipment (equipment_name, equipment_group, company_id)
                VALUES (' ', 1, 1);
            """;
        namedJdbcTemplate.update(sqlBefore, Collections.emptyMap());
    }

    @AfterEach
    void tearDown() {
        String sqlAfter = """
            DELETE FROM companies;
            ALTER TABLE companies ALTER COLUMN company_id RESTART WITH 1;
            ALTER TABLE equipment ALTER COLUMN equipment_id RESTART WITH 1;
            """;
        namedJdbcTemplate.update(sqlAfter, Collections.emptyMap());
    }

    @Test
    void getDetailsByEquipmentId_nothingAdding_shouldThrowNothing() {
        int equipmentId = 1;

        assertDoesNotThrow(() -> handler.getDetailsByEquipmentId(equipmentId));
    }

    @Test
    void getDetailsByEquipmentId_addingRam_shouldReturnRam() {
        int equipmentId = 1;
        int detailsId = 1;
        Ram ram = new Ram("", 0, equipmentId);
        handler.addDetails(ram);
        ram.setId(detailsId);
        List<Ram> expected = List.of(ram);

        assertEquals(expected, handler.getDetailsByEquipmentId(equipmentId));
    }

    @Test
    void addDetails_equipmentExist_shouldReturnRam() {
        int equipmentId = 1;
        int detailsId = 1;
        Ram ram = new Ram("", 0, equipmentId);
        handler.addDetails(ram);
        ram.setId(detailsId);
        List<Ram> expected = List.of(ram);

        assertEquals(expected, handler.getDetailsByEquipmentId(equipmentId));
    }

    @Test
    void addDetails_equipmentNonExist_shouldThrowDataIntegrityViolationException() {
        int equipmentId = 2;
        Ram ram = new Ram("", 0, equipmentId);

        assertThrows(DataIntegrityViolationException.class, () -> handler.addDetails(ram));
    }

    @Test
    void removeDetails_ramDosNotExist_shouldNotThrowAnyException() {
        int equipmentId = 1;
        int detailsId = 1;

        assertDoesNotThrow(() -> handler.removeDetails(equipmentId, detailsId));
    }

    @Test
    void removeDetails_addingRam_shouldReturnEmptyList() {
        int equipmentId = 1;
        int detailsId = 1;
        Ram ram = new Ram("", 0, equipmentId);
        handler.addDetails(ram);
        handler.removeDetails(equipmentId, detailsId);

        assertEquals(Collections.emptyList(), handler.getDetailsByEquipmentId(equipmentId));
    }

    @Test
    void updateDetails_nothingAdding_shouldNotThrowAnyException() {
        int equipmentId = 1;
        int detailsId = 1;
        Ram ram = new Ram("", 0, equipmentId);

        assertDoesNotThrow(() -> handler.updateDetails(ram, equipmentId, detailsId));
    }

    @Test
    void updateDetails_addingRam_shouldReturnListOfUpdatedRam() {
        int equipmentId = 1;
        int detailsId = 1;
        Ram ram = new Ram("", 0, equipmentId);
        handler.addDetails(ram);
        Ram ramUpdated = new Ram("", 1, equipmentId);
        ramUpdated.setId(detailsId);
        handler.updateDetails(ramUpdated, equipmentId, detailsId);
        List<Ram> expected = List.of(ramUpdated);

        assertEquals(expected, handler.getDetailsByEquipmentId(equipmentId));
    }

    @Test
    void countDetails_ramDosNotExist_shouldReturnZero() {
        int equipmentId = 1;

        assertEquals(0, handler.countDetails(equipmentId));
    }

    @Test
    void countDetails_ramExist_shouldReturnOne() {
        int equipmentId = 1;
        Ram ram = new Ram("", 0, equipmentId);
        handler.addDetails(ram);

        assertEquals(1, handler.countDetails(equipmentId));
    }

    @Test
    void getDetailsIdByEquipment_ramDosNotExist_shouldReturnEmptyList() {
        int equipmentId = 1;

        assertEquals(Collections.emptyList(), handler.getDetailsIdByEquipment(equipmentId));
    }

    @Test
    void getDetailsIdByEquipment_ramExist_shouldReturnListOfInteger() {
        int equipmentId = 1;
        int detailsId = 1;
        Ram ram = new Ram("", 0, equipmentId);
        handler.addDetails(ram);
        List<Integer> expected = List.of(detailsId);

        assertEquals(expected, handler.getDetailsIdByEquipment(equipmentId));
    }

}