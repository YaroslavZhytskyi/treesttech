package org.geekhub.treesttech.dao.hardware;

import org.geekhub.treesttech.model.hardware.PowerSupply;
import org.geekhub.treesttech.service.SqlFileHandlerImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.DataJdbcTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJdbcTest
class PowerDaoTest {

    private final NamedParameterJdbcTemplate namedJdbcTemplate;
    private final HardwareDetailsHandler<PowerSupply> handler;

    @Autowired
    public PowerDaoTest(NamedParameterJdbcTemplate namedJdbcTemplate) {
        this.namedJdbcTemplate = namedJdbcTemplate;
        this.handler = new PowerDao(namedJdbcTemplate, new SqlFileHandlerImpl());
    }

    @BeforeEach
    void setUp() {
        String sqlBefore = """
            INSERT INTO companies (company_name) VALUES ('test');
            INSERT INTO equipment (equipment_name, equipment_group, company_id)
                VALUES (' ', 1, 1);
            """;
        namedJdbcTemplate.update(sqlBefore, Collections.emptyMap());
    }

    @AfterEach
    void tearDown() {
        String sqlAfter = """
            DELETE FROM companies;
            ALTER TABLE companies ALTER COLUMN company_id RESTART WITH 1;
            ALTER TABLE equipment ALTER COLUMN equipment_id RESTART WITH 1;
            """;
        namedJdbcTemplate.update(sqlAfter, Collections.emptyMap());
    }

    @Test
    void getDetailsByEquipmentId_nothingAdding_shouldThrowNothing() {
        int equipmentId = 1;

        assertDoesNotThrow(() -> handler.getDetailsByEquipmentId(equipmentId));
    }

    @Test
    void getDetailsByEquipmentId_addingPower_shouldReturnPower() {
        int equipmentId = 1;
        int detailsId = 1;
        PowerSupply power = new PowerSupply("", 0, equipmentId);
        handler.addDetails(power);
        power.setId(detailsId);
        List<PowerSupply> expected = List.of(power);

        assertEquals(expected, handler.getDetailsByEquipmentId(equipmentId));
    }

    @Test
    void addDetails_equipmentExist_shouldReturnPower() {
        int equipmentId = 1;
        int detailsId = 1;
        PowerSupply power = new PowerSupply("", 0, equipmentId);
        handler.addDetails(power);
        power.setId(detailsId);
        List<PowerSupply> expected = List.of(power);

        assertEquals(expected, handler.getDetailsByEquipmentId(equipmentId));
    }

    @Test
    void addDetails_equipmentNonExist_shouldThrowDataIntegrityViolationException() {
        int equipmentId = 2;
        PowerSupply power = new PowerSupply("", 0, equipmentId);

        assertThrows(DataIntegrityViolationException.class, () -> handler.addDetails(power));
    }

    @Test
    void removeDetails_powerDosNotExist_shouldNotThrowAnyException() {
        int equipmentId = 1;
        int detailsId = 1;

        assertDoesNotThrow(() -> handler.removeDetails(equipmentId, detailsId));
    }

    @Test
    void removeDetails_addingPower_shouldReturnEmptyList() {
        int equipmentId = 1;
        int detailsId = 1;
        PowerSupply power = new PowerSupply("", 0, equipmentId);
        handler.addDetails(power);
        handler.removeDetails(equipmentId, detailsId);

        assertEquals(Collections.emptyList(), handler.getDetailsByEquipmentId(equipmentId));
    }

    @Test
    void updateDetails_nothingAdding_shouldNotThrowAnyException() {
        int equipmentId = 1;
        int detailsId = 1;
        PowerSupply power = new PowerSupply("", 0, equipmentId);

        assertDoesNotThrow(() -> handler.updateDetails(power, equipmentId, detailsId));
    }

    @Test
    void updateDetails_addingPower_shouldReturnListOfUpdatedPower() {
        int equipmentId = 1;
        int detailsId = 1;
        PowerSupply power = new PowerSupply("", 0, equipmentId);
        handler.addDetails(power);
        PowerSupply powerUpdated = new PowerSupply("", 1, equipmentId);
        powerUpdated.setId(detailsId);
        handler.updateDetails(powerUpdated, equipmentId, detailsId);
        List<PowerSupply> expected = List.of(powerUpdated);

        assertEquals(expected, handler.getDetailsByEquipmentId(equipmentId));
    }

    @Test
    void countDetails_powerDosNotExist_shouldReturnZero() {
        int equipmentId = 1;

        assertEquals(0, handler.countDetails(equipmentId));
    }

    @Test
    void countDetails_powerExist_shouldReturnOne() {
        int equipmentId = 1;
        PowerSupply power = new PowerSupply("", 0, equipmentId);
        handler.addDetails(power);

        assertEquals(1, handler.countDetails(equipmentId));
    }

    @Test
    void getDetailsIdByEquipment_powerDosNotExist_shouldReturnEmptyList() {
        int equipmentId = 1;

        assertEquals(Collections.emptyList(), handler.getDetailsIdByEquipment(equipmentId));
    }

    @Test
    void getDetailsIdByEquipment_powerExist_shouldReturnListOfInteger() {
        int equipmentId = 1;
        int detailsId = 1;
        PowerSupply power = new PowerSupply("", 0, equipmentId);
        handler.addDetails(power);
        List<Integer> expected = List.of(detailsId);

        assertEquals(expected, handler.getDetailsIdByEquipment(equipmentId));
    }
}