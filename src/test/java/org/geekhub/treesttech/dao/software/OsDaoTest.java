package org.geekhub.treesttech.dao.software;

import org.geekhub.treesttech.model.software.OperatingSystem;
import org.geekhub.treesttech.model.software.OperatingSystemBit;
import org.geekhub.treesttech.service.SqlFileHandlerImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.DataJdbcTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

@DataJdbcTest
class OsDaoTest {

    private final NamedParameterJdbcTemplate namedJdbcTemplate;
    private final OsHandler handler;

    @Autowired
    public OsDaoTest(NamedParameterJdbcTemplate namedJdbcTemplate) {
        this.namedJdbcTemplate = namedJdbcTemplate;
        this.handler = new OsDao(namedJdbcTemplate, new SqlFileHandlerImpl());
    }

    @BeforeEach
    void setUp() {
        String sqlBefore = """
            INSERT INTO companies (company_name) VALUES ('test');
            INSERT INTO equipment (equipment_name, equipment_group, company_id)
                VALUES (' ', 1, 1);
            """;
        namedJdbcTemplate.update(sqlBefore, Collections.emptyMap());
    }

    @AfterEach
    void tearDown() {
        String sqlAfter = """
            DELETE FROM companies;
            ALTER TABLE companies ALTER COLUMN company_id RESTART WITH 1;
            ALTER TABLE equipment ALTER COLUMN equipment_id RESTART WITH 1;
            """;
        namedJdbcTemplate.update(sqlAfter, Collections.emptyMap());
    }

    @Test
    void getOsById_nothingAdding_shouldThrowEmptyResultDataAccessException() {
        int equipmentId = 1;

        assertThrows(EmptyResultDataAccessException.class, () -> handler.getOsById(equipmentId));
    }

    @Test
    void getOsById_addingOs_shouldReturnOs() {
        int equipmentId = 1;
        OperatingSystem os = new OperatingSystem("", OperatingSystemBit.x64, equipmentId);
        handler.addOs(os);

        assertEquals(os, handler.getOsById(equipmentId));
    }

    @Test
    void addOs_equipmentExist_shouldReturnOs() {
        int equipmentId = 1;
        OperatingSystem os = new OperatingSystem("", OperatingSystemBit.x64, equipmentId);
        handler.addOs(os);

        assertEquals(os, handler.getOsById(equipmentId));
    }

    @Test
    void addOs_equipmentNonExist_shouldThrowDataIntegrityViolationException() {
        int equipmentId = 2;
        OperatingSystem os = new OperatingSystem("", OperatingSystemBit.x64, equipmentId);

        assertThrows(DataIntegrityViolationException.class, () -> handler.addOs(os));
    }

    @Test
    void removeOs_osDosNotExist_shouldNotThrowAnyException() {
        int equipmentId = 1;

        assertDoesNotThrow(() -> handler.removeOs(equipmentId));
    }

    @Test
    void removeOs_addingOs_shouldThrowEmptyResultDataAccessException() {
        int equipmentId = 1;
        OperatingSystem os = new OperatingSystem("", OperatingSystemBit.x64, equipmentId);
        handler.addOs(os);
        handler.removeOs(equipmentId);

        assertThrows(EmptyResultDataAccessException.class, () -> handler.getOsById(equipmentId));
    }

    @Test
    void updateOs_nothingAdding_shouldNotThrowAnyException() {
        int equipmentId = 1;
        OperatingSystem os = new OperatingSystem("", OperatingSystemBit.x64, equipmentId);

        assertDoesNotThrow(() -> handler.updateOs(os, equipmentId));
    }

    @Test
    void updateOs_addingOs_shouldReturnUpdatedOs() {
        int equipmentId = 1;
        OperatingSystem os = new OperatingSystem("", OperatingSystemBit.x64, equipmentId);
        handler.addOs(os);
        OperatingSystem osUpdated = new OperatingSystem("", OperatingSystemBit.x86, equipmentId);
        handler.updateOs(osUpdated,equipmentId);

        assertEquals(osUpdated, handler.getOsById(equipmentId));
    }
}