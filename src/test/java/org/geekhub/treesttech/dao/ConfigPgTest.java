package org.geekhub.treesttech.dao;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ConfigPgTest {

    private ConfigHandler config;

    @BeforeEach
    void setUp() {
        config = new ConfigPg();
    }

    @Test
    void getConfig_shouldReturnNotNull() {
        assertNotNull(config.getConfig());
    }

    @Test
    void getConfig_shouldReturnPropertyH2DriverClassName() {
        String h2Driver = "org.postgresql.Driver";
        assertEquals(h2Driver, config.getConfig().getDriverClassName());
    }
}