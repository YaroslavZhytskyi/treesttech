package org.geekhub.treesttech.dao;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class ConfigH2Test {

    private ConfigHandler config;

    @BeforeEach
    void setUp() {
        config = new ConfigH2();
    }

    @Test
    void getConfig_shouldReturnNotNull() {
        assertNotNull(config.getConfig());
    }

    @Test
    void getConfig_shouldReturnPropertyH2DriverClassName() {
        String h2Driver = "org.h2.Driver";
        assertEquals(h2Driver, config.getConfig().getDriverClassName());
    }
}