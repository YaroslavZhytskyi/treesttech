package org.geekhub.treesttech.dao.equipment;

import org.geekhub.treesttech.model.equipment.EquipmentCommon;
import org.geekhub.treesttech.service.SqlFileHandlerImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.DataJdbcTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJdbcTest
class EquipmentCommonDaoTest {

    private final NamedParameterJdbcTemplate namedJdbcTemplate;
    private final EquipmentCommonHandler handler;

    @Autowired
    public EquipmentCommonDaoTest(NamedParameterJdbcTemplate namedJdbcTemplate) {
        this.namedJdbcTemplate = namedJdbcTemplate;
        this.handler = new EquipmentCommonDao(namedJdbcTemplate, new SqlFileHandlerImpl());
    }

    @BeforeEach
    void setUp() {
        String sqlBefore = """
            INSERT INTO companies (company_name) VALUES ('test');
            """;
        namedJdbcTemplate.update(sqlBefore, Collections.emptyMap());
    }

    @AfterEach
    void tearDown() {
        String sqlAfter = """
            DELETE FROM companies;
            ALTER TABLE companies ALTER COLUMN company_id RESTART WITH 1;
            ALTER TABLE equipment ALTER COLUMN equipment_id RESTART WITH 1;
            """;
        namedJdbcTemplate.update(sqlAfter, Collections.emptyMap());
    }

    @Test
    void getEquipmentById_nothingAdding_shouldThrowEmptyResultDataAccessException() {
        int equipmentId = 1;

        assertThrows(EmptyResultDataAccessException.class,
            () -> handler.getEquipmentById(equipmentId));
    }

    @Test
    void getEquipmentById_addingEquipment_shouldReturnEquipmentCommon() {
        int groupId = 1;
        int companyId = 1;
        EquipmentCommon equipment = new EquipmentCommon("", groupId, companyId);
        handler.addEquipment(equipment);
        int equipmentId = 1;
        equipment.setId(equipmentId);

        assertEquals(equipment, handler.getEquipmentById(equipmentId));
    }

    @Test
    void getAllEquipment_nothingAdding_shouldReturnEmptyList() {
        assertEquals(Collections.emptyList(), handler.getAllEquipment());
    }

    @Test
    void getAllEquipment_addingOneEquipment_shouldReturnList() {
        int groupId = 1;
        int companyId = 1;
        EquipmentCommon equipment = new EquipmentCommon("", groupId, companyId);
        handler.addEquipment(equipment);
        int equipmentId = 1;
        equipment.setId(equipmentId);
        List<EquipmentCommon> expected = List.of(equipment);

        assertEquals(expected, handler.getAllEquipment());
    }

    @Test
    void getAllEquipmentByCompanyId_nonExistCompany_shouldReturnEmptyList() {
        int nonExistCompanyId = 2;
        assertEquals(Collections.emptyList(),
            handler.getAllEquipmentByCompanyId(nonExistCompanyId));
    }

    @Test
    void getAllEquipmentByCompanyId_existCompany_shouldReturnEmptyList() {
        int nonExistCompanyId = 1;
        assertEquals(Collections.emptyList(),
            handler.getAllEquipmentByCompanyId(nonExistCompanyId));
    }

    @Test
    void getAllEquipmentByCompanyId_addingEquipment_existCompany_shouldReturnList() {
        int groupId = 1;
        int companyId = 1;
        EquipmentCommon equipment = new EquipmentCommon("", groupId, companyId);
        handler.addEquipment(equipment);
        int equipmentId = 1;
        equipment.setId(equipmentId);
        List<EquipmentCommon> expected = List.of(equipment);

        assertEquals(expected, handler.getAllEquipmentByCompanyId(companyId));
    }

    @Test
    void addEquipment_companyExist_shouldReturnEquipmentCommon() {
        int groupId = 1;
        int companyId = 1;
        EquipmentCommon equipment = new EquipmentCommon("", groupId, companyId);
        handler.addEquipment(equipment);
        int equipmentId = 1;
        equipment.setId(equipmentId);

        assertEquals(equipment, handler.getEquipmentById(equipmentId));
    }

    @Test
    void addEquipment_companyNonExist_shouldThrowDataIntegrityViolationException() {
        int groupId = 1;
        int companyId = 2;
        EquipmentCommon equipment = new EquipmentCommon("", groupId, companyId);

        assertThrows(DataIntegrityViolationException.class, () -> handler.addEquipment(equipment));
    }

    @Test
    void removeEquipmentById_equipmentDosNotExist_shouldNotThrowAnyException() {
        int equipmentId = 1;

        assertDoesNotThrow(() -> handler.removeEquipmentById(equipmentId));
    }

    @Test
    void removeEquipmentById_addingTwoEquipments_shouldReturnListWithOneEquipmentCommon() {
        int groupId = 1;
        int companyId = 1;
        EquipmentCommon equipment1 = new EquipmentCommon("", groupId, companyId);
        EquipmentCommon equipment2 = new EquipmentCommon("", groupId, companyId);
        handler.addEquipment(equipment1);
        handler.addEquipment(equipment2);
        int equipmentId1 = 1;
        int equipmentId2 = 2;
        equipment1.setId(equipmentId1);
        handler.removeEquipmentById(equipmentId2);
        List<EquipmentCommon> expected = List.of(equipment1);

        assertEquals(expected, handler.getAllEquipment());
    }

    @Test
    void updateEquipment_nothingAdding_shouldNotThrowAnyException() {
        int groupId = 1;
        int companyId = 1;
        EquipmentCommon equipment = new EquipmentCommon("", groupId, companyId);
        int equipmentId = 1;
        equipment.setId(equipmentId);

        assertDoesNotThrow(() -> handler.updateEquipment(equipment, equipmentId));
    }

    @Test
    void updateEquipment_addingEquipment_shouldReturnUpdatedEquipment() {
        int groupId = 1;
        int companyId = 1;
        EquipmentCommon equipment = new EquipmentCommon("", groupId, companyId);
        handler.addEquipment(equipment);
        int equipmentId = 1;
        equipment.setId(equipmentId);
        equipment.setName("Alpha");
        handler.updateEquipment(equipment, equipmentId);

        assertEquals(equipment, handler.getEquipmentById(equipmentId));
    }

    @Test
    void getAllEquipmentIdByCompany_companyNonExist_shouldReturnEmptyList() {
        int companyId = 2;
        assertEquals(Collections.emptyList(), handler.getAllEquipmentIdByCompany(companyId));
    }

    @Test
    void getAllEquipmentIdByCompany_companyExistWithoutEquipment_shouldReturnEmptyList() {
        int companyId = 1;
        assertEquals(Collections.emptyList(), handler.getAllEquipmentIdByCompany(companyId));
    }

    @Test
    void getAllEquipmentIdByCompany_companyExistWithEquipment_shouldReturnListOfEquipmentId() {
        int groupId = 1;
        int companyId = 1;
        EquipmentCommon equipment = new EquipmentCommon("", groupId, companyId);
        handler.addEquipment(equipment);
        int equipmentId = 1;
        List<Integer> expected = List.of(equipmentId);

        assertEquals(expected, handler.getAllEquipmentIdByCompany(companyId));
    }
}