package org.geekhub.treesttech.dao.equipment;

import org.geekhub.treesttech.model.equipment.EquipmentInfo;
import org.geekhub.treesttech.service.SqlFileHandlerImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.DataJdbcTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.time.LocalDate;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

@DataJdbcTest
class EquipmentInfoDaoTest {

    private final NamedParameterJdbcTemplate namedJdbcTemplate;
    private final EquipmentInfoHandler handler;

    @Autowired
    public EquipmentInfoDaoTest(NamedParameterJdbcTemplate namedJdbcTemplate) {
        this.namedJdbcTemplate = namedJdbcTemplate;
        this.handler = new EquipmentInfoDao(namedJdbcTemplate, new SqlFileHandlerImpl());
    }

    @BeforeEach
    void setUp() {
        String sqlBefore = """
            INSERT INTO companies (company_name) VALUES ('test');
            INSERT INTO equipment (equipment_name, equipment_group, company_id)
                VALUES (' ', 1, 1);
            """;
        namedJdbcTemplate.update(sqlBefore, Collections.emptyMap());
    }

    @AfterEach
    void tearDown() {
        String sqlAfter = """
            DELETE FROM companies;
            ALTER TABLE companies ALTER COLUMN company_id RESTART WITH 1;
            ALTER TABLE equipment ALTER COLUMN equipment_id RESTART WITH 1;
            """;
        namedJdbcTemplate.update(sqlAfter, Collections.emptyMap());
    }

    @Test
    void getEquipmentInfo_nothingAdding_shouldThrowEmptyResultDataAccessException() {
        int equipmentId = 1;

        assertThrows(EmptyResultDataAccessException.class, () -> handler.getEquipmentInfo(equipmentId));
    }

    @Test
    void getEquipmentInfo_addingEquipmentInfo_shouldReturnEquipmentInfo() {
        int equipmentId = 1;
        EquipmentInfo info
            = new EquipmentInfo("", "", LocalDate.now(), "", equipmentId);
        handler.addEquipmentInfo(info);

        assertEquals(info, handler.getEquipmentInfo(equipmentId));
    }

    @Test
    void addEquipmentInfo_equipmentExist_shouldReturnEquipmentInfo() {
        int equipmentId = 1;
        EquipmentInfo info
            = new EquipmentInfo("", "", LocalDate.now(), "", equipmentId);
        handler.addEquipmentInfo(info);

        assertEquals(info, handler.getEquipmentInfo(equipmentId));
    }

    @Test
    void addEquipmentInfo_equipmentNonExist_shouldThrowDataIntegrityViolationException() {
        int equipmentId = 2;
        EquipmentInfo info
            = new EquipmentInfo("", "", LocalDate.now(), "", equipmentId);

        assertThrows(DataIntegrityViolationException.class, () -> handler.addEquipmentInfo(info));
    }

    @Test
    void removeEquipmentInfo_equipmentInfoDosNotExist_shouldNotThrowAnyException() {
        int equipmentId = 1;

        assertDoesNotThrow(() -> handler.removeEquipmentInfo(equipmentId));
    }

    @Test
    void removeEquipmentInfo_addingEquipmentInfo_shouldThrowEmptyResultDataAccessException() {
        int equipmentId = 1;
        EquipmentInfo info
            = new EquipmentInfo("", "", LocalDate.now(), "", equipmentId);
        handler.addEquipmentInfo(info);
        handler.removeEquipmentInfo(equipmentId);

        assertThrows(EmptyResultDataAccessException.class, () -> handler.getEquipmentInfo(equipmentId));
    }

    @Test
    void updateEquipmentInfo_nothingAdding_shouldNotThrowAnyException() {
        int equipmentId = 1;
        EquipmentInfo info
            = new EquipmentInfo("", "", LocalDate.now(), "", equipmentId);

        assertDoesNotThrow(() -> handler.updateEquipmentInfo(info, equipmentId));
    }

    @Test
    void updateEquipmentInfo_addingEquipmentInfo_shouldReturnUpdatedEquipmentInfo() {
        int equipmentId = 1;
        EquipmentInfo info
            = new EquipmentInfo("", "", LocalDate.now(), "", equipmentId);
        handler.addEquipmentInfo(info);
        EquipmentInfo infoUpdated
            = new EquipmentInfo("", "", LocalDate.now(), "", equipmentId);
        handler.updateEquipmentInfo(infoUpdated, equipmentId);

        assertEquals(infoUpdated, handler.getEquipmentInfo(equipmentId));
    }

    @Test
    void addEmptyEquipmentInfo_equipmentExist_shouldReturnEquipmentInfo() {
        int equipmentId = 1;
        final LocalDate date = LocalDate.parse("1900-01-01");
        EquipmentInfo expected
            = new EquipmentInfo(null, null, date, null, equipmentId);
        handler.addEmptyEquipmentInfo(equipmentId);

        assertEquals(expected, handler.getEquipmentInfo(equipmentId));
    }

    @Test
    void addEmptyEquipmentInfo_equipmentNonExist_shouldThrowDataIntegrityViolationException() {
        int equipmentId = 2;

        assertThrows(DataIntegrityViolationException.class,
            () -> handler.addEmptyEquipmentInfo(equipmentId));
    }
}