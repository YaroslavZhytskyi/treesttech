package org.geekhub.treesttech.dao.user;

import org.geekhub.treesttech.model.User;
import org.geekhub.treesttech.service.SqlFileHandlerImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.DataJdbcTest;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJdbcTest
class UserDaoTest {

    private final NamedParameterJdbcTemplate namedJdbcTemplate;
    private final UserHandler handler;

    @Autowired
    public UserDaoTest(NamedParameterJdbcTemplate namedJdbcTemplate) {
        this.namedJdbcTemplate = namedJdbcTemplate;
        this.handler = new UserDao(namedJdbcTemplate, new SqlFileHandlerImpl());
    }

    @BeforeEach
    void setUp() {
        String sqlBefore = """
            DELETE FROM users;
            ALTER TABLE users ALTER COLUMN user_id RESTART WITH 1;
            """;
        namedJdbcTemplate.update(sqlBefore, Collections.emptyMap());
    }

    @Test
    void getAllUsers_nothingAdding_shouldReturnEmptyList() {
        assertEquals(Collections.emptyList(), handler.getAllUsers());
    }

    @Test
    void getAllUsers_addingUser_shouldReturnListOfUser() {
        User user = new User("", "", "", "", true);
        handler.addUser(user);
        int userId = 1;
        user.setId(userId);
        List<User> expected = List.of(user);

        assertEquals(expected, handler.getAllUsers());
    }

    @Test
    void getUserById_nothingAdding_shouldThrowEmptyResultDataAccessException() {
        int userId = 1;

        assertThrows(EmptyResultDataAccessException.class, () -> handler.getUserById(userId));
    }

    @Test
    void getUserById_addingUser_shouldReturnUser() {
        User user = new User("", "", "", "", true);
        handler.addUser(user);
        int userId = 1;
        user.setId(userId);

        assertEquals(user, handler.getUserById(userId));
    }

    @Test
    void removeUserById_userDosNotExist_shouldNotThrowAnyException() {
        int userId = 1;

        assertDoesNotThrow(() -> handler.removeUserById(userId));
    }

    @Test
    void removeUserById_userExist_shouldReturnEmptyList() {
        User user = new User("", "", "", "", true);
        handler.addUser(user);
        int userId = 1;
        handler.removeUserById(userId);

        assertEquals(Collections.emptyList(), handler.getAllUsers());
    }

    @Test
    void addUser_shouldReturnUser() {
        User user = new User("", "", "", "", true);
        handler.addUser(user);
        int userId = 1;
        user.setId(userId);

        assertEquals(user, handler.getUserById(userId));
    }

    @Test
    void addUser_addingTwoUsersWithSameNames_shouldThrowDuplicateKeyException() {
        User user1 = new User("", "", "", "", true);
        User user2 = new User("", "", "", "", true);
        handler.addUser(user1);

        assertThrows(DuplicateKeyException.class, () -> handler.addUser(user2));
    }

    @Test
    void updateUser_nothingAdding_shouldNotThrowAnyException() {
        User user = new User("", "", "", "", true);
        int userId = 1;

        assertDoesNotThrow(() -> handler.updateUser(user, userId));
    }

    @Test
    void updateUser_addingUser_shouldReturnUpdatedUser() {
        User user = new User("", "", "", "", true);
        handler.addUser(user);
        User userUpdated = new User("", "", "", "", false);
        int userId = 1;
        userUpdated.setId(userId);
        handler.updateUser(userUpdated, userId);

        assertEquals(userUpdated, handler.getUserById(userId));
    }

    @Test
    void changeStateOfUser_userNonExist_shouldNotThrowAnyException() {
        int userId = 1;

        assertDoesNotThrow(() -> handler.changeStateOfUser(true, userId));
    }

    @Test
    void changeStateOfUser_userExist_shouldReturnUserWithChangedState() {
        User user = new User("", "", "", "", true);
        handler.addUser(user);
        int userId = 1;
        handler.changeStateOfUser(false, userId);
        user.setId(userId);
        user.setEnabled(false);

        assertEquals(user, handler.getUserById(userId));
    }

    @Test
    void getAllRoles_shouldReturnListOfDefaultRoles() {
        List<String> defaultRoles = List.of("ROLE_USER", "ROLE_ADMIN", "ROLE_SUPER_ADMIN");

        assertEquals(defaultRoles, handler.getAllRoles());
    }

    @Test
    void getUserByName_nothingAdding_shouldThrowEmptyResultDataAccessException() {
        String userName = "";

        assertThrows(EmptyResultDataAccessException.class, () -> handler.getUserByName(userName));
    }

    @Test
    void getUserByName_addingUser_shouldReturnUser() {
        String userName = "";
        User user = new User(userName, "", "", "", true);
        handler.addUser(user);
        int userId = 1;
        user.setId(userId);

        assertEquals(user, handler.getUserByName(userName));
    }
}