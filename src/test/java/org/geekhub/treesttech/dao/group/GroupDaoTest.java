package org.geekhub.treesttech.dao.group;

import org.geekhub.treesttech.model.equipment.EquipmentGroup;
import org.geekhub.treesttech.model.equipment.EquipmentType;
import org.geekhub.treesttech.service.SqlFileHandlerImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.DataJdbcTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJdbcTest
class GroupDaoTest {

    private final NamedParameterJdbcTemplate namedJdbcTemplate;
    private final GroupHandler handler;

    @Autowired
    public GroupDaoTest(NamedParameterJdbcTemplate namedJdbcTemplate) {
        this.namedJdbcTemplate = namedJdbcTemplate;
        this.handler = new GroupDao(namedJdbcTemplate, new SqlFileHandlerImpl());
    }

    @BeforeEach
    void setUp() {
        String sqlBefore = """
            DELETE FROM groups;
            ALTER TABLE groups ALTER COLUMN group_id RESTART WITH 1;
            """;
        namedJdbcTemplate.update(sqlBefore, Collections.emptyMap());
    }

    @Test
    void getGroupById_nothingAdding_shouldThrowEmptyResultDataAccessException() {
        int groupId = 1;

        assertThrows(EmptyResultDataAccessException.class, () -> handler.getGroupById(groupId));
    }

    @Test
    void getGroupById_addingEquipmentGroup_shouldReturnEquipmentGroup() {
        int groupId = 1;
        EquipmentGroup group = new EquipmentGroup("", EquipmentType.ENDPOINT);
        handler.addGroup(group);
        group.setId(groupId);

        assertEquals(group, handler.getGroupById(groupId));
    }

    @Test
    void getAllGroups_nothingAdding_shouldReturnEmptyList() {
        assertEquals(Collections.emptyList(), handler.getAllGroups());
    }

    @Test
    void getAllGroups_addingEquipmentGroup_shouldReturnList() {
        int groupId = 1;
        EquipmentGroup group = new EquipmentGroup("", EquipmentType.ENDPOINT);
        handler.addGroup(group);
        group.setId(groupId);
        List<EquipmentGroup> expected = List.of(group);

        assertEquals(expected, handler.getAllGroups());
    }

    @Test
    void addGroup_shouldReturnEquipmentGroup() {
        int groupId = 1;
        EquipmentGroup group = new EquipmentGroup("", EquipmentType.ENDPOINT);
        handler.addGroup(group);
        group.setId(groupId);

        assertEquals(group, handler.getGroupById(groupId));
    }

    @Test
    void removeGroup_nothingAdding_shouldThrowNothing() {
        int groupId = 1;

        assertDoesNotThrow(() -> handler.removeGroup(groupId));
    }

    @Test
    void removeGroup_addingEquipmentGroup_shouldReturnEmptyList() {
        int groupId = 1;
        EquipmentGroup group = new EquipmentGroup("", EquipmentType.ENDPOINT);
        handler.addGroup(group);
        handler.removeGroup(groupId);

        assertEquals(Collections.emptyList(), handler.getAllGroups());
    }

    @Test
    void updateGroup_nothingAdding_shouldNotThrowAnyException() {
        int groupId = 1;
        EquipmentGroup group = new EquipmentGroup("", EquipmentType.ENDPOINT);

        assertDoesNotThrow(() -> handler.updateGroup(group, groupId));
    }

    @Test
    void updateGroup_addingEquipmentGroup_shouldReturnUpdatedGroup() {
        int groupId = 1;
        EquipmentGroup group = new EquipmentGroup("", EquipmentType.ENDPOINT);
        handler.addGroup(group);
        EquipmentGroup groupUpdated = new EquipmentGroup("", EquipmentType.SERVER);
        groupUpdated.setId(groupId);
        handler.updateGroup(groupUpdated, groupId);

        assertEquals(groupUpdated, handler.getGroupById(groupId));
    }
}