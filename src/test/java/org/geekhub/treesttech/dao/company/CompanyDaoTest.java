package org.geekhub.treesttech.dao.company;

import org.geekhub.treesttech.model.company.Company;
import org.geekhub.treesttech.service.SqlFileHandlerImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.DataJdbcTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJdbcTest
class CompanyDaoTest {

    private final NamedParameterJdbcTemplate namedJdbcTemplate;
    private final CompanyHandler handler;

    @Autowired
    public CompanyDaoTest(NamedParameterJdbcTemplate namedJdbcTemplate) {
        this.namedJdbcTemplate = namedJdbcTemplate;
        this.handler = new CompanyDao(namedJdbcTemplate, new SqlFileHandlerImpl());
    }

    @BeforeEach
    void setUp() {
        String sqlClearScript = """
            DELETE FROM companies;
            ALTER TABLE companies ALTER COLUMN company_id RESTART WITH 1;
            """;
        namedJdbcTemplate.update(sqlClearScript, Collections.emptyMap());
    }

    @Test
    void getAllCompanies_nothingAdding_shouldReturnEmptyList() {
        assertEquals(Collections.emptyList(), handler.getAllCompanies());
    }

    @Test
    void getAllCompanies_addingCompany_shouldReturnListWithOneCompanyWithId() {
        Company company = new Company("test");
        int userId = 1;
        handler.addCompany(company, userId);
        int expectedId = 1;
        int expectedCompaniesAmount = 1;

        assertEquals(expectedCompaniesAmount, handler.getAllCompanies().size());
        assertEquals(expectedId, handler.getAllCompanies().get(0).getId());
    }

    @Test
    void getAllCompanies_addingCompany_shouldReturnList() {
        Company company = new Company("test");
        int userId = 1;
        handler.addCompany(company, userId);
        company.setId(1);

        assertEquals(List.of(company), handler.getAllCompanies());
    }

    @Test
    void getCompanyById_addingCompany_shouldReturnCompany() {
        Company company = new Company("test");
        int userId = 1;
        handler.addCompany(company, userId);
        int companyId = 1;
        company.setId(companyId);

        assertEquals(company, handler.getCompanyById(companyId));
    }

    @Test
    void getCompanyById_nothingAdding_shouldThrowEmptyResultDataAccessException() {
        int companyId = 1;

        assertThrows(EmptyResultDataAccessException.class,
            () -> handler.getCompanyById(companyId));
    }

    @Test
    void getCompaniesByUserId_nothingAdding_shouldReturnEmptyList() {
        int userId = 1;

        assertEquals(Collections.emptyList(), handler.getCompaniesByUserId(userId));
    }

    @Test
    void getCompaniesByUserId_addedOneCompany_shouldReturnListWithOneCompany() {
        Company company = new Company("test");
        int userId = 1;
        handler.addCompany(company, userId);
        int companyId = 1;
        company.setId(companyId);
        int expectedCompaniesAmount = 1;

        assertEquals(expectedCompaniesAmount, handler.getCompaniesByUserId(userId).size());
        assertEquals(company, handler.getCompaniesByUserId(userId).get(0));
    }

    @Test
    void addCompany_addedOneCompany() {
        int companiesAmountBeforeAdding = 0;
        assertEquals(companiesAmountBeforeAdding, handler.getAllCompanies().size());

        Company company = new Company("test");
        int userId = 1;
        handler.addCompany(company, userId);
        int companyId = 1;
        company.setId(companyId);
        int companiesAmountAfterAdding = 1;

        assertEquals(companiesAmountAfterAdding, handler.getAllCompanies().size());
        assertEquals(company, handler.getAllCompanies().get(0));
    }

    @Test
    void addCompany_addedOneCompanyWithNonExistUserId_shouldThrowDataIntegrityViolationException() {
        Company company = new Company("test");
        int nonExistUserId = 10;

        assertThrows(DataIntegrityViolationException.class,
            () -> handler.addCompany(company, nonExistUserId));
    }

    @Test
    void addCompany_addedTwoCompaniesWithSameNames_shouldThrowDuplicateKeyException() {
        Company company1 = new Company("test");
        Company company2 = new Company("test");
        int userId = 1;
        handler.addCompany(company1, userId);

        assertThrows(DuplicateKeyException.class,
            () -> handler.addCompany(company2, userId));
    }

    @Test
    void removeCompany_emptyTable_nothingShouldThrow() {
        int companyId = 1;
        assertDoesNotThrow(() -> handler.removeCompany(companyId));
    }

    @Test
    void removeCompany_addedTwoCompanies_shouldStayOneCompany() {
        Company company1 = new Company("Alpha");
        Company company2 = new Company("Beta");
        int userId = 1;
        handler.addCompany(company1, userId);
        handler.addCompany(company2, userId);
        int expectedCompaniesAmountAfterAdding = 2;

        assertEquals(expectedCompaniesAmountAfterAdding,
            handler.getAllCompanies().size());

        int companyId = 1;
        handler.removeCompany(companyId);
        int expectedCompaniesAmountAfterRemoving = 1;

        assertEquals(expectedCompaniesAmountAfterRemoving,
            handler.getAllCompanies().size());
    }

    @Test
    void updateCompany_updatedExistCompany_shouldReturnUpdatedCompany() {
        Company originCompany = new Company("Alpha");
        int userId = 1;
        handler.addCompany(originCompany, userId);
        int companyId = 1;

        Company updatedCompany = new Company("Beta");
        updatedCompany.setId(companyId);
        handler.updateCompany(updatedCompany, companyId);

        assertEquals(updatedCompany, handler.getCompanyById(companyId));
    }

    @Test
    void updateCompany_updatedNonExistCompany_shouldNothingHappen() {
        int companyId = 1;
        Company updatedCompany = new Company("Beta");
        updatedCompany.setId(companyId);

        assertDoesNotThrow(() -> handler.updateCompany(updatedCompany, companyId));
        assertEquals(Collections.emptyList(), handler.getAllCompanies());
    }

    @Test
    void assignCompanyToUser() {
        String sqlAddUser = """
            INSERT INTO users (username, password, user_email, enabled)
            VALUES (
                       'user',
                       'user',
                       '',
                       true);
            INSERT INTO authorities
            VALUES ('user', 'ROLE_USER');
            """;
        namedJdbcTemplate.update(sqlAddUser, Collections.emptyMap());
        int newUserId = 2;

        assertEquals(Collections.emptyList(), handler.getCompaniesByUserId(newUserId));

        int oldUserId = 1;
        Company company = new Company("Alpha");
        int companyId = 1;
        company.setId(companyId);
        handler.addCompany(company, oldUserId);
        int expectedCompaniesAmount = 1;

        assertEquals(expectedCompaniesAmount, handler.getCompaniesByUserId(oldUserId).size());

        handler.assignCompanyToUser(companyId, newUserId);

        assertEquals(expectedCompaniesAmount, handler.getCompaniesByUserId(newUserId).size());
    }
}