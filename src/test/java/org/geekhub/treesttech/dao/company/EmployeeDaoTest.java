package org.geekhub.treesttech.dao.company;

import org.geekhub.treesttech.dao.equipment.EquipmentCommonDao;
import org.geekhub.treesttech.model.company.Employee;
import org.geekhub.treesttech.model.equipment.EquipmentCommon;
import org.geekhub.treesttech.service.SqlFileHandlerImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.DataJdbcTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJdbcTest
class EmployeeDaoTest {

    private final NamedParameterJdbcTemplate namedJdbcTemplate;
    private final EmployeeHandler handler;

    @Autowired
    public EmployeeDaoTest(NamedParameterJdbcTemplate namedJdbcTemplate) {
        this.namedJdbcTemplate = namedJdbcTemplate;
        this.handler = new EmployeeDao(namedJdbcTemplate, new SqlFileHandlerImpl());
    }

    @BeforeEach
    void setUp() {
        String sqlBefore = """
            INSERT INTO companies (company_name) VALUES ('test');
            """;
        namedJdbcTemplate.update(sqlBefore, Collections.emptyMap());
    }

    @AfterEach
    void tearDown() {
        String sqlAfter = """
            DELETE FROM companies;
            ALTER TABLE companies ALTER COLUMN company_id RESTART WITH 1;
            DELETE FROM employees;
            ALTER TABLE employees ALTER COLUMN employee_id RESTART WITH 1;
            ALTER TABLE equipment ALTER COLUMN equipment_id RESTART WITH 1;
            """;
        namedJdbcTemplate.update(sqlAfter, Collections.emptyMap());
    }

    @Test
    void getEmployeeById_nothingAdding_shouldThrowEmptyResultDataAccessException() {
        int employeeId = 1;

        assertThrows(EmptyResultDataAccessException.class,
            () -> handler.getEmployeeById(employeeId));
    }

    @Test
    void getEmployeeById_addingOneEmployee_shouldReturnEmployee() {
        int companyId = 1;
        Employee employee = new Employee("", "", "", "", companyId);
        handler.addEmployee(employee);
        int employeeId = 1;
        employee.setId(employeeId);

        assertEquals(employee, handler.getEmployeeById(employeeId));
    }

    @Test
    void getAllEmployees_nothingAdding_shouldReturnEmptyList() {
        assertEquals(Collections.emptyList(), handler.getAllEmployees());
    }

    @Test
    void getAllEmployees_addingEmployee_shouldReturnListOfEmployees() {
        int companyId = 1;
        Employee employee = new Employee("", "", "", "", companyId);
        handler.addEmployee(employee);
        int employeeId = 1;
        employee.setId(employeeId);
        List<Employee> expected = List.of(employee);

        assertEquals(expected, handler.getAllEmployees());
    }

    @Test
    void getAllEmployeesByCompany_nothingAdding_companyExist_shouldReturnEmptyList() {
        int companyId = 1;
        assertEquals(Collections.emptyList(), handler.getAllEmployeesByCompany(companyId));
    }

    @Test
    void getAllEmployeesByCompany_nothingAdding_companyNotExist_shouldReturnEmptyList() {
        int companyId = 2;
        assertEquals(Collections.emptyList(), handler.getAllEmployeesByCompany(companyId));
    }

    @Test
    void getAllEmployeesByCompany_addingEmployee_shouldReturnListOfEmployee() {
        int companyId = 1;
        Employee employee = new Employee("", "", "", "", companyId);
        handler.addEmployee(employee);
        int employeeId = 1;
        employee.setId(employeeId);
        List<Employee> expected = List.of(employee);

        assertEquals(expected, handler.getAllEmployeesByCompany(companyId));
    }

    @Test
    void addEmployee_addingEmployee_companyNotExist_shouldThrowDataIntegrityViolationException() {
        int companyId = 2;
        Employee employee = new Employee("", "", "", "", companyId);

        assertThrows(DataIntegrityViolationException.class, () -> handler.addEmployee(employee));
    }

    @Test
    void addEmployee_addingEmployee_companyExist_shouldReturnEmployee() {
        int companyId = 1;
        Employee employee = new Employee("", "", "", "", companyId);
        handler.addEmployee(employee);
        int employeeId = 1;
        employee.setId(employeeId);

        assertEquals(employee, handler.getEmployeeById(employeeId));
    }

    @Test
    void removeEmployee_nothingAdding_shouldNotThrowAnyException() {
        int employeeId = 1;
        assertDoesNotThrow(() -> handler.removeEmployee(employeeId));
    }

    @Test
    void removeEmployee_addingTwoEmployees_shouldReturnListWithOneEmployee() {
        int companyId = 1;
        Employee employee1 = new Employee("", "", "", "", companyId);
        Employee employee2 = new Employee("", "", "", "", companyId);
        handler.addEmployee(employee1);
        handler.addEmployee(employee2);
        int employeeId1 = 1;
        int employeeId2 = 2;
        employee1.setId(employeeId1);
        employee2.setId(employeeId2);
        handler.removeEmployee(employeeId1);
        List<Employee> expected = List.of(employee2);

        assertEquals(expected, handler.getAllEmployees());
    }

    @Test
    void updateEmployee_nothingAdding_shouldNotThrowAnyException() {
        int companyId = 1;
        Employee employee = new Employee("", "", "", "", companyId);
        int employeeId = 1;
        employee.setId(employeeId);

        assertDoesNotThrow(() -> handler.updateEmployee(employee, employeeId));
    }

    @Test
    void updateEmployee_addingEmployee_shouldReturnUpdatedEmployee() {
        int companyId = 1;
        Employee employee = new Employee("", "", "", "", companyId);
        handler.addEmployee(employee);

        int employeeId = 1;
        String updatedName = "Beta";
        employee.setId(employeeId);
        employee.setName(updatedName);
        handler.updateEmployee(employee, employeeId);

        assertEquals(employee, handler.getEmployeeById(employeeId));
    }

    @Test
    void assignEquipment_noEmployee_noEquipment_shouldThrowDataIntegrityViolationException() {
        int equipmentId = 1;
        int employeeId = 1;

        assertThrows(DataIntegrityViolationException.class,
            () -> handler.assignEquipment(employeeId, equipmentId));
    }

    @Test
    void assignEquipment_noEquipment_shouldThrowDataIntegrityViolationException() {
        int companyId = 1;
        Employee employee = new Employee("", "", "", "", companyId);
        handler.addEmployee(employee);
        int equipmentId = 1;
        int employeeId = 1;

        assertThrows(DataIntegrityViolationException.class,
            () -> handler.assignEquipment(employeeId, equipmentId));
    }

    @Test
    void assignEquipment_noEmployee_shouldThrowDataIntegrityViolationException() {
        int companyId = 1;
        EquipmentCommon equipment = new EquipmentCommon("", 1, companyId);
        new EquipmentCommonDao(namedJdbcTemplate, new SqlFileHandlerImpl()).addEquipment(equipment);
        int equipmentId = 1;
        int employeeId = 1;

        assertThrows(DataIntegrityViolationException.class,
            () -> handler.assignEquipment(employeeId, equipmentId));
    }

    @Test
    void assignEquipment_addingEmployeeAndEquipment_shouldReturnListWithOneElement() {
        int companyId = 1;
        Employee employee = new Employee("", "", "", "", companyId);
        handler.addEmployee(employee);

        EquipmentCommon equipment = new EquipmentCommon("", 1, companyId);
        new EquipmentCommonDao(namedJdbcTemplate, new SqlFileHandlerImpl()).addEquipment(equipment);

        int equipmentId = 1;
        int employeeId = 1;
        handler.assignEquipment(employeeId, equipmentId);

        String sqlGetAllEquipmentEmployee = "SELECT * FROM employees_equipment;";
        List<String> allEquipmentEmployee = namedJdbcTemplate.query(sqlGetAllEquipmentEmployee,
            (rs, rowNum) -> "" + rs.getInt("employee_id") + rs.getInt("equipment_id"));

        int expectedSize = 1;
        assertEquals(expectedSize, allEquipmentEmployee.size());
    }

    @Test
    void disAssignEquipment_emptyTable_shouldNotThrowAnyException() {
        int equipmentId = 1;
        int employeeId = 1;
        assertDoesNotThrow(() -> handler.disAssignEquipment(employeeId, equipmentId));
    }

    @Test
    void disAssignEquipment_tableWithOneRecord_shouldReturnEmptyList() {
        int companyId = 1;
        Employee employee = new Employee("", "", "", "", companyId);
        handler.addEmployee(employee);

        EquipmentCommon equipment = new EquipmentCommon("", 1, companyId);
        new EquipmentCommonDao(namedJdbcTemplate, new SqlFileHandlerImpl()).addEquipment(equipment);

        int equipmentId = 1;
        int employeeId = 1;
        handler.assignEquipment(employeeId, equipmentId);
        handler.disAssignEquipment(employeeId, equipmentId);

        String sqlGetAllEquipmentEmployee = "SELECT * FROM employees_equipment;";
        List<String> allEquipmentEmployee = namedJdbcTemplate.query(sqlGetAllEquipmentEmployee,
            (rs, rowNum) -> "" + rs.getInt("employee_id") + rs.getInt("equipment_id"));

        assertEquals(Collections.emptyList(), allEquipmentEmployee);
    }
}