package org.geekhub.treesttech.security;

import org.geekhub.treesttech.dao.user.UserHandler;
import org.geekhub.treesttech.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CustomUserDetailsServiceTest {

    @Mock
    private UserHandler handler;

    private UserDetailsService userDetailsService;

    @BeforeEach
    void setUp() {
        userDetailsService = new CustomUserDetailsService(handler);
    }

    @Test
    void loadUserByUsername_inputUser_shouldReturnNotNull() {
        User user = new User("", "", "", "", true);
        when(handler.getUserByName(anyString())).thenReturn(user);
        assertNotNull(userDetailsService.loadUserByUsername(""));
    }

    @Test
    void loadUserByUsername_inputNull_shouldReturnNotNull() {
        when(handler.getUserByName(anyString())).thenReturn(null);
        assertNotNull(userDetailsService.loadUserByUsername(""));
    }

    @Test
    void loadUserByUsername_shouldThrowUsernameNotFoundException() {
        when(handler.getUserByName(anyString())).thenThrow(EmptyResultDataAccessException.class);
        assertThrows(UsernameNotFoundException.class,
            () -> userDetailsService.loadUserByUsername(""));
    }

    @Test
    void loadUserByUsername_shouldThrowRuntimeException() {
        when(handler.getUserByName(anyString())).thenThrow(IncorrectResultSizeDataAccessException.class);
        assertThrows(RuntimeException.class,
            () -> userDetailsService.loadUserByUsername(""));
    }
}